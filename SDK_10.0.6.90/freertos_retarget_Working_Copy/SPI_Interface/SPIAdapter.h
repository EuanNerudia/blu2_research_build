
void SPI_Init(HW_SPI_ID id);

void SPI_IO_configure(void);

void ReadData8bit(HW_SPI_ID id, uint8_t Register, int Peripheral);

uint16_t ReadData16bit(HW_SPI_ID id, uint8_t Register1, uint8_t Register2);

void SendCommand_SPI(HW_SPI_ID id, uint16_t Command, int Peripheral);
