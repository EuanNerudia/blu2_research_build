/*
 * Accelerometer.c
 *
 *  Created on: 16 Dec 2019
 *      Author: Euan Denton
 */

//#define CommandReg 0x7E
//
//#define AccNormalPower  0x11
//#define AccLowPower     0x12
//
//#define GyroNormal      0x15
//#define GyroFastStartUp 0x17
//
#define X_AccL 0x12
#define X_AccH 0x13

#define Y_AccL 0x14
#define Y_AccH 0x15

#define Z_AccL 0x16
#define Z_AccH 0x17
//
//#define X_GyroL 0x0C
//#define X_GyroH 0x0D
//
//#define Y_GyroL 0x0E
//#define Y_GyroH 0x0f
//
//#define Z_GyroL 0x10
//#define Z_GyroH 0x11

#define NormalPowerCommand 0x7E11

#define read_X 0x1213
#define read_Y 0x1415
#define read_Z 0x1617

#include "hw_spi.h"

#include "SPIAdapter.h"

void Read_X_Y_Z_SPI(void)
{
        volatile uint16_t X_Val = 0;
        volatile uint16_t Y_Val = 0;
        volatile uint16_t Z_Val = 0;

        X_Val = ReadData16bit(HW_SPI1, X_AccL, X_AccH);
        Y_Val = ReadData16bit(HW_SPI1, Y_AccL, Y_AccH);
        Z_Val = ReadData16bit(HW_SPI1, Z_AccL, Z_AccH);
}

void Set_Power_Mode_SPI(void)
{
        ReadData8bit(HW_SPI1, NormalPowerCommand, 0);

        while (1)
        Read_X_Y_Z_SPI();
}

void Accelerometer_Setup_SPI(void)
{
//        SPI_IO_configure();

        ReadData8bit(HW_SPI1, X_AccL, 0);

        Set_Power_Mode_SPI();
}

//void hw_spi_set_cs_low(HW_SPI_ID id)
//{
//        SPI_Data *spid = SPIDATA(id);
//
//        hw_gpio_set_inactive(spid->cs_pad.port, spid->cs_pad.pin);    // pull CS low
//}
//
//void hw_spi_set_cs_high(HW_SPI_ID id)
//{
//        SPI_Data *spid = SPIDATA(id);
//
//        hw_gpio_set_active(spid->cs_pad.port, spid->cs_pad.pin);    // push CS high
//}
