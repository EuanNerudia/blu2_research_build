/*
 * SPIAdapter.c
 *
 *  Created on: 13 Dec 2019
 *      Author: Euan Denton
 */

#include "hw_pdc.h"
#include "hw_spi.h"
#include "ad_spi.h"
#include "hw_sys.h"

/* SPI chip-select pins */
//static const ad_io_conf_t spi_master_cs[] = {{
//
//        .port = HW_GPIO_PORT_0,
//        .pin  = HW_GPIO_PIN_29,
//        .on = {
//                .mode     = HW_GPIO_MODE_OUTPUT_PUSH_PULL,
//                .function = HW_GPIO_FUNC_SPI_EN,
//                .high     = true
//        },
//        .off = {
//                .mode     = HW_GPIO_MODE_OUTPUT_PUSH_PULL,
//                .function = HW_GPIO_FUNC_SPI_EN,
//                .high     = true
//        }},
//};
//
///* SPI1 IO */
//const ad_spi_io_conf_t bus_SPI1 = {
//
//        .spi_do = {
//                .port = HW_GPIO_PORT_0,
//                .pin  = HW_GPIO_PIN_26,
//                .on   = {HW_GPIO_MODE_OUTPUT_PUSH_PULL, HW_GPIO_FUNC_SPI_DO, false},
//                .off  = {HW_GPIO_MODE_INPUT,            HW_GPIO_FUNC_GPIO,   true},
//        },
//        .spi_di = {
//                .port = HW_GPIO_PORT_0,
//                .pin  = HW_GPIO_PIN_27,
//                .on   = {HW_GPIO_MODE_INPUT, HW_GPIO_FUNC_SPI_DI, false},
//                .off  = {HW_GPIO_MODE_INPUT, HW_GPIO_FUNC_GPIO,   true},
//        },
//        .spi_clk = {
//                .port = HW_GPIO_PORT_0,
//                .pin  = HW_GPIO_PIN_28,
//                .on   = {HW_GPIO_MODE_OUTPUT_PUSH_PULL, HW_GPIO_FUNC_SPI_CLK, false},
//                .off  = {HW_GPIO_MODE_INPUT,            HW_GPIO_FUNC_GPIO,    true},
//        },
//
//        /*
//         * The number of pins in spi_master_cs array.
//         *
//         * \warning When the SPI bus is used by SNC \p cs_cnt must be always 1
//         */
//        .cs_cnt = 1,
//        .spi_cs = spi_master_cs,
//
//        .voltage_level = HW_GPIO_POWER_V33
//};
//
//
///* External sensor/module SPI driver */
//const ad_spi_driver_conf_t drv_SPI1 = {
//        .spi = {
//                .cs_pad = {HW_GPIO_PORT_0, HW_GPIO_PIN_29},
//                .word_mode = HW_SPI_WORD_16BIT, /* 2-byte mode */
//                .smn_role  = HW_SPI_MODE_MASTER,
//                .polarity_mode = HW_SPI_POL_LOW,
//                .phase_mode    = HW_SPI_PHA_MODE_0,
//                .mint_mode = HW_SPI_MINT_DISABLE,
//                .xtal_freq = HW_SPI_FREQ_DIV_8,
//                .fifo_mode = HW_SPI_FIFO_RX_TX,
//                .disabled  = 0, /* Should be disabled during initialization phase */
//                .ignore_cs = false,
//                .use_dma   = true,
//                .rx_dma_channel = HW_DMA_CHANNEL_0,
//                .tx_dma_channel = HW_DMA_CHANNEL_1
//        }
//};
//
//
///* Sensor/module device configuration */
//const ad_spi_controller_conf_t dev_SPI_CUSTOM_DEVICE = {
//        .id  = HW_SPI1,
//        .io  = &bus_SPI1,
//        .drv = &drv_SPI1
//};

// Configures the GPIO pin for the correct function
void PinConfig(HW_GPIO_PORT Port, HW_GPIO_PIN Pin, HW_GPIO_POWER Power, HW_GPIO_MODE Mode, HW_GPIO_FUNC Function, bool High, bool Enable)
{
        hw_gpio_configure_pin(Port, Pin, Mode, Function, true);
        hw_gpio_pad_latch_enable(Port, Pin);

        if (!Enable)
        {
                hw_gpio_pad_latch_disable(Port, Pin);
        }

        if (Power != HW_GPIO_POWER_NONE)
        {
                hw_gpio_configure_pin_power(Port, Pin, Power);
        }
}

void SPI_Init(HW_SPI_ID id)
{
        hw_sys_pd_com_enable();

        PinConfig(HW_GPIO_PORT_0, HW_GPIO_PIN_26, HW_GPIO_POWER_NONE, HW_GPIO_MODE_OUTPUT_PUSH_PULL, HW_GPIO_FUNC_SPI_DO, false, true);
        PinConfig(HW_GPIO_PORT_0, HW_GPIO_PIN_27, HW_GPIO_POWER_NONE, HW_GPIO_MODE_INPUT, HW_GPIO_FUNC_SPI_DI, false, true);
        PinConfig(HW_GPIO_PORT_0, HW_GPIO_PIN_28, HW_GPIO_POWER_NONE, HW_GPIO_MODE_OUTPUT_PUSH_PULL, HW_GPIO_FUNC_SPI_CLK, false, true);
        PinConfig(HW_GPIO_PORT_0, HW_GPIO_PIN_29, HW_GPIO_POWER_NONE, HW_GPIO_MODE_OUTPUT_PUSH_PULL, HW_GPIO_FUNC_SPI_EN, false, true);

        hw_spi_enable(id, 1);

        HW_SPI_REG_SETF(id, SPI_CTRL_REG, SPI_MINT, false);

        HW_SPI_REG_SETF(id, SPI_CTRL_REG, SPI_WORD, HW_SPI_WORD_16BIT);
        HW_SPI_REG_SETF(id, SPI_CTRL_REG, SPI_SMN, HW_SPI_MODE_MASTER);
        HW_SPI_REG_SETF(id, SPI_CTRL_REG, SPI_POL, HW_SPI_POL_LOW);
        HW_SPI_REG_SETF(id, SPI_CTRL_REG, SPI_PHA, HW_SPI_PHA_MODE_0);
        HW_SPI_REG_SETF(id, SPI_CTRL_REG, SPI_CLK, HW_SPI_FREQ_DIV_8);

        if (id == HW_SPI1) {
               CRG_COM->RESET_CLK_COM_REG = CRG_COM_RESET_CLK_COM_REG_SPI_CLK_SEL_Msk;
               CRG_COM->SET_CLK_COM_REG = CRG_COM_SET_CLK_COM_REG_SPI_ENABLE_Msk;
        } else {
               CRG_COM->RESET_CLK_COM_REG = CRG_COM_RESET_CLK_COM_REG_SPI2_CLK_SEL_Msk;
               CRG_COM->SET_CLK_COM_REG = CRG_COM_SET_CLK_COM_REG_SPI2_ENABLE_Msk;
        }

        HW_SPI_REG_SETF(id, SPI_CTRL_REG, SPI_FIFO_MODE, HW_SPI_FIFO_RX_TX);
        HW_SPI_REG_SETF(id, SPI_CTRL_REG, SPI_DMA_TXREQ_MODE, 0);

        hw_spi_set_cs_ctrl(id, 1);
}

void SPI_IO_configure(void)
{
        /*
         * Below is the IO pin config, this is different to the overall
         * pin set up for the SPI comms but needs to be done first
         */
        hw_sys_pd_com_enable();

        PinConfig(HW_GPIO_PORT_0, HW_GPIO_PIN_26, HW_GPIO_POWER_V33, HW_GPIO_MODE_INPUT, HW_GPIO_FUNC_GPIO, true, false);
        PinConfig(HW_GPIO_PORT_0, HW_GPIO_PIN_27, HW_GPIO_POWER_V33, HW_GPIO_MODE_INPUT, HW_GPIO_FUNC_GPIO, true, false);
        PinConfig(HW_GPIO_PORT_0, HW_GPIO_PIN_28, HW_GPIO_POWER_V33, HW_GPIO_MODE_INPUT, HW_GPIO_FUNC_GPIO, true, false);
        PinConfig(HW_GPIO_PORT_0, HW_GPIO_PIN_29, HW_GPIO_POWER_V33, HW_GPIO_MODE_OUTPUT_PUSH_PULL, HW_GPIO_FUNC_SPI_EN, true, false);

        hw_sys_pd_com_disable();
}

uint8_t SPI_Read_Start(HW_SPI_ID id, uint8_t Register)
{
        hw_gpio_set_inactive(HW_GPIO_PORT_0, HW_GPIO_PIN_29);

        SBA(id)->SPI_RX_TX_REG = Register;

//        while(!hw_spi_get_interrupt_status(id))
//        {
//                // Add in handling to stop hanging.
//        }

        hw_spi_clear_interrupt(id);

        // Check that the value is read

        // return the value
}

uint8_t SPI_Read_Next(HW_SPI_ID id)
{
//        while(!hw_spi_get_interrupt_status(id))
//        {
//                // Add in handling to stop hanging.
//        }

        hw_spi_clear_interrupt(id);
}

uint8_t SPI_Read_End(HW_SPI_ID id)
{
        hw_spi_clear_interrupt(id);

        hw_gpio_set_active(HW_GPIO_PORT_0, HW_GPIO_PIN_29);
}

void SPI_8Bit_Read(HW_SPI_ID id, uint8_t Register)
{
        hw_gpio_set_inactive(HW_GPIO_PORT_0, HW_GPIO_PIN_29);

        //SBA(id)->SPI_RX_TX_REG = 0x7F;

        SBA(id)->SPI_RX_TX_REG = 0x10;


        hw_gpio_set_active(HW_GPIO_PORT_0, HW_GPIO_PIN_29);


        // Write the register then read from it, after inter
}

uint16_t ReadData16bit(HW_SPI_ID id, uint8_t Register1, uint8_t Register2)
{
        hw_gpio_set_inactive(HW_GPIO_PORT_0, HW_GPIO_PIN_29);

        Register1 = Register1 | 0x80;

        SBA(id)->SPI_RX_TX_REG = Register1;

        volatile int Inter = hw_spi_get_interrupt_status(id);

//        hw_gpio_set_inactive(HW_GPIO_PORT_0, HW_GPIO_PIN_29);
//        while(!hw_spi_get_interrupt_status(id))
//        {
//                // Add in handling to stop hanging.
//        }

        uint8_t Value = SBA(id)->SPI_RX_TX_REG;

        hw_spi_clear_interrupt(id);

        hw_gpio_set_active(HW_GPIO_PORT_0, HW_GPIO_PIN_29);

        return Value;
}

void SPI_Command(HW_SPI_ID id, uint16_t Command, int Peripheral)
{
        Command = Command & 0x7F;

        hw_gpio_set_inactive(HW_GPIO_PORT_0, HW_GPIO_PIN_29);

        SBA(id)->SPI_RX_TX_REG = Command;

        hw_gpio_set_active(HW_GPIO_PORT_0, HW_GPIO_PIN_29);
}



