typedef unsigned char                   N_U08;
typedef unsigned short                  N_U16;
typedef unsigned long                   N_U32; // unsigned int
typedef unsigned long long              N_U64;

typedef signed char                     N_S08;
typedef signed short                    N_S16;
typedef signed long                     N_S32; // signed int
typedef signed long long                N_S64;

typedef float                           N_F32;
typedef double                          N_D64;
typedef N_U08                           N_BOOL;

N_BOOL ConfiguredCheck(void);

N_U08 Read_8Bit(N_U08 Register);

N_U16 Read_16Bit(N_U08 Register1, N_U08 Register2);

N_U32 Read_24Bit(N_U08 Register1, N_U08 Register2, N_U08 Register3);

void Send_Command(N_U08 Register, N_U08 Command);

void Change_Slave_address(N_U08 address);

void I2C_IO_Setup(void);
