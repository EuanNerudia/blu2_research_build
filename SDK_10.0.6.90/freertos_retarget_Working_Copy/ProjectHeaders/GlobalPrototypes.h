/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: .c   
 * Purpose: 
 * Version: V1.0
 * Author: M Watson
 * Version 
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/
extern void System_Main(void);
extern void System_Init(void);
extern void System_Tick(void);
extern void hard_fault_handler_c (unsigned int * hardfault_args);


//#include "../../HardwareAdapters/HardwareAdapterHeaders/HardwareAdapterPrototypes.h"
//#include "../../PlatformAdapters/PlatformAdapterHeaders/PlatformAdapterPrototypes.h"
//#include "../../ApplicationLayer/Initialise/InitialisePrototypes.h"
//#include "../../ApplicationLayer/LoggingStateMachineModule/LoggingStateMachinePrototypes.h"
//#include "../../ApplicationLayer/GenericStateMachineModule/EnginesPrototypes.h"
//#include "../../ApplicationLayer/TimerModule/TimerPrototypes.h"
//#include "../../ApplicationLayer/NerudicomModule/NerudicomModulePrototypes.h"
//#include "../../ApplicationLayer/ChargeStateMachineModule/ChargingStateMachinePrototypes.h"
//#include "../../ApplicationLayer/ConnectionStateMachineModule/ConnectionStateMachinePrototypes.h"
//#include "../../ApplicationLayer/HeaterStateMachineModule/HeaterStateMachinePrototypes.h"
//#include "../../ApplicationLayer/LockStateMachineModule/LockStateMachinePrototypes.h"
//#include "../../ApplicationLayer/GestureStateMachineModule/GestureStateMachinePrototypes.h"
//#include "../../ApplicationLayer/FeedbackStateMachineModule/FeedbackStateMachinePrototypes.h"
/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/
 


