/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: .c   
 * Purpose: 
 * Version: V1.0
 * Author: M Watson
 * Version 
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/
#define INLINE

#define BIT(n)			((U32)(1UL<<(n)))

#define REG_VAR 
#define __AT(__ADDR)     __attribute__((section(".ARM.__at_" #__ADDR)))
#define AT_ADDR(__ADDR)  __AT(__ADDR)

#define PERSISTENT_MEM


// Project Includes
//#include "../../HardwareAdapters/HardwareAdapterHeaders/HardwareAdapterDefines.h"
//#include "../../PlatformAdapters/PlatformAdapterHeaders/PlatformAdapterDefines.h"
//#include "../../ApplicationLayer/Initialise/InitialiseDefines.h"
//#include "../../ApplicationLayer/LoggingStateMachineModule/LoggingStateMachineDefines.h"
//#include "../../ApplicationLayer/GenericStateMachineModule/EnginesDefines.h"
//#include "../../ApplicationLayer/TimerModule/TimerDefines.h"
//#include "../../ApplicationLayer/NerudicomModule/NerudicomModuleDefines.h"
//#include "../../ApplicationLayer/ChargeStateMachineModule/ChargingStateMachineDefines.h"
//#include "../../ApplicationLayer/ConnectionStateMachineModule/ConnectionStateMachineDefines.h"
//#include "../../ApplicationLayer/HeaterStateMachineModule/HeaterStateMachineDefines.h"
//#include "../../ApplicationLayer/LockStateMachineModule/LockStateMachineDefines.h"
//#include "../../ApplicationLayer/GestureStateMachineModule/GestureStateMachineDefines.h"
//#include "../../ApplicationLayer/FeedbackStateMachineModule/FeedbackStateMachineDefines.h"

/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/
 








