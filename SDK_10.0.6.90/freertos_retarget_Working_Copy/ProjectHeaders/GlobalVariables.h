/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: .c   
 * Purpose: 
 * Version: V1.0
 * Author: M Watson
 * Version 
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/
#ifdef MAIN_FILE
	#define INT_EXT
#else
	#define INT_EXT extern
#endif


// Project Includes
//#include "../../HardwareAdapters/HardwareAdapterHeaders/HardwareAdapterVariables.h"
//#include "../../PlatformAdapters/PlatformAdapterHeaders/PlatformAdapterVariables.h"
//#include "../../ApplicationLayer/ProjectHeaders/SharedMemoryAreas.h"
//#include "../../ApplicationLayer/Initialise/InitialiseVariables.h"
//#include "../../ApplicationLayer/LoggingStateMachineModule/LoggingStateMachineVariables.h"
//#include "../../ApplicationLayer/GenericStateMachineModule/EnginesVariables.h"
//#include "../../ApplicationLayer/TimerModule/TimerVariables.h"
//#include "../../ApplicationLayer/NerudicomModule/NerudicomModuleVariables.h"
//#include "../../ApplicationLayer/ChargeStateMachineModule/ChargingStateMachineVariables.h"
//#include "../../ApplicationLayer/ConnectionStateMachineModule/ConnectionStateMachineVariables.h"
//#include "../../ApplicationLayer/HeaterStateMachineModule/HeaterStateMachineVariables.h"
//#include "../../ApplicationLayer/LockStateMachineModule/LockStateMachineVariables.h"
//#include "../../ApplicationLayer/GestureStateMachineModule/GestureStateMachineVariables.h"
//#include "../../ApplicationLayer/FeedbackStateMachineModule/FeedbackStateMachineVariables.h"
	#ifdef INT_EXT 
	#undef INT_EXT
#endif
#ifdef MAIN_FILE
	#undef MAIN_FILE
#endif


/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/



