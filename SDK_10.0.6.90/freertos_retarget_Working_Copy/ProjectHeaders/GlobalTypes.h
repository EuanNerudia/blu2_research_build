/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: .c   
 * Purpose: 
 * Version: V1.0
 * Author: M Watson
 * Version 
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/
// declare all standard types

// Data types

/*
Data Type 			Bits 	Bytes 	Alignment 	Value Range 
=========			====	=====	=========	==============================
signed char 		 		8 		  1 		1 		-128 to +127 
unsigned char 		 8 		  1 		1 		0 to 255 
signed short 			16 		  2 		2 		-32768 to +32767 
unsigned short		16 		  2 		2 		0 to 65535 
enum 							32 		  4 		4 		-2147483648 to +2147483647 
signed int 				32 		  4 		4 		-2147483648 to +2147483647 
unsigned int 			32 		  4 		4 		0 to 4294967295 
signed long 			32 		  4 		4 		-2147483648 to +2147483647 
unsigned long 		32 		  4 		4 		0 to 4294967295 
signed long long 	64 		  8 		4 		-263 to +263-1 
unsigned long long	64 		  8 		4 		0 to 264-1 
float 				32 		  4 		4 		�1.175494E-38 to �3.402823E+38 
double 				64 		  8 		4 		�1.7-308 to �1.7E+308 
pointer 			32 		  4 		4 		32-bit Address 
*/
#ifndef VARIABLE_TYPES
#define VARIABLE_TYPES

typedef unsigned char			N_U08;
typedef unsigned short                  N_U16;
typedef unsigned long			N_U32; // unsigned int
typedef unsigned long long		N_U64;

typedef signed char			N_S08;
typedef signed short			N_S16;
typedef signed long			N_S32; // signed int
typedef signed long long		N_S64;

typedef float				N_F32;
typedef double				N_D64;
typedef N_U08                           N_BOOL;

#endif

#define N_TRUE	(N_BOOL)1
#define N_FALSE (N_BOOL)0
//#define true TRUE
//#define false FALSE

//#define __PACKED 

#ifndef NULL
#define NULL    ((void *)0)
#endif
// Project Includes
//#include "../../HardwareAdapters/HardwareAdapterHeaders/HardwareAdapterTypes.h"
//#include "../../PlatformAdapters/PlatformAdapterHeaders/PlatformAdapterTypes.h"
//#include "../../ApplicationLayer/Initialise/InitialiseTypes.h"
//#include "../../ApplicationLayer/LoggingStateMachineModule/LoggingStateMachineTypes.h"
//#include "../../ApplicationLayer/GenericStateMachineModule/EnginesTypes.h"
//#include "../../ApplicationLayer/TimerModule/TimerTypes.h"
//#include "../../ApplicationLayer/NerudicomModule/NerudicomModuleTypes.h"
//#include "../../ApplicationLayer/ChargeStateMachineModule/ChargingStateMachineTypes.h"
//#include "../../ApplicationLayer/ConnectionStateMachineModule/ConnectionStateMachineTypes.h"
//#include "../../ApplicationLayer/HeaterStateMachineModule/HeaterStateMachineTypes.h"
//#include "../../ApplicationLayer/LockStateMachineModule/LockStateMachineTypes.h"
//#include "../../ApplicationLayer/GestureStateMachineModule/GestureStateMachineTypes.h"
//#include "../../ApplicationLayer/FeedbackStateMachineModule/FeedbackStateMachinePrototypes.h"
/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/
 




















