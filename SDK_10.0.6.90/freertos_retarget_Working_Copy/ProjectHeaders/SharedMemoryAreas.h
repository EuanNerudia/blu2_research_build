/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: SharedMemoryAreas.h   
 * Purpose: Contains the shared memory structures and associated defines for bootloader integration
 * Version: V1.0
 * Author: M Watson
 * Version 
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/
 

#define MEMORY_STRUCTURE_START_ADDS             0x20000000
#define MEMORY_STRUCTURE_START_ADDS_RSI         0x20000000
#define MEMORY_STRUCTURE_START_ADDS_AAB         0x20000004
#define MEMORY_STRUCTURE_START_ADDS_HF	        0x20000008



#ifdef MAIN_FILE
INT_EXT AT_ADDR(MEMORY_STRUCTURE_START_ADDS_RSI) volatile N_U32 reset_source_id; // Why the reset occurred e.g. watchdog or hard reset
INT_EXT AT_ADDR(MEMORY_STRUCTURE_START_ADDS_AAB) volatile N_U32 action_at_boot; // Uses
INT_EXT AT_ADDR(MEMORY_STRUCTURE_START_ADDS_HF) volatile HardFaultInfoStruct LastHardFault;


#else
INT_EXT volatile N_U32 reset_source_id;
INT_EXT volatile N_U32 action_at_boot; // Uses
INT_EXT volatile HardFaultInfoStruct   LastHardFault;

#endif
 
/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/

