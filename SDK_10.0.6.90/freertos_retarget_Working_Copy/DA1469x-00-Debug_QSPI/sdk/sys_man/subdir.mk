################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_adc.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_bsr.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_charger_da1469x.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_clock_mgr_da1469x.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_power_mgr_da1469x.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_rcx_calibrate.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_tcs_da1469x.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_timer.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_trng.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_usb_da1468x.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_usb_da1469x.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_watchdog.c 

OBJS += \
./sdk/sys_man/sys_adc.o \
./sdk/sys_man/sys_bsr.o \
./sdk/sys_man/sys_charger_da1469x.o \
./sdk/sys_man/sys_clock_mgr_da1469x.o \
./sdk/sys_man/sys_power_mgr_da1469x.o \
./sdk/sys_man/sys_rcx_calibrate.o \
./sdk/sys_man/sys_tcs_da1469x.o \
./sdk/sys_man/sys_timer.o \
./sdk/sys_man/sys_trng.o \
./sdk/sys_man/sys_usb_da1468x.o \
./sdk/sys_man/sys_usb_da1469x.o \
./sdk/sys_man/sys_watchdog.o 

C_DEPS += \
./sdk/sys_man/sys_adc.d \
./sdk/sys_man/sys_bsr.d \
./sdk/sys_man/sys_charger_da1469x.d \
./sdk/sys_man/sys_clock_mgr_da1469x.d \
./sdk/sys_man/sys_power_mgr_da1469x.d \
./sdk/sys_man/sys_rcx_calibrate.d \
./sdk/sys_man/sys_tcs_da1469x.d \
./sdk/sys_man/sys_timer.d \
./sdk/sys_man/sys_trng.d \
./sdk/sys_man/sys_usb_da1468x.d \
./sdk/sys_man/sys_usb_da1469x.d \
./sdk/sys_man/sys_watchdog.d 


# Each subdirectory must supply rules for building sources it contributes
sdk/sys_man/sys_adc.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_adc.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\portable\GCC\DA1469x" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -include"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/sys_man/sys_bsr.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_bsr.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\portable\GCC\DA1469x" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -include"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/sys_man/sys_charger_da1469x.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_charger_da1469x.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\portable\GCC\DA1469x" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -include"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/sys_man/sys_clock_mgr_da1469x.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_clock_mgr_da1469x.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\portable\GCC\DA1469x" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -include"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/sys_man/sys_power_mgr_da1469x.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_power_mgr_da1469x.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\portable\GCC\DA1469x" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -include"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/sys_man/sys_rcx_calibrate.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_rcx_calibrate.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\portable\GCC\DA1469x" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -include"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/sys_man/sys_tcs_da1469x.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_tcs_da1469x.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\portable\GCC\DA1469x" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -include"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/sys_man/sys_timer.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_timer.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\portable\GCC\DA1469x" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -include"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/sys_man/sys_trng.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_trng.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\portable\GCC\DA1469x" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -include"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/sys_man/sys_usb_da1468x.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_usb_da1468x.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\portable\GCC\DA1469x" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -include"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/sys_man/sys_usb_da1469x.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_usb_da1469x.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\portable\GCC\DA1469x" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -include"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/sys_man/sys_watchdog.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/system/sys_man/sys_watchdog.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\portable\GCC\DA1469x" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -include"C:\Projects\SmartSnippets\ADC_Project\SDK_10.0.6.90\freertos_retarget_Working_Copy\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


