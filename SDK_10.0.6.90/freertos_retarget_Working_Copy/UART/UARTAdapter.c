/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: .c   
 * Purpose: 
 * Version: V1.0
 * Author: M Watson
 * Version 
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/

#include "sys_power_mgr.h"
#include "sys_bsr.h"
#include <osal.h>
#include <hw_gpio.h>
#include <ad.h>
#include "hw_uart.h"
#include "UARTAdapterDefines.h"
//#include "UARTAdapterTypes.h"
#include "../ProjectHeaders/ProjectIncludes.h"


//-----------[O]----------//
// Internal Variables
volatile N_U08 RxBuffer[N_AVAIL_UARTS][UART_MAX_PACKET_SIZE];
volatile N_U08 TxBuffer[N_AVAIL_UARTS][UART_MAX_PACKET_SIZE];
volatile N_U08 RxBufferCounter[N_AVAIL_UARTS];
volatile N_U08 TxBufferCounter[N_AVAIL_UARTS];
volatile N_U08 TxBufferSent[N_AVAIL_UARTS];
volatile N_BOOL RxReady[N_AVAIL_UARTS];
volatile N_U32 Countdown_RxReady[N_AVAIL_UARTS];
volatile N_U32 InterByteRxTimeout[N_AVAIL_UARTS];
volatile N_U32 RxExpected[N_AVAIL_UARTS];
volatile N_U32 OffsetToLengthByte[N_AVAIL_UARTS];




static void UART1CallBack(void);
static void UART2CallBack(void);
static void UART3CallBack(void);
void UARTCallBack(N_U08 MyUART);
void UARTDataReset(N_U08 MyUART);
void ResetFIFOs(N_U08 MyUART);
N_U08 UART_ReadByte(N_U08 MyUART);
void UART_WriteByte(N_U08 MyUART, N_U08 DataByte);
void ConfigureUART(N_U08 MyUART);


//-----------[O]----------//



bool UARTAdapter_WakingUp(void)
{

        return true; // allow M33 to enter sleep

}

//-----------[O]----------//

bool UARTAdapter_ReadyToSleep(void)
{

        return true; // allow M33 to enter sleep

}

//-----------[O]----------//

/* Callback function triggered following a sleep cancellation cycle */
void UARTAdapter_CancelSleep(void)
{

}

//-----------[O]----------//

void UARTCallBack(N_U08 MyUART)
{
        HW_UART_ID id;
        N_U08 ByteCount;
        N_U32 ISReg = 0;
        if(MyUART==N_UART1)
                id = HW_UART1;
        else if(MyUART==N_UART2)
                id = HW_UART2;
        else if(MyUART==N_UART3)
                id = HW_UART3;
        else
                return;
        ISReg = UBA(id)->UART2_IIR_FCR_REG;// warning, the interrupt identifier is not a bit pattern!!!!!
        UBA(id)->UART2_IIR_FCR_REG = 0;


        if((ISReg & (0xf))==1)
        {// no interrupt pending
                return;
        }

        if((ISReg & (0xf))==6)
        {
                UARTDataReset (MyUART);
                ResetFIFOs(MyUART);
        }

        if((ISReg & (0xf))==4)
        {// Data in rx fifo, empty it
                RxBuffer[MyUART][RxBufferCounter[MyUART]] = UART_ReadByte(MyUART);
                if(RxBufferCounter[MyUART]==OffsetToLengthByte[MyUART])
                {// this byte is expected size of full packet
                        RxExpected[MyUART]= RxBuffer[MyUART][RxBufferCounter[MyUART]];
                }

                RxBufferCounter[MyUART]++;
                if(RxExpected[MyUART]<=RxBufferCounter[MyUART])
                {// got the number we were expecting so flag packet ready

                        Countdown_RxReady[MyUART]=0;
                        RxReady[MyUART]=1;
                }
                else
                        Countdown_RxReady[MyUART]=InterByteRxTimeout[MyUART];
        }

        if((ISReg & (0xf))==2)
        {// Tx FIFO not full
                if(TxBufferCounter[MyUART]>0)
                {// space in fifo and stuff left to send, load next byte into fifo
                        UART_WriteByte(MyUART ,TxBuffer[MyUART][TxBufferSent[MyUART]]);
                        TxBufferSent[MyUART]++;
                        TxBufferCounter[MyUART]--;
                }
                else
                {
                        NVIC_DisableIRQ(UART_INT(id));
                        HW_UART_REG_SETF(id, IER_DLH, ETBEI_DLH1, N_FALSE);
                        // tx empty interrupt
                        NVIC_EnableIRQ(UART_INT(id));
                }

        }
        // need to check for some errors here?

}

//-----------[O]----------//

void RxComplete(N_U08 MyUART)
{
        RxReady[MyUART]=0;
        RxBufferCounter[MyUART]=0;
        RxBufferCounter[MyUART]=0;
        Countdown_RxReady[MyUART]=0;
}

//-----------[O]----------//


void SetOffsetToLength(N_U08 MyUART, N_U08 Offset)
{
        OffsetToLengthByte[MyUART] = Offset;
}

//-----------[O]----------//


static void UART1CallBack(void)
{
        UARTCallBack(N_UART1);
}

//-----------[O]----------//


static void UART2CallBack(void)
{
        UARTCallBack(N_UART2);
}

//-----------[O]----------//

static void UART3CallBack(void)
{
        UARTCallBack(N_UART3);
}

//-----------[O]----------//

void UARTDataReset(N_U08 MyUART)
{
        N_U32 DataIndex;
        for(DataIndex=0;DataIndex<UART_MAX_PACKET_SIZE;DataIndex++)
        {
                RxBuffer[MyUART][DataIndex]=0;
                TxBuffer[MyUART][DataIndex]=0;
        }
        RxBufferCounter[MyUART]=0;
        TxBufferCounter[MyUART]=0;
        TxBufferSent[MyUART]=0;
        RxReady[MyUART]=0;
        Countdown_RxReady[MyUART]=0;
        InterByteRxTimeout[MyUART]=0;
        RxExpected[MyUART]=0;
        OffsetToLengthByte[MyUART]=0;
   //     ResetFIFOs(MyUART);
}

//-----------[O]----------//

void InitialiseUARTAdpater(void)
{
        N_U08 Index;
        for(Index=0;Index<N_AVAIL_UARTS;Index++)
        {
                UARTDataReset(Index);
        }


}

//-----------[O]----------//


void ConfigureUART(N_U08 MyUART)
{
        // we're the peripheral so we get to dictate the standard comms setup
        HW_UART_ID uart;
        if(MyUART==N_UART1)
                uart = HW_UART1;
        else if(MyUART==N_UART2)
                uart = HW_UART2;
        else if(MyUART==N_UART3)
                uart = HW_UART3;
        else
                return;
        // receive data avail int

        NVIC_DisableIRQ(UART_INT(uart));
        // set Rx Interrupt
        HW_UART_REG_SETF(uart, IER_DLH, ERBFI_DLH0, N_TRUE);
        NVIC_EnableIRQ(UART_INT(uart));
        // disable loopback
        HW_UART_REG_SETF(uart, MCR, UART_LB, 0);
        // disable flow control
        HW_UART_REG_SETF(uart, MCR, UART_AFCE, 0);
        HW_UART_REG_SETF(uart, MCR, UART_RTS, 0);
        // enable fifo
   //     hw_uart_fifo_en_setf(uart, 1);
        hw_uart_fifo_en_setf(uart, 0);
        // interrupt at 1 char in fifo
        UBA(uart)->UART2_SRT_REG=0;
        ResetFIFOs(MyUART);
        // Set Parity
        UBA(uart)->UART2_LCR_REG = UBA(uart)->UART2_LCR_REG | (0) << 3;
        // Set Data Bits
        HW_UART_REG_SETF(uart, LCR, UART_DLS, 3);
        // Set Stop Bits
        HW_UART_REG_SETF(uart, LCR, UART_STOP, 0);
}

//-----------[O]----------//


void ResetFIFOs(N_U08 MyUART)
{
        HW_UART_ID uart;
        if(MyUART==N_UART1)
                uart = HW_UART1;
        else if(MyUART==N_UART2)
                uart = HW_UART2;
        else if(MyUART==N_UART3)
                uart = HW_UART3;
        else
                return;
       HW_UART_REG_SETF(uart, SRR, UART_XFR, 1);
        HW_UART_REG_SETF(uart, SRR, UART_RFR, 1);
}

//-----------[O]----------//


void  UART_SetTimeout(N_U08 MyUART, N_U08 Timeout)
{
        InterByteRxTimeout[MyUART] = Timeout;
}

//-----------[O]----------//


N_U08 UART_ReadByte(N_U08 MyUART)
{
        HW_UART_ID uart;
        if(MyUART==N_UART1)
                uart = HW_UART1;
        else if(MyUART==N_UART2)
                uart = HW_UART2;
        else if(MyUART==N_UART3)
                uart = HW_UART3;
        else
                return(0);

        // Read element from the receive FIFO
        return UBA(uart)->UART2_RBR_THR_DLL_REG;
}

//-----------[O]----------//


void UART_WriteByte(N_U08 MyUART, N_U08 DataByte)
{
        HW_UART_ID uart;
        if(MyUART==N_UART1)
                uart = HW_UART1;
        else if(MyUART==N_UART2)
                uart = HW_UART2;
        else if(MyUART==N_UART3)
                uart = HW_UART3;
        else
                return;
        UBA(uart)->UART2_RBR_THR_DLL_REG = DataByte;
        NVIC_DisableIRQ(UART_INT(uart));
        // tx empty interrupt
        HW_UART_REG_SETF(uart, IER_DLH, ETBEI_DLH1, N_TRUE);
        NVIC_EnableIRQ(UART_INT(uart));
}


//-----------[O]----------//


bool ConfigureUART_GPIO(N_U08 id, HW_GPIO_PORT tx_port, HW_GPIO_PIN tx_pin, HW_GPIO_PORT rx_port, HW_GPIO_PIN rx_pin, N_U08 Power)
{
        hw_sys_pd_com_enable();

        if(id == N_UART1)
        {
                hw_gpio_set_pin_function(tx_port, tx_pin, Power, HW_GPIO_FUNC_UART_TX);
                hw_gpio_set_pin_function(rx_port, rx_pin, Power, HW_GPIO_FUNC_UART_RX );
                hw_gpio_pad_latch_enable(tx_port, tx_pin);
                hw_gpio_pad_latch_enable(rx_port, rx_pin);
                GLOBAL_INT_DISABLE();
                REG_SET_BIT(CRG_COM, SET_CLK_COM_REG, UART_ENABLE);
                GLOBAL_INT_RESTORE();
                hw_uart_set_isr(HW_UART1, UART1CallBack);
                ConfigureUART(id);
                return(N_TRUE);
        }
        if(id == N_UART2)
        {
                hw_gpio_set_pin_function(tx_port, tx_pin, Power, HW_GPIO_FUNC_UART2_TX);
                hw_gpio_set_pin_function(rx_port, rx_pin, Power, HW_GPIO_FUNC_UART2_RX );
                hw_gpio_pad_latch_enable(tx_port, tx_pin);
                hw_gpio_pad_latch_enable(rx_port, rx_pin);
                GLOBAL_INT_DISABLE();
                REG_SET_BIT(CRG_COM, SET_CLK_COM_REG, UART2_ENABLE);
                GLOBAL_INT_RESTORE();
                hw_uart_set_isr(HW_UART2, UART2CallBack);
                ConfigureUART(id);
                return(N_TRUE);
        }
        if(id == N_UART3)
        {
                hw_gpio_set_pin_function(tx_port, tx_pin, Power, HW_GPIO_FUNC_UART3_TX);
                hw_gpio_set_pin_function(rx_port, rx_pin, Power, HW_GPIO_FUNC_UART3_RX );
                hw_gpio_pad_latch_enable(tx_port, tx_pin);
                hw_gpio_pad_latch_enable(rx_port, rx_pin);
                GLOBAL_INT_DISABLE();
                REG_SET_BIT(CRG_COM, SET_CLK_COM_REG, UART3_ENABLE);
                GLOBAL_INT_RESTORE();
                hw_uart_set_isr(HW_UART3, UART3CallBack);
                ConfigureUART(id);
                return(N_TRUE);
        }
        return(N_FALSE);
}

//-----------[O]----------//

void SetUARTBaudRate(N_U08 MyUART, HW_UART_BAUDRATE Baud)
{
        HW_UART_BAUDRATE MyBaudRate=HW_UART_BAUDRATE_115200;

//        switch (Baud)
//        {
//                case N_UART_BAUDRATE_1000000:
//                        MyBaudRate=HW_UART_BAUDRATE_1000000;
//                        break;
//                case N_UART_BAUDRATE_500000:
//                        MyBaudRate=HW_UART_BAUDRATE_500000;
//                        break;
//                case N_UART_BAUDRATE_230400:
//                        MyBaudRate=HW_UART_BAUDRATE_230400;
//                        break;
//                case N_UART_BAUDRATE_115200:
//                        MyBaudRate=HW_UART_BAUDRATE_115200;
//                        break;
//                case N_UART_BAUDRATE_57600:
//                        MyBaudRate=HW_UART_BAUDRATE_57600;
//                        break;
//                case N_UART_BAUDRATE_38400:
//                        MyBaudRate=HW_UART_BAUDRATE_38400;
//                        break;
//                case N_UART_BAUDRATE_28800:
//                        MyBaudRate=HW_UART_BAUDRATE_28800;
//                        break;
//                case N_UART_BAUDRATE_19200:
//                        MyBaudRate=HW_UART_BAUDRATE_19200;
//                        break;
//                case N_UART_BAUDRATE_14400:
//                        MyBaudRate=HW_UART_BAUDRATE_14400;
//                        break;
//                case N_UART_BAUDRATE_9600:
//                        MyBaudRate=HW_UART_BAUDRATE_9600 ;
//                        break;
//                case N_UART_BAUDRATE_4800:
//                        MyBaudRate=HW_UART_BAUDRATE_4800 ;
//                        break;
//        }
        if(MyUART==N_UART1)
          hw_uart_baudrate_set(HW_UART1, MyBaudRate);
        if(MyUART==N_UART2)
          hw_uart_baudrate_set(HW_UART2, MyBaudRate);
        if(MyUART==N_UART3)
          hw_uart_baudrate_set(HW_UART3, MyBaudRate);
}

//-----------[O]----------//

N_BOOL UARTDataReady(N_U08 MyUART, N_U32 * Size)
{
        *Size = RxBufferCounter[MyUART];
        return(RxReady[MyUART]);
}

//-----------[O]----------//

void FetchUARTDataPacket(N_U08 MyUART, N_U32 * Size, N_U08 * Data)
{
        int Index;

        for(Index=0;Index<RxBufferCounter[MyUART];Index++)
        {// transfer to comms buffer
                *Data = RxBuffer[MyUART][Index];
                Data++;
        }
        *Size = RxBufferCounter[MyUART];
}

//-----------[O]----------//

N_BOOL SendUARTData(N_U08 MyUART, N_U08 Size, N_U08 * Data)
{
        int Index;

        if(TxBufferCounter[MyUART]==0)
        {// don't try and transmit if we're busy
                for(Index=0;Index<Size;Index++)
                {// transfer to comms buffer
                        TxBuffer[MyUART][Index]= *Data;
                        Data++;
                }
                TxBufferCounter[MyUART]= Size;
                // ready to start tx
                TxBufferSent[MyUART]=1;
                TxBufferCounter[MyUART]--;
                UART_WriteByte(MyUART ,TxBuffer[MyUART][0]);
                return(N_TRUE);
        }
        else
                return(N_FALSE);
}

//-----------[O]----------//

void UARTAdapterTimers(void)
{
        N_U08 Index;

        for(Index=0;Index<N_AVAIL_UARTS;Index++)
        {
                if(Countdown_RxReady[Index])
                {
                        Countdown_RxReady[Index]--;
                        if(!Countdown_RxReady[Index])
                        {
                                RxReady[Index]=N_TRUE;
                        }
                }
        }
}

//-----------[O]----------//

/*
 * This is the injection interface used by remote test harness to simulate hardware subsystem
 *
 *
 *
 */

N_BOOL UARTAdapterSetInjectionData(N_U16 * Data, N_U16 Size, N_U16 * Resolution)
{
        return(N_TRUE);

}

//-----------[O]----------//

N_BOOL UARTAdapterGetEjectionData(N_U16 * Data,  N_U16 * Size)
{
        return(N_TRUE);
}
//-----------[O]----------//

N_BOOL UARTAdapterRecordTestCaseData(N_U16 * Data, N_U16 * Size, N_U16 * Resolution)
{
        return(N_TRUE);

}
//-----------[O]----------//

N_BOOL UARTAdapterStartInjection(void)
{
        return(N_TRUE);

}

//-----------[O]----------//

N_BOOL UARTAdapterStopInjection(void)
{
        return(N_TRUE);

}

//-----------[O]----------//

N_BOOL UARTAdapterPauseInjection(void)
{
        return(N_TRUE);

}

//-----------[O]----------//

N_U16 UARTAdapterInjectionStatus(void)
{
        return((N_U16)0);
}


/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/
