/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: .c   
 * Purpose: 
 * Version: V1.0
 * Author: M Watson
 * Version 
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/
//typedef enum {
//        N_UART_BAUDRATE_1000000   = 11,
//        N_UART_BAUDRATE_500000    = 10,
//        N_UART_BAUDRATE_230400    = 9,
//        N_UART_BAUDRATE_115200    = 8,
//        N_UART_BAUDRATE_57600     = 7,
//        N_UART_BAUDRATE_38400     = 6,
//        N_UART_BAUDRATE_28800     = 5,
//        N_UART_BAUDRATE_19200     = 4,
//        N_UART_BAUDRATE_14400     = 3,
//        N_UART_BAUDRATE_9600      = 2,
//        N_UART_BAUDRATE_4800      = 1,
//} N_UART_BAUDRATE;


/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/
