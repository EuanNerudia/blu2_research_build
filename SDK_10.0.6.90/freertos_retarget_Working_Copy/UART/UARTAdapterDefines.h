/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: .c   
 * Purpose: 
 * Version: V1.0
 * Author: M Watson
 * Version 
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/
#define MAX_UARTS 3

#define UART_RFF  4
#define UART_RFNE 3
#define UART_TFE  2
#define UART_TFNF 1
#define UART_BUSY 0

#define N_UART1 0
#define N_UART2 1
#define N_UART3 2


#define UART_MAX_PACKET_SIZE 230
#define N_AVAIL_UARTS 3

#define UART_INT(id) ((id) == HW_UART1 ? (UART_IRQn) : ((id) == HW_UART2 ? (UART2_IRQn) : (UART3_IRQn)))

/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/
