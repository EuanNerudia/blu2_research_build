/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: .c   
 * Purpose: 
 * Version: V1.0
 * Author: M Watson
 * Version 
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/
#include <hw_gpio.h>

//#include "UARTAdapterTypes.h"

bool UARTAdapter_WakingUp(void);
bool UARTAdapter_ReadyToSleep(void);
void UARTAdapter_CancelSleep(void);

void SetOffsetToLength(N_U08 MyUART, N_U08 Offset);
bool ConfigureUART_GPIO(N_U08 id, HW_GPIO_PORT tx_port, HW_GPIO_PIN tx_pin, HW_GPIO_PORT rx_port, HW_GPIO_PIN rx_pin, N_U08 Power);
void SetUARTBaudRate(N_U08 MyUART, N_U08 Baud);
N_BOOL UARTDataReady(N_U08 MyUART, N_U32 * Size);
void FetchUARTDataPacket(N_U08 MyUART, N_U32 * Size, N_U08 * Data);
N_BOOL SendUARTData(N_U08 MyUART, N_U08 Size, N_U08 * Data);
void UART_SetTimeout(N_U08 MyUART, N_U08 Timeout);

void InitialiseUARTAdpater(void);
void UARTAdapterTimers(void);
N_BOOL UARTAdapterSetInjectionData(N_U16 * Data, N_U16 Size, N_U16 * Resolution);
N_BOOL UARTAdapterGetEjectionData(N_U16 * Data,  N_U16 * Size);
N_BOOL UARTAdapterRecordTestCaseData(N_U16 * Data, N_U16 * Size, N_U16 * Resolution);
N_BOOL UARTAdapterStartInjection(void);
N_BOOL UARTAdapterStopInjection(void);
N_BOOL UARTAdapterPauseInjection(void);
N_U16 UARTAdapterInjectionStatus(void);
void RxComplete(N_U08 MyUART);
/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/
