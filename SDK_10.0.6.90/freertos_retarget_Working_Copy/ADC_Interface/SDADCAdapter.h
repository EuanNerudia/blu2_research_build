/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: SDADCAdapter.h
 * Purpose:
 * Version: V1.0
 * Author: E Denton
 * Version
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/

typedef enum {
        SD_PIN_8 = HW_GPIO_PIN_8,      /**< GPIO Pin 8 */
        SD_PIN_9 = HW_GPIO_PIN_9,      /**< GPIO Pin 9 */
        SD_PIN_20 = HW_GPIO_PIN_20,    /**< GPIO Pin 20 */
        SD_PIN_21 = HW_GPIO_PIN_21,    /**< GPIO Pin 21 */
        SD_PIN_22 = HW_GPIO_PIN_22,    /**< GPIO Pin 22 */
        SD_PIN_25 = HW_GPIO_PIN_25,    /**< GPIO Pin 25 */
        SD_PIN_MAX         /**< GPIO Pin max*/
} SD_ADC_Pins;

typedef enum {
        SD_PORT_0 = HW_GPIO_PORT_0,     /**< GPIO Port 0 */
        SD_PORT_1 = HW_GPIO_PORT_1,     /**< GPIO Port 1 */
        SD_PORT_MAX        /**< GPIO Port max */
} SD_ADC_Ports;

typedef struct {
        SD_ADC_Ports port;
        SD_ADC_Pins  pin;
} SD_Pin_Con;

void SD_IO_Con(void);

void SD_Config(SD_ADC_Config SD_ADC);

void SD_ADC_Start(hw_gpadc_interrupt_cb cb);

void SD_ADC_Stop(SD_ADC_Config SD_ADC);

void SD_ADC_Close(SD_ADC_Config SD_ADC);

/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/
