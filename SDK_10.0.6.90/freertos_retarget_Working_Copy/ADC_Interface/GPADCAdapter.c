/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: GPADCAdapter.c
 * Purpose:
 * Version: V1.0
 * Author: E Denton
 * Version
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/

#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include "osal.h"
#include "hw_cpm.h"
#include "hw_pdc.h"
#include "hw_sys.h"
#include "hw_wkup.h"
#include "resmgmt.h"
#include "hw_gpio.h"
#include "ad_gpadc.h"
#include "hw_gpadc.h"
#include "hw_watchdog.h"
#include "sys_clock_mgr.h"
#include "sys_power_mgr.h"

#include "ADCAdapter.h"
#include "GPADCAdapter.h"

#define PX_SET_DATA_REG_ADDR(_port)     ((volatile uint32_t *)(GPIO_BASE + offsetof(GPIO_Type, P0_SET_DATA_REG)) + _port)
#define PX_SET_DATA_REG(_port)          *PX_SET_DATA_REG_ADDR(_port)
#define PX_RESET_DATA_REG_ADDR(_port)   ((volatile uint32_t *)(GPIO_BASE + offsetof(GPIO_Type, P0_RESET_DATA_REG)) + _port)
#define PX_RESET_DATA_REG(_port)        *PX_RESET_DATA_REG_ADDR(_port)
#define PXX_MODE_REG_ADDR(_port, _pin)  ((volatile uint32_t *)(GPIO_BASE + offsetof(GPIO_Type, P0_00_MODE_REG)) + (_port * 32)  + _pin)
#define PXX_MODE_REG(_port, _pin)       *PXX_MODE_REG_ADDR(_port, _pin)

static hw_gpadc_interrupt_cb GP_intr_cb = NULL;

int GP_IO_Check[8] = { 1, 1, 1, 1, 1, 1, 1, 1 };

GP_Pin_Con GP_Channel[8] = {
        {
                .port = GP_PORT_0,
                .pin  = GP_PIN_8,
        },
        {
                .port = GP_PORT_0,
                .pin  = GP_PIN_9,
        },
        {
                .port = GP_PORT_0,
                .pin  = GP_PIN_25,
        },
        {
                .port = GP_PORT_1,
                .pin  = GP_PIN_9,
        },
        {
                .port = GP_PORT_1,
                .pin  = GP_PIN_12,
        },
        {
                .port = GP_PORT_1,
                .pin  = GP_PIN_13,
        },
        {
                .port = GP_PORT_1,
                .pin  = GP_PIN_18,
        },
        {
                .port = GP_PORT_1,
                .pin  = GP_PIN_19,
        }
};


//-----------[O]----------//

void GP_IO_Con(void)
{
        GP_Pin_Con To_Con[8];

        int position = 0;

        for (int i=0; i<8; i++)
        {
                if (GP_IO_Check[i] == 1)
                {
                        To_Con[position] = GP_Channel[i];
                        position++;
                }
        }

        if (position > 0)
        {
                hw_sys_pd_com_enable();

                for (int i=0; i<(position); i++)
                {
                        PXX_MODE_REG(To_Con[i].port, To_Con[i].pin) = HW_GPIO_MODE_INPUT | HW_GPIO_FUNC_ADC;
                        PX_SET_DATA_REG(To_Con[i].port) = 1 << To_Con[i].pin;

                        if (To_Con[i].port == 0)
                        {
                                CRG_TOP->P0_SET_PAD_LATCH_REG = 1 << To_Con[i].pin;
                                CRG_TOP->P0_RESET_PAD_LATCH_REG = 1 << To_Con[i].pin;
                        }
                        else
                        {
                                CRG_TOP->P1_SET_PAD_LATCH_REG = 1 << To_Con[i].pin;
                                CRG_TOP->P1_RESET_PAD_LATCH_REG = 1 << To_Con[i].pin;
                        }
                }

                hw_sys_pd_com_disable();
        }
}

//-----------[O]----------//

void ADC_Handler(void)
{
        if (GP_intr_cb)
        {
                GP_intr_cb();
        }
        else
        {
                hw_gpadc_clear_interrupt();
        }
}

//-----------[O]----------//

// The below is used the same as GPADC open
void GP_Config(GP_ADC_Config GP_ADC)
{
        hw_sys_pd_periph_enable();
        hw_sys_pd_com_enable();

        // Reset the GPADC to ensure the config sticks
        GPADC->GP_ADC_CTRL_REG = REG_MSK(GPADC, GP_ADC_CTRL_REG, GP_ADC_EN);
        GPADC->GP_ADC_CTRL2_REG = 0;
        GPADC->GP_ADC_CTRL3_REG = 0x40;      // default value for GP_ADC_EN_DEL

        //Configure the GPADC register for the desired function
        REG_SETF(GPADC, GP_ADC_CTRL_REG, GP_ADC_SEL, GP_ADC.driver.input);
        REG_SETF(GPADC, GP_ADC_CTRL_REG, GP_ADC_CLK_SEL, GP_ADC.driver.clock);
        REG_SETF(GPADC, GP_ADC_CTRL_REG, GP_ADC_SE, GP_ADC.driver.input_mode);
        REG_SETF(GPADC, GP_ADC_CTRL_REG, GP_ADC_CHOP, !!GP_ADC.driver.chopping);
        REG_SETF(GPADC, GP_ADC_CTRL_REG, GP_ADC_CONT, !!GP_ADC.driver.continous);
        REG_SETF(GPADC, GP_ADC_CTRL3_REG, GP_ADC_INTERVAL, GP_ADC.driver.temp_sensor);
        REG_SETF(GPADC, GP_ADC_CTRL2_REG, GP_ADC_SMPL_TIME, GP_ADC.driver.sample_time);
        REG_SETF(GPADC, GP_ADC_CTRL2_REG, GP_ADC_CONV_NRS, GP_ADC.driver.oversampling);
        REG_SETF(GPADC, GP_ADC_CTRL2_REG, GP_ADC_ATTN3X, !!GP_ADC.driver.input_attenuator);

        if (GP_ADC.driver.temp_sensor == 5)
        {
                REG_SETF(GPADC, GP_ADC_CTRL_REG, GP_ADC_DIFF_TEMP_EN, !!true);

                REG_SETF(GPADC, GP_ADC_CTRL_REG, GP_ADC_DIFF_TEMP_SEL, GP_ADC.driver.temp_sensor & 0x03);
        }

        // Pad latch
        //CRG_TOP->P1_SET_PAD_LATCH_REG = 1 << GP_ADC.io_bus.input0.pin;

        hw_sys_pd_com_disable();
        hw_sys_pd_periph_disable();
}

//-----------[O]----------//

void GP_ADC_Start(hw_gpadc_interrupt_cb cb)
{
        hw_sys_pd_periph_enable();
        hw_sys_pd_com_enable();

        GP_intr_cb = cb;

        REG_SETF(GPADC, GP_ADC_CTRL_REG, GP_ADC_MINT, 1);

        NVIC_EnableIRQ(GPADC_IRQn);

        REG_SETF(GPADC, GP_ADC_CTRL_REG, GP_ADC_START, 1);
}

//-----------[O]----------//

void GP_ADC_Stop(GP_ADC_Config GP_ADC)
{
        // Unregister interrupt
        NVIC_DisableIRQ(GPADC_IRQn);

        REG_SETF(GPADC, GP_ADC_CTRL_REG, GP_ADC_MINT, 0);

        // Clear interrupt
        GPADC->GP_ADC_CLEAR_INT_REG = 1;

        // Reset ADC
        GPADC->GP_ADC_CTRL_REG = REG_MSK(GPADC, GP_ADC_CTRL_REG, GP_ADC_EN);
        GPADC->GP_ADC_CTRL2_REG = 0;
        GPADC->GP_ADC_CTRL3_REG = 0x40;      // default value for GP_ADC_EN_DEL

        // Disable Pad Latch
        //CRG_TOP->P1_RESET_PAD_LATCH_REG = 1 << GP_ADC.io_bus.input0.pin;

        hw_sys_pd_com_disable();
        hw_sys_pd_periph_disable();
}

//-----------[O]----------//

void GP_ADC_Close(GP_ADC_Config GP_ADC)
{
        // Unregister interrupt
        //NVIC_DisableIRQ(GPADC_IRQn);

        REG_SETF(GPADC, GP_ADC_CTRL_REG, GP_ADC_MINT, 0);

        // Clear interrupt
        GPADC->GP_ADC_CLEAR_INT_REG = 1;

        // Disable Pad Latch
        //CRG_TOP->P1_RESET_PAD_LATCH_REG = 1 << GP_ADC.io_bus.input0.pin;

        hw_sys_pd_com_disable();
        hw_sys_pd_periph_disable();
}

//-----------[O]----------//

/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/

