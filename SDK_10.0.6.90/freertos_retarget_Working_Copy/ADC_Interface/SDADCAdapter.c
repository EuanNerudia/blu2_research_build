/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: SDADCAdapter.c
 * Purpose:
 * Version: V1.0
 * Author: E Denton
 * Version
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/

#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include "osal.h"
#include "hw_cpm.h"
#include "hw_pdc.h"
#include "hw_sys.h"
#include "hw_wkup.h"
#include "resmgmt.h"
#include "hw_gpio.h"
#include "ad_sdadc.h"
#include "hw_watchdog.h"
#include "sys_clock_mgr.h"
#include "sys_power_mgr.h"

#include "ADCAdapter.h"
#include "SDADCAdapter.h"

#define PX_SET_DATA_REG_ADDR(_port)     ((volatile uint32_t *)(GPIO_BASE + offsetof(GPIO_Type, P0_SET_DATA_REG)) + _port)
#define PX_SET_DATA_REG(_port)          *PX_SET_DATA_REG_ADDR(_port)
#define PX_RESET_DATA_REG_ADDR(_port)   ((volatile uint32_t *)(GPIO_BASE + offsetof(GPIO_Type, P0_RESET_DATA_REG)) + _port)
#define PX_RESET_DATA_REG(_port)        *PX_RESET_DATA_REG_ADDR(_port)
#define PXX_MODE_REG_ADDR(_port, _pin)  ((volatile uint32_t *)(GPIO_BASE + offsetof(GPIO_Type, P0_00_MODE_REG)) + (_port * 32)  + _pin)
#define PXX_MODE_REG(_port, _pin)       *PXX_MODE_REG_ADDR(_port, _pin)

static hw_sdadc_interrupt_cb SD_intr_cb = NULL;

bool ADC = false;

int SD_IO_Check[7] = { 1, 1, 1, 1, 1, 1, 1 };

SD_Pin_Con SD_Channel[7] = {
        {
                .port = SD_PORT_0,
                .pin  = SD_PIN_8,
        },
        {
                .port = SD_PORT_0,
                .pin  = SD_PIN_9,
        },
        {
                .port = SD_PORT_0,
                .pin  = SD_PIN_25,
        },
        {
                .port = SD_PORT_1,
                .pin  = SD_PIN_9,
        },
        {
                .port = SD_PORT_1,
                .pin  = SD_PIN_20,
        },
        {
                .port = SD_PORT_1,
                .pin  = SD_PIN_21,
        },
        {
                .port = SD_PORT_1,
                .pin  = SD_PIN_22,
        },
};

//-----------[O]----------//

void SD_IO_Con(void)
{
        SD_Pin_Con To_Con[7];

        int position = 0;

        for (int i=0; i<7; i++)
        {
                if (SD_IO_Check[i] == 1)
                {
                        To_Con[position] = SD_Channel[i];
                        position++;
                }
        }

        if (position > 0)
        {
                hw_sys_pd_com_enable();

                for (int i=0; i<(position); i++)
                {
                        PXX_MODE_REG(To_Con[i].port, To_Con[i].pin) = HW_GPIO_MODE_INPUT | HW_GPIO_FUNC_ADC;
                        PX_SET_DATA_REG(To_Con[i].port) = 1 << To_Con[i].pin;

                        if (To_Con[i].port == SD_PORT_0)
                        {
                                CRG_TOP->P0_SET_PAD_LATCH_REG = 1 << To_Con[i].pin;
                                CRG_TOP->P0_RESET_PAD_LATCH_REG = 1 << To_Con[i].pin;
                        }
                        else
                        {
                                CRG_TOP->P1_SET_PAD_LATCH_REG = 1 << To_Con[i].pin;
                                CRG_TOP->P1_RESET_PAD_LATCH_REG = 1 << To_Con[i].pin;
                        }
                }

                hw_sys_pd_com_disable();
        }
}

void SD_Config(SD_ADC_Config SD_ADC)
{
        // Set control register to default value "0" but keep enable bit set
        SDADC->SDADC_CTRL_REG = REG_MSK(SDADC, SDADC_CTRL_REG, SDADC_EN);

        REG_SETF(SDADC, SDADC_CTRL_REG, SDADC_SE, SD_ADC.driver.input_mode);
        REG_SETF(SDADC, SDADC_CTRL_REG, SDADC_INP_SEL, SD_ADC.driver.inp);
        REG_SETF(SDADC, SDADC_CTRL_REG, SDADC_INN_SEL, SD_ADC.driver.inn); // only used for diff mode
        REG_SETF(SDADC, SDADC_CTRL_REG, SDADC_CONT, !!SD_ADC.driver.continuous);
        REG_SETF(SDADC, SDADC_CTRL_REG, SDADC_OSR, SD_ADC.driver.over_sampling);
        REG_SETF(SDADC, SDADC_CTRL_REG, SDADC_VREF_SEL, SD_ADC.driver.vref_selection);
        REG_SETF(SDADC, SDADC_CTRL_REG, SDADC_DMA_EN, !!SD_ADC.driver.use_dma);
        // Mask either sets or disables the interupt at this point
        REG_CLR_BIT(SDADC, SDADC_CTRL_REG, SDADC_MINT);
        REG_SETF(SDADC, SDADC_TEST_REG, SDADC_CLK_FREQ, SD_ADC.driver.freq);

        hw_sys_pd_com_enable();
        if (SD_ADC.io_bus.input0.port == ADC_GPIO_PORT_0) {
                CRG_TOP->P0_SET_PAD_LATCH_REG = 1 << SD_ADC.io_bus.input0.pin;
        } else if (SD_ADC.io_bus.input0.port == ADC_GPIO_PORT_1) {
                CRG_TOP->P1_SET_PAD_LATCH_REG = 1 << SD_ADC.io_bus.input0.pin;
        }
        hw_sys_pd_com_disable();
}

void SD_ADC_Dry_Scanning(hw_gpadc_interrupt_cb cb)
{
        ADC_io_conf SD_io_configP0 = {
                .port = ADC_GPIO_PORT_0,
                .pin  = ADC_GPIO_PIN_25,
                .on   = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_ADC,  INPUT_TRUE},
                .off  = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_GPIO, INPUT_TRUE}
        };

        ADC_io_conf SD_io_configP1 = {
                .port = ADC_GPIO_PORT_MAX,
                .pin  = ADC_GPIO_PIN_MAX,
                .on   = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_GPIO,  INPUT_TRUE},
                .off  = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_GPIO, INPUT_TRUE}
        };

        ADC_BUS SD_ADC_io_bus = {
                .input0        = SD_io_configP0,
                .input1        = SD_io_configP1,
                .voltage_level = ADC_GPIO_POWER_VDD1V8P,
        };

        SD_ADC_driver_config SD_Driver_1 = {
                .inn                    = SD_ADC_IN_ADC1_P0_25,
                .inp                    = SD_ADC_IN_ADC1_P0_25,
                .clock                  = SD_ADC_CLOCK_0,
                .freq                   = SD_ADC_CLOCK_FREQ_1M,
                .use_dma                = SD_ADC_DMA_FALSE,
                .mask_int               = SD_ADC_MASK_FALSE,
                .input_mode             = SD_ADC_INPUT_MODE_SINGLE_ENDED,
                .continuous             = ADC_CONTINUOUS_FALSE,
                .vref_voltage           = SD_ADC_VREF_VOLTAGE_MAX,
                .over_sampling          = SD_ADC_OSR_128,
                .vref_selection         = SD_ADC_VREF_INTERNAL,
        };

        SD_ADC_driver_config SD_Driver_2 = {
                .inn                    = HW_SDADC_IN_ADC5_P1_20,
                .inp                    = HW_SDADC_IN_ADC5_P1_20,
                .clock                  = SD_ADC_CLOCK_0,
                .freq                   = SD_ADC_CLOCK_FREQ_1M,
                .use_dma                = SD_ADC_DMA_FALSE,
                .mask_int               = SD_ADC_MASK_FALSE,
                .input_mode             = SD_ADC_INPUT_MODE_SINGLE_ENDED,
                .continuous             = ADC_CONTINUOUS_FALSE,
                .vref_voltage           = SD_ADC_VREF_VOLTAGE_MAX,
                .over_sampling          = SD_ADC_OSR_128,
                .vref_selection         = SD_ADC_VREF_INTERNAL,
        };

        SD_ADC_Config SD_Config_1 = {
                .error      = 0,
                .io_bus     = SD_ADC_io_bus,
                .driver     = SD_Driver_1,
                .configured = false,
        };

        SD_ADC_Config SD_Config_2 = {
                .error      = 0,
                .io_bus     = SD_ADC_io_bus,
                .driver     = SD_Driver_2,
                .configured = false,
        };

        if (ADC)
        {
                SD_Config(SD_Config_2);

                ADC = !ADC;

        }
        else
        {
                SD_Config(SD_Config_1);

                ADC = !ADC;
        }

        SD_ADC_Start(cb);
}

void SD_ADC_Start(hw_gpadc_interrupt_cb cb)
{
        hw_sys_pd_com_enable();

        hw_sdadc_register_interrupt(cb);

        REG_SETF(SDADC, SDADC_CTRL_REG, SDADC_START, 1);
}


void SD_ADC_Stop(SD_ADC_Config SD_ADC)
{
        hw_sdadc_unregister_interrupt();

        SDADC->SDADC_CTRL_REG = REG_MSK(SDADC, SDADC_CTRL_REG, SDADC_EN);

        // Disable Pad Latch
        if (SD_ADC.io_bus.input0.port == ADC_GPIO_PORT_0) {
                CRG_TOP->P0_SET_PAD_LATCH_REG = 1 << SD_ADC.io_bus.input0.pin;
        } else if (SD_ADC.io_bus.input0.port == ADC_GPIO_PORT_1) {
                CRG_TOP->P1_RESET_PAD_LATCH_REG = 1 << SD_ADC.io_bus.input0.pin;
        }

        // Clear all and reset SD_ADC

        hw_sys_pd_com_disable();
        hw_sys_pd_periph_disable();
}

void SD_ADC_Close(SD_ADC_Config SD_ADC)
{
        REG_SETF(SDADC, SDADC_CTRL_REG, SDADC_MINT, 0);

        hw_sys_pd_com_disable();
}
//-----------[O]----------//

/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/
