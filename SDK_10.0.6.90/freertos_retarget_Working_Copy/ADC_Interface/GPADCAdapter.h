/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: GPADCAdapter.h
 * Purpose:
 * Version: V1.0
 * Author: E Denton
 * Version
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/

typedef enum {
        GP_PIN_8  = HW_GPIO_PIN_8,
        GP_PIN_9  = HW_GPIO_PIN_9,
        GP_PIN_12 = HW_GPIO_PIN_12,    /**< GPIO Pin 12 */
        GP_PIN_13 = HW_GPIO_PIN_13,    /**< GPIO Pin 13 */
        GP_PIN_18 = HW_GPIO_PIN_18 ,    /**< GPIO Pin 18 */
        GP_PIN_19 = HW_GPIO_PIN_19,    /**< GPIO Pin 19 */
        GP_PIN_25 = HW_GPIO_PIN_25,
        GP_PIN_MAX         /**< GPIO Pin max*/
} GP_ADC_Pins;

typedef enum {
        GP_PORT_0 = HW_GPIO_PORT_0,
        GP_PORT_1 = HW_GPIO_PORT_1,     /**< GPIO Port 1 */
        GP_PORT_MAX        /**< GPIO Port max */
} GP_ADC_Port;

typedef struct {
        GP_ADC_Port port;
        GP_ADC_Pins  pin;
} GP_Pin_Con;

void GP_IO_Con(void);

void GP_Config(GP_ADC_Config GP_ADC);

void GP_ADC_Start(hw_gpadc_interrupt_cb cb);

void GP_ADC_Stop(GP_ADC_Config GP_ADC);

void GP_ADC_Close(GP_ADC_Config GP_ADC);

/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/
