################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/cli_config_parser.c \
../src/cmd_handlers.c \
../src/ini_parser.c \
../src/main.c \
../src/opt_handlers.c \
../src/queue.c \
../src/util.c 

OBJS += \
./src/cli_config_parser.o \
./src/cmd_handlers.o \
./src/ini_parser.o \
./src/main.o \
./src/opt_handlers.o \
./src/queue.o \
./src/util.o 

C_DEPS += \
./src/cli_config_parser.d \
./src/cmd_handlers.d \
./src/ini_parser.d \
./src/main.d \
./src/opt_handlers.d \
./src/queue.d \
./src/util.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DDEVICE_DA1469x -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\cli_programmer\libprogrammer" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\uartboot\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\cli_programmer\cli\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\cli_programmer\libprogrammer\api" -O3 -Wall -Werror -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


