#ifndef CLI_VERSION_H_
#define CLI_VERSION_H_

#define CLI_VERSION_MAJOR       1
#define CLI_VERSION_MINOR       26

#define CLI_VERSION_MAJOR_STR   "1"
#define CLI_VERSION_MINOR_STR   "26"

#endif /* CLI_VERSION_H_ */
