################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../crc16.c \
../gdb_server_cmds.c \
../programmer.c \
../protocol_cmds.c \
../serial_win.c 

OBJS += \
./crc16.o \
./gdb_server_cmds.o \
./programmer.o \
./protocol_cmds.o \
./serial_win.o 

C_DEPS += \
./crc16.d \
./gdb_server_cmds.d \
./programmer.d \
./protocol_cmds.d \
./serial_win.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\cli_programmer\libprogrammer\api" -I"C:/Projects/SmartSnippets/SDK_10.0.6.90/utilities/cli_programmer/libprogrammer/../../../sdk/middleware/adapters/include" -I"C:/Projects/SmartSnippets/SDK_10.0.6.90/utilities/cli_programmer/libprogrammer/../../../sdk/bsp/include" -I"C:/Projects/SmartSnippets/SDK_10.0.6.90/utilities/cli_programmer/libprogrammer/../../../sdk/bsp/system/loaders/uartboot/include" -O3 -Wall -Werror -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


