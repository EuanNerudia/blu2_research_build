################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/curve25519_sandy2x.c \
../third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/fe51_invert.c \
../third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/fe_frombytes_sandy2x.c 

S_UPPER_SRCS += \
../third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/consts.S \
../third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/fe51_mul.S \
../third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/fe51_nsquare.S \
../third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/fe51_pack.S \
../third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/ladder.S \
../third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/ladder_base.S \
../third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/sandy2x.S 

OBJS += \
./third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/consts.o \
./third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/curve25519_sandy2x.o \
./third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/fe51_invert.o \
./third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/fe51_mul.o \
./third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/fe51_nsquare.o \
./third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/fe51_pack.o \
./third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/fe_frombytes_sandy2x.o \
./third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/ladder.o \
./third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/ladder_base.o \
./third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/sandy2x.o 

C_DEPS += \
./third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/curve25519_sandy2x.d \
./third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/fe51_invert.d \
./third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/fe_frombytes_sandy2x.d 


# Each subdirectory must supply rules for building sources it contributes
third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/%.o: ../third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: GCC Assembler'
	gcc -c -std=gnu99 -include libsodium_config.h -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include\sodium" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/%.o: ../third_party_crypto/libsodium/crypto_scalarmult/curve25519/sandy2x/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\api" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\mbedtls\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include\sodium" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include" -includelibsodium_config.h -O2 -Wall -Werror -c -fmessage-length=0 -Wno-unknown-pragmas -Wno-strict-aliasing -maes -mssse3 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


