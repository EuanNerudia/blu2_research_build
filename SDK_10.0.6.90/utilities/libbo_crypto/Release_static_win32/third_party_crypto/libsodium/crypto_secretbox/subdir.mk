################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../third_party_crypto/libsodium/crypto_secretbox/crypto_secretbox.c \
../third_party_crypto/libsodium/crypto_secretbox/crypto_secretbox_easy.c 

OBJS += \
./third_party_crypto/libsodium/crypto_secretbox/crypto_secretbox.o \
./third_party_crypto/libsodium/crypto_secretbox/crypto_secretbox_easy.o 

C_DEPS += \
./third_party_crypto/libsodium/crypto_secretbox/crypto_secretbox.d \
./third_party_crypto/libsodium/crypto_secretbox/crypto_secretbox_easy.d 


# Each subdirectory must supply rules for building sources it contributes
third_party_crypto/libsodium/crypto_secretbox/%.o: ../third_party_crypto/libsodium/crypto_secretbox/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\api" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\mbedtls\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include\sodium" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include" -includelibsodium_config.h -O2 -Wall -Werror -c -fmessage-length=0 -Wno-unknown-pragmas -Wno-strict-aliasing -maes -mssse3 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


