################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../third_party_crypto/libsodium/crypto_pwhash/argon2/argon2-core.c \
../third_party_crypto/libsodium/crypto_pwhash/argon2/argon2-encoding.c \
../third_party_crypto/libsodium/crypto_pwhash/argon2/argon2-fill-block-avx2.c \
../third_party_crypto/libsodium/crypto_pwhash/argon2/argon2-fill-block-ref.c \
../third_party_crypto/libsodium/crypto_pwhash/argon2/argon2-fill-block-ssse3.c \
../third_party_crypto/libsodium/crypto_pwhash/argon2/argon2.c \
../third_party_crypto/libsodium/crypto_pwhash/argon2/blake2b-long.c \
../third_party_crypto/libsodium/crypto_pwhash/argon2/pwhash_argon2i.c \
../third_party_crypto/libsodium/crypto_pwhash/argon2/pwhash_argon2id.c 

OBJS += \
./third_party_crypto/libsodium/crypto_pwhash/argon2/argon2-core.o \
./third_party_crypto/libsodium/crypto_pwhash/argon2/argon2-encoding.o \
./third_party_crypto/libsodium/crypto_pwhash/argon2/argon2-fill-block-avx2.o \
./third_party_crypto/libsodium/crypto_pwhash/argon2/argon2-fill-block-ref.o \
./third_party_crypto/libsodium/crypto_pwhash/argon2/argon2-fill-block-ssse3.o \
./third_party_crypto/libsodium/crypto_pwhash/argon2/argon2.o \
./third_party_crypto/libsodium/crypto_pwhash/argon2/blake2b-long.o \
./third_party_crypto/libsodium/crypto_pwhash/argon2/pwhash_argon2i.o \
./third_party_crypto/libsodium/crypto_pwhash/argon2/pwhash_argon2id.o 

C_DEPS += \
./third_party_crypto/libsodium/crypto_pwhash/argon2/argon2-core.d \
./third_party_crypto/libsodium/crypto_pwhash/argon2/argon2-encoding.d \
./third_party_crypto/libsodium/crypto_pwhash/argon2/argon2-fill-block-avx2.d \
./third_party_crypto/libsodium/crypto_pwhash/argon2/argon2-fill-block-ref.d \
./third_party_crypto/libsodium/crypto_pwhash/argon2/argon2-fill-block-ssse3.d \
./third_party_crypto/libsodium/crypto_pwhash/argon2/argon2.d \
./third_party_crypto/libsodium/crypto_pwhash/argon2/blake2b-long.d \
./third_party_crypto/libsodium/crypto_pwhash/argon2/pwhash_argon2i.d \
./third_party_crypto/libsodium/crypto_pwhash/argon2/pwhash_argon2id.d 


# Each subdirectory must supply rules for building sources it contributes
third_party_crypto/libsodium/crypto_pwhash/argon2/%.o: ../third_party_crypto/libsodium/crypto_pwhash/argon2/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\api" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\mbedtls\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include\sodium" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include" -includelibsodium_config.h -O2 -Wall -Werror -c -fmessage-length=0 -Wno-unknown-pragmas -Wno-strict-aliasing -maes -mssse3 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


