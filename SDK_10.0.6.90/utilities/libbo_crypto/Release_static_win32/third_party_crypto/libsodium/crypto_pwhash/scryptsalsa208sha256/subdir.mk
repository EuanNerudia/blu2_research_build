################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../third_party_crypto/libsodium/crypto_pwhash/scryptsalsa208sha256/crypto_scrypt-common.c \
../third_party_crypto/libsodium/crypto_pwhash/scryptsalsa208sha256/pbkdf2-sha256.c \
../third_party_crypto/libsodium/crypto_pwhash/scryptsalsa208sha256/pwhash_scryptsalsa208sha256.c \
../third_party_crypto/libsodium/crypto_pwhash/scryptsalsa208sha256/scrypt_platform.c 

OBJS += \
./third_party_crypto/libsodium/crypto_pwhash/scryptsalsa208sha256/crypto_scrypt-common.o \
./third_party_crypto/libsodium/crypto_pwhash/scryptsalsa208sha256/pbkdf2-sha256.o \
./third_party_crypto/libsodium/crypto_pwhash/scryptsalsa208sha256/pwhash_scryptsalsa208sha256.o \
./third_party_crypto/libsodium/crypto_pwhash/scryptsalsa208sha256/scrypt_platform.o 

C_DEPS += \
./third_party_crypto/libsodium/crypto_pwhash/scryptsalsa208sha256/crypto_scrypt-common.d \
./third_party_crypto/libsodium/crypto_pwhash/scryptsalsa208sha256/pbkdf2-sha256.d \
./third_party_crypto/libsodium/crypto_pwhash/scryptsalsa208sha256/pwhash_scryptsalsa208sha256.d \
./third_party_crypto/libsodium/crypto_pwhash/scryptsalsa208sha256/scrypt_platform.d 


# Each subdirectory must supply rules for building sources it contributes
third_party_crypto/libsodium/crypto_pwhash/scryptsalsa208sha256/%.o: ../third_party_crypto/libsodium/crypto_pwhash/scryptsalsa208sha256/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\api" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\mbedtls\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include\sodium" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include" -includelibsodium_config.h -O2 -Wall -Werror -c -fmessage-length=0 -Wno-unknown-pragmas -Wno-strict-aliasing -maes -mssse3 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


