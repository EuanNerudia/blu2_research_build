################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../third_party_crypto/libsodium/crypto_stream/aes128ctr/nacl/afternm_aes128ctr.c \
../third_party_crypto/libsodium/crypto_stream/aes128ctr/nacl/beforenm_aes128ctr.c \
../third_party_crypto/libsodium/crypto_stream/aes128ctr/nacl/consts_aes128ctr.c \
../third_party_crypto/libsodium/crypto_stream/aes128ctr/nacl/int128_aes128ctr.c \
../third_party_crypto/libsodium/crypto_stream/aes128ctr/nacl/stream_aes128ctr_nacl.c \
../third_party_crypto/libsodium/crypto_stream/aes128ctr/nacl/xor_afternm_aes128ctr.c 

OBJS += \
./third_party_crypto/libsodium/crypto_stream/aes128ctr/nacl/afternm_aes128ctr.o \
./third_party_crypto/libsodium/crypto_stream/aes128ctr/nacl/beforenm_aes128ctr.o \
./third_party_crypto/libsodium/crypto_stream/aes128ctr/nacl/consts_aes128ctr.o \
./third_party_crypto/libsodium/crypto_stream/aes128ctr/nacl/int128_aes128ctr.o \
./third_party_crypto/libsodium/crypto_stream/aes128ctr/nacl/stream_aes128ctr_nacl.o \
./third_party_crypto/libsodium/crypto_stream/aes128ctr/nacl/xor_afternm_aes128ctr.o 

C_DEPS += \
./third_party_crypto/libsodium/crypto_stream/aes128ctr/nacl/afternm_aes128ctr.d \
./third_party_crypto/libsodium/crypto_stream/aes128ctr/nacl/beforenm_aes128ctr.d \
./third_party_crypto/libsodium/crypto_stream/aes128ctr/nacl/consts_aes128ctr.d \
./third_party_crypto/libsodium/crypto_stream/aes128ctr/nacl/int128_aes128ctr.d \
./third_party_crypto/libsodium/crypto_stream/aes128ctr/nacl/stream_aes128ctr_nacl.d \
./third_party_crypto/libsodium/crypto_stream/aes128ctr/nacl/xor_afternm_aes128ctr.d 


# Each subdirectory must supply rules for building sources it contributes
third_party_crypto/libsodium/crypto_stream/aes128ctr/nacl/%.o: ../third_party_crypto/libsodium/crypto_stream/aes128ctr/nacl/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\api" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\mbedtls\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include\sodium" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include" -includelibsodium_config.h -O2 -Wall -Werror -c -fmessage-length=0 -Wno-unknown-pragmas -Wno-strict-aliasing -maes -mssse3 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


