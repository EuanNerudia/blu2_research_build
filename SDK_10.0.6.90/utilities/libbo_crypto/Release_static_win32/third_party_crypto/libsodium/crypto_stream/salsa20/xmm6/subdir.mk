################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../third_party_crypto/libsodium/crypto_stream/salsa20/xmm6/salsa20_xmm6.c 

S_UPPER_SRCS += \
../third_party_crypto/libsodium/crypto_stream/salsa20/xmm6/salsa20_xmm6-asm.S 

OBJS += \
./third_party_crypto/libsodium/crypto_stream/salsa20/xmm6/salsa20_xmm6-asm.o \
./third_party_crypto/libsodium/crypto_stream/salsa20/xmm6/salsa20_xmm6.o 

C_DEPS += \
./third_party_crypto/libsodium/crypto_stream/salsa20/xmm6/salsa20_xmm6.d 


# Each subdirectory must supply rules for building sources it contributes
third_party_crypto/libsodium/crypto_stream/salsa20/xmm6/%.o: ../third_party_crypto/libsodium/crypto_stream/salsa20/xmm6/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: GCC Assembler'
	gcc -c -std=gnu99 -include libsodium_config.h -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include\sodium" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

third_party_crypto/libsodium/crypto_stream/salsa20/xmm6/%.o: ../third_party_crypto/libsodium/crypto_stream/salsa20/xmm6/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\api" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\mbedtls\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include\sodium" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include" -includelibsodium_config.h -O2 -Wall -Werror -c -fmessage-length=0 -Wno-unknown-pragmas -Wno-strict-aliasing -maes -mssse3 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


