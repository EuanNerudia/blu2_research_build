################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../third_party_crypto/libsodium/crypto_sign/ed25519/ref10/keypair.c \
../third_party_crypto/libsodium/crypto_sign/ed25519/ref10/obsolete.c \
../third_party_crypto/libsodium/crypto_sign/ed25519/ref10/open.c \
../third_party_crypto/libsodium/crypto_sign/ed25519/ref10/sign.c 

OBJS += \
./third_party_crypto/libsodium/crypto_sign/ed25519/ref10/keypair.o \
./third_party_crypto/libsodium/crypto_sign/ed25519/ref10/obsolete.o \
./third_party_crypto/libsodium/crypto_sign/ed25519/ref10/open.o \
./third_party_crypto/libsodium/crypto_sign/ed25519/ref10/sign.o 

C_DEPS += \
./third_party_crypto/libsodium/crypto_sign/ed25519/ref10/keypair.d \
./third_party_crypto/libsodium/crypto_sign/ed25519/ref10/obsolete.d \
./third_party_crypto/libsodium/crypto_sign/ed25519/ref10/open.d \
./third_party_crypto/libsodium/crypto_sign/ed25519/ref10/sign.d 


# Each subdirectory must supply rules for building sources it contributes
third_party_crypto/libsodium/crypto_sign/ed25519/ref10/%.o: ../third_party_crypto/libsodium/crypto_sign/ed25519/ref10/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\api" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\mbedtls\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include\sodium" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include" -includelibsodium_config.h -O2 -Wall -Werror -c -fmessage-length=0 -Wno-unknown-pragmas -Wno-strict-aliasing -maes -mssse3 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


