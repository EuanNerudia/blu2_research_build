################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../third_party_crypto/libsodium/crypto_generichash/blake2b/ref/blake2b-compress-avx2.c \
../third_party_crypto/libsodium/crypto_generichash/blake2b/ref/blake2b-compress-ref.c \
../third_party_crypto/libsodium/crypto_generichash/blake2b/ref/blake2b-compress-sse41.c \
../third_party_crypto/libsodium/crypto_generichash/blake2b/ref/blake2b-compress-ssse3.c \
../third_party_crypto/libsodium/crypto_generichash/blake2b/ref/blake2b-ref.c \
../third_party_crypto/libsodium/crypto_generichash/blake2b/ref/generichash_blake2b.c 

OBJS += \
./third_party_crypto/libsodium/crypto_generichash/blake2b/ref/blake2b-compress-avx2.o \
./third_party_crypto/libsodium/crypto_generichash/blake2b/ref/blake2b-compress-ref.o \
./third_party_crypto/libsodium/crypto_generichash/blake2b/ref/blake2b-compress-sse41.o \
./third_party_crypto/libsodium/crypto_generichash/blake2b/ref/blake2b-compress-ssse3.o \
./third_party_crypto/libsodium/crypto_generichash/blake2b/ref/blake2b-ref.o \
./third_party_crypto/libsodium/crypto_generichash/blake2b/ref/generichash_blake2b.o 

C_DEPS += \
./third_party_crypto/libsodium/crypto_generichash/blake2b/ref/blake2b-compress-avx2.d \
./third_party_crypto/libsodium/crypto_generichash/blake2b/ref/blake2b-compress-ref.d \
./third_party_crypto/libsodium/crypto_generichash/blake2b/ref/blake2b-compress-sse41.d \
./third_party_crypto/libsodium/crypto_generichash/blake2b/ref/blake2b-compress-ssse3.d \
./third_party_crypto/libsodium/crypto_generichash/blake2b/ref/blake2b-ref.d \
./third_party_crypto/libsodium/crypto_generichash/blake2b/ref/generichash_blake2b.d 


# Each subdirectory must supply rules for building sources it contributes
third_party_crypto/libsodium/crypto_generichash/blake2b/ref/%.o: ../third_party_crypto/libsodium/crypto_generichash/blake2b/ref/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\api" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\mbedtls\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include\sodium" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include" -includelibsodium_config.h -O2 -Wall -Werror -c -fmessage-length=0 -Wno-unknown-pragmas -Wno-strict-aliasing -maes -mssse3 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


