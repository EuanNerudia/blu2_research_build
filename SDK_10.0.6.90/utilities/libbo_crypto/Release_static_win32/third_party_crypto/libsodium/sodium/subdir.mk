################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../third_party_crypto/libsodium/sodium/core.c \
../third_party_crypto/libsodium/sodium/runtime.c \
../third_party_crypto/libsodium/sodium/utils.c \
../third_party_crypto/libsodium/sodium/version.c 

OBJS += \
./third_party_crypto/libsodium/sodium/core.o \
./third_party_crypto/libsodium/sodium/runtime.o \
./third_party_crypto/libsodium/sodium/utils.o \
./third_party_crypto/libsodium/sodium/version.o 

C_DEPS += \
./third_party_crypto/libsodium/sodium/core.d \
./third_party_crypto/libsodium/sodium/runtime.d \
./third_party_crypto/libsodium/sodium/utils.d \
./third_party_crypto/libsodium/sodium/version.d 


# Each subdirectory must supply rules for building sources it contributes
third_party_crypto/libsodium/sodium/core.o: ../third_party_crypto/libsodium/sodium/core.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\api" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\mbedtls\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include\sodium" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include" -includelibsodium_config.h -O2 -Wall -Werror -c -fmessage-length=0 -Wno-implicit-function-declaration -Wno-unknown-pragmas -Wno-strict-aliasing -maes -mssse3 -MMD -MP -MF"$(@:%.o=%.d)" -MT"third_party_crypto/libsodium/sodium/core.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

third_party_crypto/libsodium/sodium/%.o: ../third_party_crypto/libsodium/sodium/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\api" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\mbedtls\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include\sodium" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include" -includelibsodium_config.h -O2 -Wall -Werror -c -fmessage-length=0 -Wno-unknown-pragmas -Wno-strict-aliasing -maes -mssse3 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

third_party_crypto/libsodium/sodium/utils.o: ../third_party_crypto/libsodium/sodium/utils.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\api" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\mbedtls\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include\sodium" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include" -includelibsodium_config.h -O2 -Wall -Werror -c -fmessage-length=0 -Wno-implicit-function-declaration -Wno-unknown-pragmas -Wno-strict-aliasing -maes -mssse3 -MMD -MP -MF"$(@:%.o=%.d)" -MT"third_party_crypto/libsodium/sodium/utils.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


