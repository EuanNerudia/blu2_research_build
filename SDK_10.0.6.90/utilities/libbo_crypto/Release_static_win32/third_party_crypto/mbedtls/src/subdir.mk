################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../third_party_crypto/mbedtls/src/aes.c \
../third_party_crypto/mbedtls/src/asn1parse.c \
../third_party_crypto/mbedtls/src/asn1write.c \
../third_party_crypto/mbedtls/src/bignum.c \
../third_party_crypto/mbedtls/src/ctr_drbg.c \
../third_party_crypto/mbedtls/src/ecdsa.c \
../third_party_crypto/mbedtls/src/ecp.c \
../third_party_crypto/mbedtls/src/ecp_curves.c \
../third_party_crypto/mbedtls/src/entropy.c \
../third_party_crypto/mbedtls/src/entropy_poll.c \
../third_party_crypto/mbedtls/src/hmac_drbg.c \
../third_party_crypto/mbedtls/src/md.c \
../third_party_crypto/mbedtls/src/md5.c \
../third_party_crypto/mbedtls/src/md_wrap.c \
../third_party_crypto/mbedtls/src/platform.c \
../third_party_crypto/mbedtls/src/sha1.c \
../third_party_crypto/mbedtls/src/sha256.c \
../third_party_crypto/mbedtls/src/sha512.c \
../third_party_crypto/mbedtls/src/timing.c 

OBJS += \
./third_party_crypto/mbedtls/src/aes.o \
./third_party_crypto/mbedtls/src/asn1parse.o \
./third_party_crypto/mbedtls/src/asn1write.o \
./third_party_crypto/mbedtls/src/bignum.o \
./third_party_crypto/mbedtls/src/ctr_drbg.o \
./third_party_crypto/mbedtls/src/ecdsa.o \
./third_party_crypto/mbedtls/src/ecp.o \
./third_party_crypto/mbedtls/src/ecp_curves.o \
./third_party_crypto/mbedtls/src/entropy.o \
./third_party_crypto/mbedtls/src/entropy_poll.o \
./third_party_crypto/mbedtls/src/hmac_drbg.o \
./third_party_crypto/mbedtls/src/md.o \
./third_party_crypto/mbedtls/src/md5.o \
./third_party_crypto/mbedtls/src/md_wrap.o \
./third_party_crypto/mbedtls/src/platform.o \
./third_party_crypto/mbedtls/src/sha1.o \
./third_party_crypto/mbedtls/src/sha256.o \
./third_party_crypto/mbedtls/src/sha512.o \
./third_party_crypto/mbedtls/src/timing.o 

C_DEPS += \
./third_party_crypto/mbedtls/src/aes.d \
./third_party_crypto/mbedtls/src/asn1parse.d \
./third_party_crypto/mbedtls/src/asn1write.d \
./third_party_crypto/mbedtls/src/bignum.d \
./third_party_crypto/mbedtls/src/ctr_drbg.d \
./third_party_crypto/mbedtls/src/ecdsa.d \
./third_party_crypto/mbedtls/src/ecp.d \
./third_party_crypto/mbedtls/src/ecp_curves.d \
./third_party_crypto/mbedtls/src/entropy.d \
./third_party_crypto/mbedtls/src/entropy_poll.d \
./third_party_crypto/mbedtls/src/hmac_drbg.d \
./third_party_crypto/mbedtls/src/md.d \
./third_party_crypto/mbedtls/src/md5.d \
./third_party_crypto/mbedtls/src/md_wrap.d \
./third_party_crypto/mbedtls/src/platform.d \
./third_party_crypto/mbedtls/src/sha1.d \
./third_party_crypto/mbedtls/src/sha256.d \
./third_party_crypto/mbedtls/src/sha512.d \
./third_party_crypto/mbedtls/src/timing.d 


# Each subdirectory must supply rules for building sources it contributes
third_party_crypto/mbedtls/src/%.o: ../third_party_crypto/mbedtls/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\api" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\mbedtls\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include\sodium" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include" -includelibsodium_config.h -O2 -Wall -Werror -c -fmessage-length=0 -Wno-unknown-pragmas -Wno-strict-aliasing -maes -mssse3 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

third_party_crypto/mbedtls/src/ctr_drbg.o: ../third_party_crypto/mbedtls/src/ctr_drbg.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\api" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\mbedtls\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include\sodium" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include" -includelibsodium_config.h -O2 -Wall -Werror -c -fmessage-length=0 -Wno-int-conversion -Wno-implicit-function-declaration -Wno-unknown-pragmas -Wno-strict-aliasing -maes -mssse3 -MMD -MP -MF"$(@:%.o=%.d)" -MT"third_party_crypto/mbedtls/src/ctr_drbg.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

third_party_crypto/mbedtls/src/entropy.o: ../third_party_crypto/mbedtls/src/entropy.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\api" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\mbedtls\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include\sodium" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include" -includelibsodium_config.h -O2 -Wall -Werror -c -fmessage-length=0 -Wno-int-conversion -Wno-implicit-function-declaration -Wno-unknown-pragmas -Wno-strict-aliasing -maes -mssse3 -MMD -MP -MF"$(@:%.o=%.d)" -MT"third_party_crypto/mbedtls/src/entropy.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

third_party_crypto/mbedtls/src/hmac_drbg.o: ../third_party_crypto/mbedtls/src/hmac_drbg.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\api" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\mbedtls\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include\sodium" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include" -includelibsodium_config.h -O2 -Wall -Werror -c -fmessage-length=0 -Wno-int-conversion -Wno-implicit-function-declaration -Wno-unknown-pragmas -Wno-strict-aliasing -maes -mssse3 -MMD -MP -MF"$(@:%.o=%.d)" -MT"third_party_crypto/mbedtls/src/hmac_drbg.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

third_party_crypto/mbedtls/src/md.o: ../third_party_crypto/mbedtls/src/md.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\api" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\mbedtls\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include\sodium" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\utilities\libbo_crypto\third_party_crypto\libsodium\include" -includelibsodium_config.h -O2 -Wall -Werror -c -fmessage-length=0 -Wno-int-conversion -Wno-implicit-function-declaration -Wno-unknown-pragmas -Wno-strict-aliasing -maes -mssse3 -MMD -MP -MF"$(@:%.o=%.d)" -MT"third_party_crypto/mbedtls/src/md.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


