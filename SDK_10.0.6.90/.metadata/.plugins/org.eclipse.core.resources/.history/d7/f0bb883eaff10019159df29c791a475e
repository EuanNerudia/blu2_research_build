/*
 * platform_devices.c
 *
 *  Created on: 15 Oct 2019
 *      Author: Euan Denton
 */

#include <ad_gpadc.h>

#include "peripheral_setup.h"
#include "platform_devices.h"

/*
 * PLATFORM PERIPHERALS GPIO CONFIGURATION
 *****************************************************************************************
 */

#if dg_configGPADC_ADAPTER || dg_configUSE_HW_GPADC

// The pins that can be used as well as their ports
// are detailed below.

// Port 0 | Pin 8
// Port 0 | Pin 9
// Port 0 | Pin 25

// Port 1 | Pin 12
// Port 1 | Pin 13
// Port 1 | Pin 18
// Port 1 | Pin 19

/* GPADC IO */
const ad_gpadc_io_conf_t bus_GPADC = {
        .input0 = {
                .port = HW_GPIO_PORT_0,
                .pin  = HW_GPIO_PIN_25,
                .on   = {HW_GPIO_MODE_INPUT, HW_GPIO_FUNC_ADC,  true},
                .off  = {HW_GPIO_MODE_INPUT, HW_GPIO_FUNC_GPIO, true}
        },
        .input1 = {
                .port = HW_GPIO_PORT_NONE,
                .pin  = HW_GPIO_PIN_NONE,
                .on   = {HW_GPIO_MODE_INPUT, HW_GPIO_FUNC_GPIO, true},
                .off  = {HW_GPIO_MODE_INPUT, HW_GPIO_FUNC_GPIO, true}
        },
        .voltage_level = HW_GPIO_POWER_V33
};

/* GPADC driver configurations */
const ad_gpadc_driver_conf_t drv_GPADC = {
        .clock                  = HW_GPADC_CLOCK_INTERNAL, /* high-speed clock */
        .input_mode             = HW_GPADC_INPUT_MODE_SINGLE_ENDED,
        /* Analog pins are mapped on specific pins on DA1469x */
        .input                  = HW_GPADC_INPUT_SE_P0_25,
        .temp_sensor            = HW_GPADC_NO_TEMP_SENSOR,
        .sample_time            = 0,
        .continous              = false,
        .chopping               = true, /* Useful for DC and slowly changing signals */
        .oversampling           = HW_GPADC_OVERSAMPLING_64_SAMPLES,
        .input_attenuator       = HW_GPADC_INPUT_VOLTAGE_UP_TO_1V2,
};


/*  External device/module configurations */
const ad_gpadc_controller_conf_t dev_GPADC = {
        .id  = HW_GPADC_1, /* GPADC controller instance */
        .io  = &bus_GPADC,
        .drv = &drv_GPADC
};


gpadc_device POT_DEVICE = &dev_GPADC;

#endif /* dg_configGPADC_ADAPTER || dg_configUSE_HW_GPADC */
