/*
 * I2CAdapter.c
 *
 *  Created on: 21 Nov 2019
 *      Author: Euan Denton
 */

#include "hw_pdc.h"
#include "hw_i2c.h"
#include "ad_i2c.h"
#include "hw_sys.h"

#include "I2CAdapter.h"

const ad_i2c_io_conf_t i2c_Io_Con = {
        .scl = {
                .port = HW_GPIO_PORT_0, .pin = HW_GPIO_PIN_30,
                .on =  { HW_GPIO_MODE_OUTPUT_OPEN_DRAIN, HW_GPIO_FUNC_I2C_SCL, false },
                .off = { HW_GPIO_MODE_INPUT,             HW_GPIO_FUNC_GPIO,    true  }
        },
        .sda = {
                .port = HW_GPIO_PORT_0, .pin =HW_GPIO_PIN_31,
                .on =  { HW_GPIO_MODE_OUTPUT_OPEN_DRAIN, HW_GPIO_FUNC_I2C_SDA, false },
                .off = { HW_GPIO_MODE_INPUT,             HW_GPIO_FUNC_GPIO,    true  }
        },
        .voltage_level = HW_GPIO_POWER_VDD1V8P
};

const ad_i2c_driver_conf_t drv_EEPROM_24FC256 = {
        I2C_DEFAULT_CLK_CFG,
        .i2c.speed              = HW_I2C_SPEED_STANDARD,
        .i2c.mode               = HW_I2C_MODE_MASTER,
        .i2c.addr_mode          = HW_I2C_ADDRESSING_7B,
        .i2c.address            = 0b0000000,
        .dma_channel            = HW_DMA_CHANNEL_2
};

/* EEPROM 24FC256 I2C controller configuration */
const ad_i2c_controller_conf_t dev_24FC256 = {
        .id     = HW_I2C1,
        .io     = &i2c_Io_Con,
        .drv    = &drv_EEPROM_24FC256
};

static _Bool Configured = false;

const HW_I2C_ID Local_I2C_ID = HW_I2C1;

N_U08 ReadDirect(int start)
{
        if (start)
        {
                IBA(Local_I2C_ID)->I2C_DATA_CMD_REG = 0x14 & I2C_I2C_DATA_CMD_REG_I2C_DAT_Msk | I2C_I2C_DATA_CMD_REG_I2C_RESTART_Msk;
        }

        N_U08 Value = HW_I2C_REG_GETF(Local_I2C_ID, I2C_DATA_CMD, I2C_DAT);

        IBA(Local_I2C_ID)->I2C_DATA_CMD_REG = I2C_I2C_DATA_CMD_REG_I2C_CMD_Msk;

        return Value;
}

N_U08 Read_8Bit(N_U08 Register)
{
        IBA(Local_I2C_ID)->I2C_DATA_CMD_REG = Register; //& I2C_I2C_DATA_CMD_REG_I2C_DAT_Msk | I2C_I2C_DATA_CMD_REG_I2C_RESTART_Msk;
        N_U08 Value = HW_I2C_REG_GETF(Local_I2C_ID, I2C_DATA_CMD, I2C_DAT);
        //IBA(Local_I2C_ID)->I2C_DATA_CMD_REG = I2C_I2C_DATA_CMD_REG_I2C_STOP_Msk; //I2C_I2C_DATA_CMD_REG_I2C_CMD_Msk | I2C_I2C_DATA_CMD_REG_I2C_STOP_Msk;

        return Value;
}

N_U16 Read_16Bit(uint8_t Register1, uint8_t Register2)
{
        volatile N_U08 Buffer[2];

        Buffer[0] = Read_8Bit(Register1);
        Buffer[1] = Read_8Bit(Register2);

        // the int Word is used to build the full 16bit word from the two 8bit register reads.
        volatile N_U16 Word = Buffer[1] << 8 | Buffer[0];

        return Word;
}

N_U32 Read_24Bit(uint8_t Register1, uint8_t Register2, uint8_t Register3)
{
        volatile N_U08 Buffer[3];

        Buffer[0] = Read_8Bit(Register1);
        Buffer[1] = Read_8Bit(Register2);
        Buffer[2] = Read_8Bit(Register3);

        // the int Word is used to build the full 16bit word from the two 8bit register reads.
        N_U16 Temp = Buffer[2] << 8;

        Temp = Temp | Buffer[1];

        volatile N_U32 Word = Temp << 8 | Buffer[0];

        return Word;
}

void Send_Command(uint8_t Register, uint8_t Command)
{
        IBA(Local_I2C_ID)->I2C_DATA_CMD_REG = Register & I2C_I2C_DATA_CMD_REG_I2C_DAT_Msk;
        IBA(Local_I2C_ID)->I2C_DATA_CMD_REG = Command & I2C_I2C_DATA_CMD_REG_I2C_DAT_Msk | I2C_I2C_DATA_CMD_REG_I2C_STOP_Msk;
}

N_BOOL ConfiguredCheck(void)
{
        return Configured;
}

void Change_Slave_address(uint8_t address)
{
        if (HW_I2C_REG_GETF(Local_I2C_ID, I2C_TAR, IC_TAR) != address)
        {
                while (hw_i2c_is_master_busy(Local_I2C_ID));

                /* Now is safe to disable the I2C to change the Target Address */
                hw_i2c_disable(Local_I2C_ID);

                /* Change the Target Address */
                HW_I2C_REG_SETF(Local_I2C_ID, I2C_TAR, IC_TAR, address);

                /* Enable again the I2C to use the new address */
                hw_i2c_enable(Local_I2C_ID);
        }
}

void I2C_Configure(void)
{
        hw_sys_pd_com_enable();

        HW_GPIO_FUNC scl_function = ((Local_I2C_ID == HW_I2C1) ? HW_GPIO_FUNC_I2C_SCL :
                                                               HW_GPIO_FUNC_I2C2_SCL);
        HW_GPIO_FUNC sda_function = ((Local_I2C_ID == HW_I2C1) ? HW_GPIO_FUNC_I2C_SDA :
                                                       HW_GPIO_FUNC_I2C2_SDA);
        /* Configure SCL */
        hw_gpio_set_pin_function(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin, HW_GPIO_MODE_OUTPUT,
                                 scl_function);
        hw_gpio_configure_pin_power(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin, i2c_Io_Con.voltage_level);
        /* Configure SDA */
        hw_gpio_set_pin_function(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin, HW_GPIO_MODE_OUTPUT,
                                 sda_function);
        hw_gpio_configure_pin_power(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin, i2c_Io_Con.voltage_level);

        hw_gpio_pad_latch_enable(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin);
        hw_gpio_pad_latch_enable(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin);

        IRQn_Type irq_type = I2C_IRQn;

        if (Local_I2C_ID == HW_I2C1) {
                CRG_COM->RESET_CLK_COM_REG = CRG_COM_RESET_CLK_COM_REG_I2C_CLK_SEL_Msk;
                CRG_COM->SET_CLK_COM_REG = CRG_COM_SET_CLK_COM_REG_I2C_ENABLE_Msk;
        } else {
                irq_type = I2C2_IRQn;
                CRG_COM->RESET_CLK_COM_REG = CRG_COM_RESET_CLK_COM_REG_I2C2_CLK_SEL_Msk;
                CRG_COM->SET_CLK_COM_REG = CRG_COM_SET_CLK_COM_REG_I2C2_ENABLE_Msk;
        }

        hw_i2c_master_abort_transfer(Local_I2C_ID);

        hw_i2c_disable(Local_I2C_ID);

        while (hw_i2c_get_enable_status(Local_I2C_ID) & I2C_I2C_ENABLE_STATUS_REG_IC_EN_Msk) {
                hw_clk_delay_usec(500);
        }

        IBA(Local_I2C_ID)->I2C_INTR_MASK_REG = 0x0000;

        hw_i2c_configure(Local_I2C_ID, &drv_EEPROM_24FC256.i2c);

        NVIC_EnableIRQ(irq_type);

        hw_i2c_init(Local_I2C_ID, &(drv_EEPROM_24FC256.i2c));

        hw_i2c_enable(Local_I2C_ID);

        hw_i2c_reset_abort_source(Local_I2C_ID);
        hw_i2c_reset_int_all(Local_I2C_ID);

        Configured = true;
}

void I2C_IO_Setup(void)
{
        hw_sys_pd_com_enable();

        hw_gpio_set_pin_function(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin, i2c_Io_Con.scl.off.mode, i2c_Io_Con.scl.off.function);
        if (i2c_Io_Con.scl.off.mode == HW_GPIO_MODE_OUTPUT || i2c_Io_Con.scl.off.mode == HW_GPIO_MODE_OUTPUT_PUSH_PULL ||
                i2c_Io_Con.scl.off.mode == HW_GPIO_MODE_OUTPUT_OPEN_DRAIN) {
                if (i2c_Io_Con.scl.off.high) {
                        hw_gpio_set_active(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin);
                } else {
                        hw_gpio_set_inactive(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin);
                }
        }
        hw_gpio_configure_pin_power(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin, i2c_Io_Con.voltage_level);

        hw_gpio_set_pin_function(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin, i2c_Io_Con.sda.off.mode, i2c_Io_Con.sda.off.function);
        if (i2c_Io_Con.sda.off.mode == HW_GPIO_MODE_OUTPUT || i2c_Io_Con.sda.off.mode == HW_GPIO_MODE_OUTPUT_PUSH_PULL ||
                i2c_Io_Con.sda.off.mode == HW_GPIO_MODE_OUTPUT_OPEN_DRAIN) {
                if (i2c_Io_Con.sda.off.high) {
                        hw_gpio_set_active(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin);
                } else {
                        hw_gpio_set_inactive(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin);
                }
        }
        hw_gpio_configure_pin_power(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin, i2c_Io_Con.voltage_level);

        hw_gpio_pad_latch_enable(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin);
        hw_gpio_pad_latch_disable(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin);
        hw_gpio_pad_latch_enable(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin);
        hw_gpio_pad_latch_disable(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin);

        hw_sys_pd_com_disable();

        I2C_Configure();
}
