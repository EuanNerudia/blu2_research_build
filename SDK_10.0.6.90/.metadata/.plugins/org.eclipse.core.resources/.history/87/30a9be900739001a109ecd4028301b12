#define Address_ACC 0b1101000

#define CommandReg 0x7E

#define AccNormalPower  0x11
#define AccLowPower     0x12

#define GyroNormal      0x15
#define GyroFastStartUp 0x17

#define X_AccL 0x12
#define X_AccH 0x13

#define Y_AccL 0x14
#define Y_AccH 0x15

#define Z_AccL 0x16
#define Z_AccH 0x17

#define X_GyroL 0x0C
#define X_GyroH 0x0D

#define Y_GyroL 0x0E
#define Y_GyroH 0x0f

#define Z_GyroL 0x10
#define Z_GyroH 0x11

#include "I2CAdapter.h"

#include "..\UART\UARTAdapterPrototypes.h"
#include "..\UART\UARTAdapterTypes.h"
#include "..\UART\UARTAdapterDefines.h"

void Accelerometer_Setup(void)
{
        if (!ConfiguredCheck())
        {
                I2C_IO_Setup();
        }

        Change_Slave_address(Address_ACC);
}

void ACC_UART_Print(uint16_t Print)
{
        ConfigureUART_GPIO(N_UART1, 0, 3, 0, 4, HW_GPIO_POWER_V33);
        SetUARTBaudRate(N_UART1, 4358);
        SetOffsetToLength(N_UART1, 0);
        UART_SetTimeout(N_UART1, 200);

        char DataSend[6];

        char HexVal[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

        DataSend[0] = HexVal[((Print & 0xF000) >> 12)];
        DataSend[1] = HexVal[((Print & 0x0F00) >> 8)];
        DataSend[2] = HexVal[((Print & 0x00F0) >> 4)];
        DataSend[3] = HexVal[((Print & 0x000F))];
        DataSend[4] = '\r';
        DataSend[5] = '\n';

        SendUARTData(N_UART1, 6, &DataSend);
}

void Full_UART_Print(N_U16 X, N_U16 Y, N_U16 Z)
{
        ConfigureUART_GPIO(N_UART1, 0, 3, 0, 4, HW_GPIO_POWER_V33);
        SetUARTBaudRate(N_UART1, 4358);
        SetOffsetToLength(N_UART1, 0);
        UART_SetTimeout(N_UART1, 200);

        char DataSend[15];

        char HexVal[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

        DataSend[0] = HexVal[((X & 0xF000) >> 12)];
        DataSend[1] = HexVal[((X & 0x0F00) >> 8)];
        DataSend[2] = HexVal[((X & 0x00F0) >> 4)];
        DataSend[3] = HexVal[((X & 0x000F))];
        DataSend[4] = ',';
        DataSend[5] = HexVal[((Y & 0xF000) >> 12)];
        DataSend[6] = HexVal[((Y & 0x0F00) >> 8)];
        DataSend[7] = HexVal[((Y & 0x00F0) >> 4)];
        DataSend[8] = HexVal[((Y & 0x000F))];
        DataSend[9] = ',';
        DataSend[10] = HexVal[((Z & 0xF000) >> 12)];
        DataSend[11] = HexVal[((Z & 0x0F00) >> 8)];
        DataSend[12] = HexVal[((Z & 0x00F0) >> 4)];
        DataSend[13] = HexVal[((Z & 0x000F))];
        DataSend[14] = ',';
//        DataSend[15] = '\n';

        SendUARTData(N_UART1, 15, &DataSend);
}

// Sets the tap inter up and starts the device so that this can be seen.
void Tap_Setup(void)
{
//        Send_Command(0x050, 0b00100000); // Single Tap enabled
        Send_Command(0x050, 0b00010000); // Double Tap

        Send_Command(0x53, 0b00001010); // Set up interupt pin one as an output and active high

        Send_Command(0x54, 0b00000000); // Pin Latching and interupt duration, set to no latching.

        Send_Command(0x55, 0b00010000);

        Send_Command(0x63, 0b10000010);

        Send_Command(0x64, 0b00000000);

        Send_Command(CommandReg, AccNormalPower);
}

void Accel_Config(void)
{
        // Check Config
        Read_8Bit(0x40); // Check it is set to the reset val (0b00101000)

        Send_Command(0x40, 0b10101000); // 100Hz setup, normal pwr mode

        Send_Command(0x41, 0b00000011); // Set g-range as + or - 2g
}

void Gyro_Config(void)
{
        Read_8Bit(0x42);

        Send_Command(0x42, 0b00101000); // sets the odr to 100Hz

        Send_Command(0x43, 0b00000010); // Angular rate range and resolution of + or - 500 degrees a second
}

void Set_Pwr_1(void)
{
        Send_Command(0x7E, 0x11);
}

void Set_Pwr_2(void)
{
        Send_Command(0x7E, 0x15);
}

void Read_Accel(void)
{
        volatile N_U16 X_Accel = (Read_8Bit(0x13) << 8) | Read_8Bit(0x12);
        volatile N_U16 Y_Accel = (Read_8Bit(0x15) << 8) | Read_8Bit(0x14);
        volatile N_U16 Z_Accel = (Read_8Bit(0x17) << 8) | Read_8Bit(0x16);

        Full_UART_Print(X_Accel, Y_Accel, Z_Accel);
}

void Read_Gyro(void)
{
        volatile N_U16 X_Gyro = (Read_8Bit(0x19) << 8) | Read_8Bit(0x18);
        volatile N_U16 Y_Gyro = (Read_8Bit(0x21) << 8) | Read_8Bit(0x20);
        volatile N_U16 Z_Gyro = (Read_8Bit(0x23) << 8) | Read_8Bit(0x22);

        Full_UART_Print(X_Gyro, Y_Gyro, Z_Gyro);
}

int Ticks = 0;

N_BOOL Set = false;

void Accel_Gryo_Pwr_Up_Read(void)
{
        if (Ticks == 1)
        {
                ACC_UART_Print(0);
        }

        if (!Set && Ticks < 1)
        {
                Accel_Config();
                Gyro_Config();

                Set_Pwr_1();
        }

        if (!Set && Ticks == 5)
        {
                Set_Pwr_2();

                Set = true;

                Ticks = 0;
        }

        if (Ticks == 100)
        {
                Read_Accel();
        }

        if (Ticks == 101)
        {
                Read_Gyro();
                Ticks = 0;
        }

        Ticks++;
}

/*
 * Sensor Config
 *
 * Set Accel Conf
 *
 * check result
 *
 * Set Gyro Conf
 *
 * Check result
 *
 * set Power mode
 *
 * Check result
 *
 * check invalid setting
 */

/*
 * Set Accel conf
 *
 * Read_8Bit(0x40) // Check current config
 *
 * Send_Command(0x40, 0x00)
 */


/*
 * Shows the current power mode
 * PMU_Status 0x03
 *
 * Data registers for Mag, RHALL, Gyr X Y Z and ACC X Y Z
 * Gyr and Acc split between two registers
 * 0x04 - 0x17
 *
 * Sensor time register, 24bit counter
 * 0x18 - 0x1A
 *
 * Status register displays data rediness
 * 0x1B
 *
 * Interrupt status register, holds the interrupt status flags
 * 0x1C - 0x1F
 *
 * Interrupt set up registers
 * 0x50 - 0x52
 *
 * Tap interupt Config register, tap duration of either
 * 0x63 - 0x64
 */
