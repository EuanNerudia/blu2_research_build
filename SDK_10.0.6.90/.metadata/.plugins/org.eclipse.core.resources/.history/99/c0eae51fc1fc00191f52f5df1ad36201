/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: .c
 * Purpose:
 * Version: V1.0
 * Author: E Denton
 * Version
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/

#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include "osal.h"
#include "resmgmt.h"
#include "hw_cpm.h"
#include "hw_gpio.h"
#include "hw_watchdog.h"
#include "sys_clock_mgr.h"
#include "sys_power_mgr.h"

// Includes required for the ADC
#include "hw_pdc.h"
#include "ad_gpadc.h"
#include "hw_gpadc.h"
#include "hw_wkup.h"
#include "hw_sys.h"

#include "ADCAdapter.h"

// Type structure required for the General purpose ADC
typedef const void* gp_adc_device;
gp_adc_device GP_ADC_DEVICE;

ad_gpadc_handle_t GP_DEV;

ad_gpadc_io_conf_t GP_ADC_BUS = {
        .input0 = {
                .port = HW_GPIO_PORT_NONE,
                .pin  = HW_GPIO_PIN_NONE,
                .on   = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_ADC,  INPUT_TRUE},
                .off  = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_GPIO, INPUT_TRUE}
        },
        .input1 = {
                .port = HW_GPIO_PORT_NONE,
                .pin  = HW_GPIO_PIN_NONE,
                .on   = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_GPIO,  INPUT_TRUE},
                .off  = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_GPIO, INPUT_TRUE}
        },
        .voltage_level = ADC_GPIO_POWER_NONE
};

ad_gpadc_driver_conf_t GP_ADC_DRV = {
        .clock                  = GP_ADC_CLOCK_INTERNAL,
        .input_mode             = GP_ADC_INPUT_MODE_SINGLE_ENDED,
        .input                  = GP_ADC_INPUT_SE_P0_25,
        .temp_sensor            = GP_ADC_NO_TEMP_SENSOR,
        .sample_time            = GP_ADC_SAMPLE_1,
        .continous              = ADC_CONTINUOUS_FALSE,
        .chopping               = GP_ADC_CHOPPING_TRUE,
        .oversampling           = GP_ADC_OVERSAMPLING_1_SAMPLE,
        .input_attenuator       = GP_ADC_INPUT_VOLTAGE_UP_TO_1V2
};

ad_gpadc_controller_conf_t GP_ADC_DEV = {
        .id  = HW_GPADC_1,
        .io  = &GP_ADC_BUS,
        .drv = &GP_ADC_DRV
};

gp_adc_device GP_ADC_DEVICE = &GP_ADC_DEV;

GP_ADC_Config Local_GP_ADC_Config;

void (*Loacl_callback)(int Reading);

int Read_error;

int GP_min   = 0;
int GP_max   = 0;
int GP_pass  = 0;
int GP_total = 0;

gp_adc_device GP_ADC_Configure(GP_ADC_Config *Config)
{
        ad_gpadc_io_conf_t Temp_BUS = {
                .input0 = {
                        .pin  = Config->io_bus.input0.pin,
                        .port = Config->io_bus.input0.port,
                        .on   = {
                                Config->io_bus.input0.on.mode,
                                Config->io_bus.input0.on.function,
                                Config->io_bus.input0.on.high
                        },
                        .off  = {
                                Config->io_bus.input0.off.mode,
                                Config->io_bus.input0.off.function,
                                Config->io_bus.input0.off.high
                        }
                },
                .input1 = {
                        .pin  = Config->io_bus.input1.pin,
                        .port = Config->io_bus.input1.port,
                        .on   = {
                                Config->io_bus.input1.on.mode,
                                Config->io_bus.input1.on.function,
                                Config->io_bus.input1.on.high
                        },
                        .off  = {
                                Config->io_bus.input1.off.mode,
                                Config->io_bus.input1.off.function,
                                Config->io_bus.input1.off.high
                        }
                },
                .voltage_level = Config->io_bus.voltage_level
        };

        GP_ADC_BUS = Temp_BUS;

        ad_gpadc_driver_conf_t Temp_Driver = {
                .clock            = Config->driver.clock,
                .input            = Config->driver.input,
                .chopping         = Config->driver.chopping,
                .continous        = Config->driver.continous,
                .input_mode       = Config->driver.input_mode,
                .temp_sensor      = Config->driver.temp_sensor,
                .sample_time      = Config->driver.sample_time,
                .oversampling     = Config->driver.oversampling,
                .input_attenuator = Config->driver.input_attenuator,
        };

        GP_ADC_DRV = Temp_Driver;

//        GP_ADC_DRV.clock            = Config->driver.clock;
//        GP_ADC_DRV.input            = Config->driver.input;
//        GP_ADC_DRV.chopping         = Config->driver.chopping;
//        GP_ADC_DRV.continous        = Config->driver.continous;
//        GP_ADC_DRV.input_mode       = Config->driver.input_mode;
//        GP_ADC_DRV.temp_sensor      = Config->driver.temp_sensor;
//        GP_ADC_DRV.sample_time      = Config->driver.sample_time;
//        GP_ADC_DRV.oversampling     = Config->driver.oversampling;
//        GP_ADC_DRV.input_attenuator = Config->driver.input_attenuator;

        ad_gpadc_controller_conf_t Temp_Contorler = {
                .id  = HW_GPADC_1,
                .io  = &GP_ADC_BUS,
                .drv = &GP_ADC_DRV
        };

        GP_ADC_DEV.io  = &GP_ADC_BUS;
        GP_ADC_DEV.drv = &GP_ADC_DRV;

        GP_ADC_DEVICE = &GP_ADC_DEV;

        Config->configured = true;

        Local_GP_ADC_Config = *Config;

        return GP_ADC_DEVICE;
}

int GP_ADC_convert(gp_adc_device src, int value)
{
        ad_gpadc_controller_conf_t *cfg = (ad_gpadc_controller_conf_t *)src;

        const uint16 adc_src_max =  ad_gpadc_get_source_max(cfg->drv);
        uint32_t mv_src_max = (cfg->drv->input_attenuator ==
                                     HW_GPADC_INPUT_VOLTAGE_UP_TO_1V2) ? 1200 : 3600;
        int ret = 0;

        switch (cfg->drv->input_mode) {
        case HW_GPADC_INPUT_MODE_SINGLE_ENDED:
                if (cfg->drv->input == HW_GPADC_INPUT_SE_VBAT) {
                        mv_src_max = 5000;
                }
                ret =  (mv_src_max * value) / adc_src_max;
                break;
        case HW_GPADC_INPUT_MODE_DIFFERENTIAL:
                ret = ((int)mv_src_max * (value - (adc_src_max >> 1))) /
                                                         (adc_src_max >> 1);
                break;
        }

        return ret;
}

void GPADC_SetupHardware(void)
{
        ad_gpadc_io_config(((ad_gpadc_controller_conf_t *)GP_ADC_DEVICE)->id,
                ((ad_gpadc_controller_conf_t *)GP_ADC_DEVICE)->io, AD_IO_CONF_OFF);
}

//void GP_ADC_Value_Send(int value)
//{
//        value = (value - (GP_max + GP_min)) / 6;
//
//        value = GP_ADC_convert(GP_ADC_DEVICE, value);
//
//        GP_max   = 0;
//        GP_min   = 0;
//        GP_pass  = 0;
//        GP_total = 0;
//
//        if (Read_error == AD_GPADC_ERROR_NONE)
//        {
//
//                (*Loacl_callback)(value);
//        }
//}
//
//void GP_ADC_average(void *user_data, int value)
//{
//        if (GP_pass > 6)
//        {
//                Finished = true;
//                GP_ADC_Value_Send(GP_total);
//        }
//
//        GP_pass++;
//
//        if (GP_min == 0 || (value < GP_min && value > 0))
//        {
//                GP_min = value;
//        }
//
//        if (value > GP_max)
//        {
//                GP_max = value;
//        }
//
//        if (value == 0)
//        {
//                GP_pass--;
//        }
//
//        GP_total += value;
//
//        if (GP_pass < 6)
//        {
//                GP_ADC_re_read();
//        }
//}
//
//void GP_ADC_callback(void)
//{
//        int val;
//        int Send;
//
//        val = hw_gpadc_get_value();
//
//        hw_gpadc_clear_interrupt();
//
//        GP_ADC_average((void *)&Send, val);
//}
//
//void GP_ADC_re_read(void)
//{
//        bool aquired = ad_gpadc_acquire_to(RES_WAIT_FOREVER);
//
//        if (aquired)
//        {
//
//        }
//
//        hw_gpadc_register_interrupt(GP_ADC_callback);
//
//        hw_gpadc_start();
//}

void GP_ADC_Value_set(void *user_data, int value)
{
        int *pData = (int*)user_data;

        *pData = value;

        int send = GP_ADC_convert(GP_ADC_DEVICE, value);

        (*Loacl_callback)(send);
}


void GP_ADC_Read(void(*Call)(int reading))
{
        Loacl_callback = Call;

        Read_error = 0;

        int Read_data;

        ad_gpadc_handle_t GP_Device = ad_gpadc_open((ad_gpadc_controller_conf_t *)GP_ADC_DEVICE);

        Read_error = ad_gpadc_read_async(GP_Device, GP_ADC_Value_set, (void *)&Read_data);

        ad_gpadc_close(GP_Device, true);
}

/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/

