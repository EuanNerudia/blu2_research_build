/*
 * I2CAdapter.c
 *
 *  Created on: 21 Nov 2019
 *      Author: Euan Denton
 */

#include "hw_pdc.h"
#include "hw_i2c.h"
#include "ad_i2c.h"
#include "hw_wkup.h"
#include "hw_sys.h"

const ad_i2c_io_conf_t i2c_Io_Con = {
        .scl = {
                .port = HW_GPIO_PORT_0, .pin = HW_GPIO_PIN_30,
                .on =  { HW_GPIO_MODE_OUTPUT_OPEN_DRAIN, HW_GPIO_FUNC_I2C_SCL, false },
                .off = { HW_GPIO_MODE_INPUT,             HW_GPIO_FUNC_GPIO,    true  }
        },
        .sda = {
                .port = HW_GPIO_PORT_0, .pin =HW_GPIO_PIN_31,
                .on =  { HW_GPIO_MODE_OUTPUT_OPEN_DRAIN, HW_GPIO_FUNC_I2C_SDA, false },
                .off = { HW_GPIO_MODE_INPUT,             HW_GPIO_FUNC_GPIO,    true  }
        },
        .voltage_level = HW_GPIO_POWER_VDD1V8P
};

const ad_i2c_driver_conf_t drv_EEPROM_24FC256 = {
        I2C_DEFAULT_CLK_CFG,
        .i2c.speed              = HW_I2C_SPEED_STANDARD,
        .i2c.mode               = HW_I2C_MODE_MASTER,
        .i2c.addr_mode          = HW_I2C_ADDRESSING_7B,
        .i2c.address            = 0b1101011,
        .dma_channel            = HW_DMA_CHANNEL_2
};

/* EEPROM 24FC256 I2C controller configuration */
const ad_i2c_controller_conf_t dev_24FC256 = {
        .id     = HW_I2C1,
        .io     = &i2c_Io_Con,
        .drv    = &drv_EEPROM_24FC256
};

#define CTRL3_ADDR 0x09
#define CTRL3_COMM 0b00110011

#define CTRL4_ADDR 0x0A
#define CTRL4_COMM 0b00000110

#define CTRL1_XL_A 0x10
#define CTRL1_XL_C 0b01000100

const HW_I2C_ID Local_I2C_ID = HW_I2C1;

uint8_t Buff[8];

void I2C_IO_Setup(void)
{
        hw_sys_pd_com_enable();

        hw_gpio_set_pin_function(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin, i2c_Io_Con.scl.off.mode, i2c_Io_Con.scl.off.function);
        if (i2c_Io_Con.scl.off.mode == HW_GPIO_MODE_OUTPUT || i2c_Io_Con.scl.off.mode == HW_GPIO_MODE_OUTPUT_PUSH_PULL ||
                i2c_Io_Con.scl.off.mode == HW_GPIO_MODE_OUTPUT_OPEN_DRAIN) {
                if (i2c_Io_Con.scl.off.high) {
                        hw_gpio_set_active(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin);
                } else {
                        hw_gpio_set_inactive(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin);
                }
        }
        hw_gpio_configure_pin_power(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin, i2c_Io_Con.voltage_level);

        hw_gpio_set_pin_function(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin, i2c_Io_Con.sda.off.mode, i2c_Io_Con.sda.off.function);
        if (i2c_Io_Con.sda.off.mode == HW_GPIO_MODE_OUTPUT || i2c_Io_Con.sda.off.mode == HW_GPIO_MODE_OUTPUT_PUSH_PULL ||
                i2c_Io_Con.sda.off.mode == HW_GPIO_MODE_OUTPUT_OPEN_DRAIN) {
                if (i2c_Io_Con.sda.off.high) {
                        hw_gpio_set_active(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin);
                } else {
                        hw_gpio_set_inactive(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin);
                }
        }
        hw_gpio_configure_pin_power(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin, i2c_Io_Con.voltage_level);

        hw_gpio_pad_latch_enable(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin);
        hw_gpio_pad_latch_disable(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin);
        hw_gpio_pad_latch_enable(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin);
        hw_gpio_pad_latch_disable(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin);

        hw_sys_pd_com_disable();

        I2C_Configure();
}

hw_i2c_complete_cb I2C_CallBack(HW_I2C_ID Id, uint8_t *CallbackData, uint16_t len, bool state)
{
        for (uint16_t i=0; i<len; i++)
        {
                printf("%c", CallbackData[i]);
        }
        printf("\r\n");

       // I2C_Read_Async();
}

// I2C device open
void I2C_Configure(void)
{
        hw_sys_pd_com_enable();

        HW_GPIO_FUNC scl_function = ((Local_I2C_ID == HW_I2C1) ? HW_GPIO_FUNC_I2C_SCL :
                                                               HW_GPIO_FUNC_I2C2_SCL);
        HW_GPIO_FUNC sda_function = ((Local_I2C_ID == HW_I2C1) ? HW_GPIO_FUNC_I2C_SDA :
                                                       HW_GPIO_FUNC_I2C2_SDA);
        /* Configure SCL */
        hw_gpio_set_pin_function(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin, HW_GPIO_MODE_OUTPUT,
                                 scl_function);
        hw_gpio_configure_pin_power(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin, i2c_Io_Con.voltage_level);
        /* Configure SDA */
        hw_gpio_set_pin_function(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin, HW_GPIO_MODE_OUTPUT,
                                 sda_function);
        hw_gpio_configure_pin_power(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin, i2c_Io_Con.voltage_level);

        hw_gpio_pad_latch_enable(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin);
        hw_gpio_pad_latch_enable(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin);

        ///////////////////////////////////////////////////////////////////////
        IRQn_Type irq_type = I2C_IRQn;

        if (Local_I2C_ID == HW_I2C1) {
                CRG_COM->RESET_CLK_COM_REG = CRG_COM_RESET_CLK_COM_REG_I2C_CLK_SEL_Msk;
                CRG_COM->SET_CLK_COM_REG = CRG_COM_SET_CLK_COM_REG_I2C_ENABLE_Msk;
        } else {
                irq_type = I2C2_IRQn;
                CRG_COM->RESET_CLK_COM_REG = CRG_COM_RESET_CLK_COM_REG_I2C2_CLK_SEL_Msk;
                CRG_COM->SET_CLK_COM_REG = CRG_COM_SET_CLK_COM_REG_I2C2_ENABLE_Msk;
        }

        /* force controller to abort any ongoing transaction */
        hw_i2c_master_abort_transfer(Local_I2C_ID);

        /*
         * Look into the below as this should not be necessary
         */
        hw_i2c_disable(Local_I2C_ID);

        while (hw_i2c_get_enable_status(Local_I2C_ID) & I2C_I2C_ENABLE_STATUS_REG_IC_EN_Msk) {
                hw_clk_delay_usec(500);
        }

        IBA(Local_I2C_ID)->I2C_INTR_MASK_REG = 0x0000;

        hw_i2c_configure(Local_I2C_ID, &drv_EEPROM_24FC256.i2c);

        NVIC_EnableIRQ(irq_type);

        hw_i2c_init(Local_I2C_ID, &(drv_EEPROM_24FC256.i2c));

        hw_i2c_enable(Local_I2C_ID);

        hw_i2c_reset_abort_source(Local_I2C_ID);
        hw_i2c_reset_int_all(Local_I2C_ID);
}

uint16_t Read(uint8_t Register1, uint8_t Register2)
{
        uint8_t Buffer[2];

        // The first call reads the first register, the lower part of the 16bit word for the read
        IBA(Local_I2C_ID)->I2C_DATA_CMD_REG = Register1 & I2C_I2C_DATA_CMD_REG_I2C_DAT_Msk | I2C_I2C_DATA_CMD_REG_I2C_RESTART_Msk;
        IBA(Local_I2C_ID)->I2C_DATA_CMD_REG = I2C_I2C_DATA_CMD_REG_I2C_CMD_Msk | I2C_I2C_DATA_CMD_REG_I2C_STOP_Msk;
        Buffer[0] = HW_I2C_REG_GETF(Local_I2C_ID, I2C_DATA_CMD, I2C_DAT);

        //The second call reads the second register for the higher part of the 16bit word
        IBA(Local_I2C_ID)->I2C_DATA_CMD_REG = Register2 & I2C_I2C_DATA_CMD_REG_I2C_DAT_Msk | I2C_I2C_DATA_CMD_REG_I2C_RESTART_Msk;
        IBA(Local_I2C_ID)->I2C_DATA_CMD_REG = I2C_I2C_DATA_CMD_REG_I2C_CMD_Msk | I2C_I2C_DATA_CMD_REG_I2C_STOP_Msk;
        Buffer[1] = HW_I2C_REG_GETF(Local_I2C_ID, I2C_DATA_CMD, I2C_DAT);

        // the int Word is used to build the full 16bit word from the two 8bit register reads.
        volatile uint16_t Word = Buffer[1] << 8 | Buffer[0];

        return Word;
}

void Send_Command(uint8_t Register, uint8_t Command, bool restart)
{
        if (restart)
        {
                IBA(Local_I2C_ID)->I2C_DATA_CMD_REG = Register & I2C_I2C_DATA_CMD_REG_I2C_DAT_Msk;
                IBA(Local_I2C_ID)->I2C_DATA_CMD_REG = Command & I2C_I2C_DATA_CMD_REG_I2C_DAT_Msk | I2C_I2C_DATA_CMD_REG_I2C_STOP_Msk;
        }
        else
        {
                IBA(Local_I2C_ID)->I2C_DATA_CMD_REG = Register & I2C_I2C_DATA_CMD_REG_I2C_DAT_Msk | I2C_I2C_DATA_CMD_REG_I2C_RESTART_Msk;
                IBA(Local_I2C_ID)->I2C_DATA_CMD_REG = I2C_I2C_DATA_CMD_REG_I2C_CMD_Msk; //| I2C_I2C_DATA_CMD_REG_I2C_RESTART_Msk ;
        }


}

void Accelerometer_Set_Up_And_Read(void)
{
        Send_Command(CTRL3_ADDR, CTRL3_COMM, true);
        Send_Command(CTRL4_ADDR, CTRL4_COMM, true);
        Send_Command(CTRL1_XL_A, CTRL1_XL_C, false);

        volatile uint8_t Buff[2];
        volatile uint16_t AccBuff[3];

        while(true)
        {
                AccBuff[0] = Read(0x79, 0x7A);
                AccBuff[1] = Read(0x7B, 0x7C);
                AccBuff[2] = Read(0x7D, 0x7E);
        }
}

void TestWrite(void)
{
        int EEPROM_24LC256_PAGE_SIZE = 64;

        uint8_t e_data_buff[EEPROM_24LC256_PAGE_SIZE + 2];
        uint8_t *pSrc;

        /* Open the device */
        ad_i2c_handle_t dev_hdr = ad_i2c_open(&drv_EEPROM_24FC256);


        /* Write arbitrary data in EEPROM */
        for (int i = 0; i < (1024 / EEPROM_24LC256_PAGE_SIZE); i++) {

                /* The first two bytes sent are the target slave address */
                e_data_buff[0] = 1 >> 8;
                e_data_buff[1] = 2;

                /* Point to the right next EEPROM page */
                //pSrc = (uint8_t *)e_src_buff + pageAddr;

                /* Prepare the data to be sent */
                memcpy((void *)&e_data_buff[2], (void *)pSrc, EEPROM_24LC256_PAGE_SIZE);

                /* Write one page size in EEPROM */
                int I2C_error_code = ad_i2c_write(dev_hdr, (const uint8_t *)e_data_buff,
                                     sizeof(e_data_buff), HW_I2C_F_ADD_STOP);
        }
}



void I2C_Read_Async(void)
{
        uint8_t ReadBuffer[8];

//        int hw_i2c_read_buffer_async(HW_I2C_ID id, uint8_t *data, uint16_t len, hw_i2c_complete_cb cb, void *cb_data, uint32_t flags)
        hw_i2c_read_buffer_async(Local_I2C_ID, (const uint8_t *) ReadBuffer, 8, I2C_CallBack, &Buff, HW_I2C_F_ADD_STOP);

//        bool master = HW_I2C_REG_GETF(Local_I2C_ID, I2C_CON, I2C_MASTER_MODE);
//        uint16_t mask = master ? HW_I2C_INT_TX_EMPTY : HW_I2C_INT_READ_REQUEST;
//        mask |= HW_I2C_INT_RX_FULL | HW_I2C_INT_TX_ABORT;
//
//        hw_i2c_set_rx_fifo_threshold(Local_I2C_ID, 0);
//
//        hw_i2c_reset_int_tx_abort(Local_I2C_ID);
//
//        hw_i2c_register_int(Local_I2C_ID, I2C_CallBack, mask);
}

static volatile uint16_t hw_i2c_prepare_dma_read_cmd;

void I2C_DMA_Setup(void)
{
        bool master = HW_I2C_REG_GETF(Local_I2C_ID, I2C_CON, I2C_MASTER_MODE);

//        hw_i2c_prepare_dma(id, channel, (void *)data, len,
//                                master ? HW_I2C_DMA_TRANSFER_MASTER_READ : HW_I2C_DMA_TRANSFER_SLAVE_READ,
//                                cb, cb_data, flags);

        hw_i2c_prepare_dma_read_cmd = I2C_I2C_DATA_CMD_REG_I2C_CMD_Msk;

        DMA_setup dma;
        struct i2c *i2c = get_i2c(Local_I2C_ID);

        /* for sanity so even if channel is set to odd number, we'll use proper pair */
        uint8_t channel = HW_DMA_CHANNEL_2;
        channel &= 0xfe;

        /* make sure I2C DMA is off so it's not unexpectedly triggered when channels are enabled */
        IBA(Local_I2C_ID)->I2C_DMA_CR_REG = 0;

//        hw_dma_channel_initialization(&dma);

        hw_dma_channel_enable(channel, HW_DMA_STATE_ENABLED);

        I2C_DMA_Start();
}

void I2C_DMA_Start(void)
{
        IBA(Local_I2C_ID)->I2C_DMA_CR_REG = (1 << I2C_I2C_DMA_CR_REG_TDMAE_Pos) | (1 << I2C_I2C_DMA_CR_REG_RDMAE_Pos);
}
