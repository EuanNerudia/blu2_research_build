
#define Address_Pre 0b1011101

#define Interrupt_CFG    0x0B

#define Threshold_Low    0x0C
#define Threshold_Hig    0x0D

#define Control_Reg_1    0x10
#define Control_Reg_2    0x11
#define Control_Reg_3    0x12

#define Press_Reg_L      0x28
#define Press_Reg_M      0x29
#define Press_Reg_H      0x2A

#define Interupt_CMD 0b11001010

#define Control3_CMD 0b00000010

#define THS_LSB_Val  0b00000011
#define THS_MSB_val  0b00000000

#define SWRESET_CMD  0b00000100

#define Register_Rs  0b00000000

#define Reading_CMD  0b01010010

#define OneShot_CMD  0b00000001

#include <stdio.h>

#include "I2CAdapter.h"

#include "..\UART\UARTAdapterPrototypes.h"
#include "..\UART\UARTAdapterTypes.h"
#include "..\UART\UARTAdapterDefines.h"

void Pressure_Setup(void)
{
        if (!ConfiguredCheck())
        {
                I2C_IO_Setup();
        }

        Change_Slave_address(Address_Pre);
}

void Interupt_setup(void)
{
        Send_Command(Interrupt_CFG, Interupt_CMD); // Interrupt Setup
        Send_Command(Control_Reg_3, Control3_CMD); // Interrupt pin Set
        Send_Command(Threshold_Low, THS_LSB_Val);
        Send_Command(Threshold_Hig, THS_MSB_val);
}

N_U32 NominalStore[10] = {0};
N_U32 Collected[6] = {0};

long Nominal = 0;
long Sum = 0;

N_U32 Ring_Average(int Possition, int Length, N_U32 NewNumber)
{
        if (Length == (sizeof(NominalStore) / sizeof(int)))
        {
                Nominal = Nominal - NominalStore[Possition] + NewNumber;

                NominalStore[Possition] = NewNumber;

                if (NominalStore[9] != 0)
                {
                        return Nominal / Length;
                }
        }
        else if (Length == (sizeof(Collected) / sizeof(int)))
        {
                Sum = Sum - Collected[Possition] + NewNumber;

                Collected[Possition] = NewNumber;

                if (Collected[5] != 0)
                {
                        return Sum / Length;
                }
        }

        return NewNumber;
}

long Get_Nominal()
{
        int Len = sizeof(NominalStore) / sizeof(int);

        return Nominal / Len;
}

long Get_Current()
{
        int Len = sizeof(Collected) / sizeof(int);

        return Sum / Len;
}

N_U32 Read_Pressure(void)
{
        Send_Command(Control_Reg_2, SWRESET_CMD);
        Send_Command(Interrupt_CFG, Register_Rs);
        Send_Command(Control_Reg_1, Reading_CMD);
        Send_Command(Control_Reg_2, OneShot_CMD);

        N_U08 Top = Read_8Bit(Press_Reg_H);
        N_U08 Mid = Read_8Bit(Press_Reg_M);
        N_U08 Low = Read_8Bit(Press_Reg_L);

        N_U32 ReturnVal = (Top << 16) | (Mid << 8) | Low;

        return ReturnVal;
}

void UART_Print(N_U32 Print)
{
        ConfigureUART_GPIO(N_UART1, 0, 3, 0, 4, HW_GPIO_POWER_V33);
        SetUARTBaudRate(N_UART1, 4358);
        SetOffsetToLength(N_UART1, 0);
        UART_SetTimeout(N_UART1, 200);

        char DataSend[8];

        char HexVal[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

        DataSend[0] = HexVal[((Print & 0xF00000) >> 20)];
        DataSend[1] = HexVal[((Print & 0x0F0000) >> 16)];
        DataSend[2] = HexVal[((Print & 0x00F000) >> 12)];
        DataSend[3] = HexVal[((Print & 0x000F00) >> 8)];
        DataSend[4] = HexVal[((Print & 0x0000F0) >> 4)];
        DataSend[5] = HexVal[((Print & 0x00000F))];
        DataSend[6] = '\r';
        DataSend[7] = '\n';

        SendUARTData(N_UART1, 8, &DataSend);
}

int NominalPos = 0;

void ReadNominal(void)
{
        int Length = sizeof(NominalStore) / sizeof(int);

        N_U32 NominalRead = Read_Pressure();

        N_U32 Sending = Ring_Average(NominalPos, Length, NominalRead);

        NominalPos++;

        if (NominalPos == Length)
        {
                NominalPos = 0;
        }

        if (Sending > 0)
        {
                UART_Print(Sending);
        }
}

int OneShotPos = 0;

long OneShotRead(void)
{
        int Length = sizeof(Collected) / sizeof(int);

        N_U32 Reading = Read_Pressure();

        N_U32 Sending = Ring_Average(OneShotPos, Length, Reading);

        OneShotPos++;

        if (OneShotPos == Length)
        {
                OneShotPos = 0;
        }

        UART_Print(Sending);

        return Sending;
}

void Reset_Current()
{
        for(int i = 0; i < 6; i++)
        {
                Collected[i] = 0;
        }

        Sum = 0;
        OneShotPos = 0;
}

N_U64 Count = 0;
N_U64 Tick = 0;
N_U64 nominal = 0;
N_BOOL ReadStart = false;

void Pressure_Ticks(void)
{
        if (ReadStart)
        {
                if (Tick%15 == 0 && nominal > 20)
                {
                        OneShotRead();

                        if(Count > 8 && (Get_Nominal() - Get_Current()) < 500)
                        {
                                Reset_Current();

                                Count = 0;
                                Tick = 0;
                                nominal = 20;
                                ReadStart = false;
                        }

                        Count++;

                        Tick = 0;
                }
        }
        else
        {
                if(Tick%15 == 0 && nominal < 20)
                {
                        nominal++;

                        ReadNominal();

                        Tick = 0;
                }

                else if (Tick > 0 && Tick%100 == 0 && nominal == 20)
                {
                        Interupt_setup();
                        Tick = 0 ;
                        nominal = 21;
                }

                else if (Tick > 1000)
                {
                        nominal = 17;
                        Tick = 0;
                }
        }

        Tick++;
}

void Read_StartHandle(void)
{
        ReadStart = true;
}

//void Read_Pressure(void)
//{
//        // configure serial port
//        ConfigureUART_GPIO(N_UART1, 0, 3, 0, 4, HW_GPIO_POWER_V33);
//        SetUARTBaudRate(N_UART1, 4358);
//        SetOffsetToLength(N_UART1, 0);
//        UART_SetTimeout(N_UART1, 200);
//
////        Send_Command(0x0B, 0b00100000);
////        Send_Command(0x10, 0b01011110);
////        Send_Command(0x11, 0b01010000);
////        Send_Command(0x14, 0b00110000);
//
//        Send_Command(0x11, 0b10000000);
//        Send_Command(0x0B, 0b10000000);
//        Send_Command(0x10, 0b01011110);
//
//        volatile N_U32 Reading;
//
//        volatile N_U08 status;
//
//        char DataSend[8];
//
//        char HexVal[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
//
//        volatile N_U32 Cumulative = 0;
//
//        volatile N_U32 RefPres;
//
//        int Addition = 0;
//
//        int ZeroCount = 0;
//
//        while(1)
//        {
////                Send_Command(0x10, 0b01011110);
//
////                status = Read_8Bit(0x26);
//                status = Read_8Bit(0x27);
//                status = status & 0b00010001;
//
//                if (status > 0)
//                {
//                        RefPres = Read_24Bit(0x15, 0x16, 0x17);
//
////                        Send_Command(0x10, 0b00000000);
//
////                        Reading = Read_24Bit(0x28, 0x29, 0x2A);
//
//                        volatile int Last = Read_8Bit(0x2A);
//                        Last = Read_8Bit(0x2A);
//                        Last = Read_8Bit(0x2A);
//
//                        DataSend[0] = HexVal[((Last & 0xF0) >> 4)];
//                        DataSend[1] = HexVal[((Last & 0x0F))];
//
//                        volatile int Mid = Read_8Bit(0x29);
//                        Mid = Read_8Bit(0x29);
//                        Mid = Read_8Bit(0x29);
//
//                        DataSend[2] = HexVal[((Mid & 0xF0) >> 4)];
//                        DataSend[3] = HexVal[((Mid & 0x0F))];
//
//                        volatile int First = Read_8Bit(0x28);
//
//                        DataSend[4] = HexVal[((First & 0xF0) >> 4)];
//                        DataSend[5] = HexVal[((First & 0x0F))];
//
//                        status = Read_8Bit(0x27);
//                        status = status & 0b00010001;
//
//                        Reading = (Last << 16) | (Mid << 8) | First;
//
//                       // Reading = RefPres - Reading;
//
//                        DataSend[6] = '\r';
//                        DataSend[7] = '\n';
//
////                        if (status == 0)
////                        {
//                                SendUARTData(N_UART1, 8, &DataSend);
////                        }
//
//
//
////                        DataSend[0] = HexVal[((RefPres & 0xF00000) >> 20)];
////                        DataSend[1] = HexVal[((RefPres & 0x0F0000) >> 16)];
////                        DataSend[2] = HexVal[((RefPres & 0x00F000) >> 12)];
////                        DataSend[3] = HexVal[((RefPres & 0x000F00) >> 8)];
////                        DataSend[4] = HexVal[((RefPres & 0x0000F0) >> 4)];
////                        DataSend[5] = HexVal[((RefPres & 0x00000F))];
////                        DataSend[6] = '\r';
////                        DataSend[7] = '\n';
////
//////                        Send_Command(0x10, 0b01011110);
////
////                        if (status == 0)
////                        {
////                                SendUARTData(N_UART1, 8, &DataSend);
////                        }
//
//
//
//
//                }
////                else if (status == 0)
////                {
////
////                        if (ZeroCount > 10)
////                        {
////                                Send_Command(0x11, 0b10000100);
////                                Send_Command(0x0B, 0b00100000);
////                                Send_Command(0x10, 0b01011110);
////                                ZeroCount = 0;
////                        }
////                        ZeroCount++;
////                }
//        }
//        Interupt_setup();
//}

//void FIFO_Read_Pressure(void)
//{
////        Send_Command(CommandReg, PresureCmd);
////
////        Send_Command(0x17, 0b00001011);
////        Send_Command(0x19, 0b00010010);
//
//        Send_Command(0x14, 0b001);
//
//        volatile N_U16 Length;
//
//        while (1)
//        {
//                Length = Read_16Bit(0x12, 0x13);
//                //Length &= 0b0000000111111111;
//                if (Length == 512)
//                {
//                        break;
//                }
//        }
//
//        volatile N_U08 FiFOData[512];
//
//        if (Length == 512)
//        {
//                volatile N_U08 data;
//
//                ReadDirect(1);
//
//                for (int i=0; i < Length; i++)
//                {
//                        ReadDirect(0);
//                }
//
//                for (int i=0; i < Length; i++)
//                {
//                        data = ReadDirect(0);
//                        //if (i > 1 && data != FiFOData[(i-1)])
//                        //{
//                                FiFOData[i] = data;
//                        //}
//                }
//
//                volatile N_U32 Data[170];
//
//                int count = 0;
//                int DataCount = 0;
//
//                for (int i=0; i < 510; i++)
//                {
//                        if (count == 2)
//                        {
//                                Data[DataCount] = FiFOData [i] << 16 | FiFOData[(i-1)] << 8 | FiFOData[(i-2)];
//                                DataCount++;
//                                count = 0;
//                        }
//                        count++;
//                }
//
//                Send_Command(0x7E, 0xB0);
//        }
//}
