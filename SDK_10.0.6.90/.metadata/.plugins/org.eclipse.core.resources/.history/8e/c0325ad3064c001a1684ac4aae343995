/**
 ****************************************************************************************
 *
 * @file main.c
 *
 * @brief FreeRTOS template application with retarget
 *
 * Copyright (C) 2015-2019 Dialog Semiconductor.
 * This computer program includes Confidential, Proprietary Information
 * of Dialog Semiconductor. All Rights Reserved.
 *
 ****************************************************************************************
 */

/*
 * The code below is used to interact with the ADC on the
 * pin defined in Platform_devices.c
 * The code reads the ADC on an interrupt
 */

#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include "osal.h"
#include "resmgmt.h"
#include "hw_cpm.h"
#include "hw_gpio.h"
#include "hw_watchdog.h"
#include "sys_clock_mgr.h"
#include "sys_power_mgr.h"

#include "hw_pdc.h"
#include "hw_wkup.h"
#include "hw_sys.h"

#include "hw_spi.h"

#include "ADC_Interface\ADCAdapter.h"
#include "ADC_Interface\GPADCAdapter.h"
#include "ADC_Interface\SDADCAdapter.h"

/* Task priorities */
#define mainTEMPLATE_TASK_PRIORITY              ( OS_TASK_PRIORITY_NORMAL )

/* The rate at which data is template task counter is incremented. */
#define mainCOUNTER_FREQUENCY_MS                OS_MS_2_TICKS(200)
/*
 * Perform any application specific hardware configuration.  The clocks,
 * memory, etc. are configured before main() is called.
 */
static void prvSetupHardware( void );
/*
 * Task functions .
 */
static void prvTemplateTask( void *pvParameters );

static OS_TASK xHandle;

__RETAINED static OS_EVENT signal_pot;

ADC_Selected_Config Local_ADC_Config;

volatile bool SetUP = false;

//int Count = 0;
//
//int Tick = 0;
//
//int nominal = 0;
//
//bool ReadStart = false;

static void system_init( void *pvParameters )
{
        OS_TASK task_h = NULL;

#if defined CONFIG_RETARGET
        extern void retarget_init(void);
#endif

        cm_sys_clk_init(sysclk_XTAL32M);
        cm_apb_set_clock_divider(apb_div1);
        cm_ahb_set_clock_divider(ahb_div1);
        cm_lp_clk_init();

        /* Prepare the hardware to run this demo. */
        prvSetupHardware();

#if defined CONFIG_RETARGET
        retarget_init();
#endif

        /*
         * Upon a wakeup cycle, wait for the XTAL32M crystal to settle.
         * BLE, USB and UART blocks require the XTAL32M to be up and
         * running to work properly.
         */
        pm_set_wakeup_mode(true);


        /* Set the desired sleep mode. */
        pm_sleep_mode_set(pm_mode_extended_sleep);

        /*
         * Set the desired wakeup mode.
         *
         * \warning When set is Ultra-Fast wakeup mode, sleep voltage should be 0.9V
         *          and not less than that.
         *
         **/
        pm_set_sys_wakeup_mode(pm_sys_wakeup_mode_fast);

        OS_EVENT_CREATE(signal_pot);

        /* Start main task here (text menu available via UART1 to control application) */
        OS_TASK_CREATE( "Template",            /* The text name assigned to the task, for
                                                           debug only; not used by the kernel. */
                        prvTemplateTask,                /* The function that implements the task. */
                        NULL,                           /* The parameter passed to the task. */
                        configMINIMAL_STACK_SIZE * OS_STACK_WORD_SIZE,
                                                        /* The number of bytes to allocate to the
                                                           stack of the task. */
                        mainTEMPLATE_TASK_PRIORITY,     /* The priority assigned to the task. */
                        task_h );                       /* The task handle */
        OS_ASSERT(task_h);

        /* the work of the SysInit task is done */
        OS_TASK_DELETE( xHandle );
}

// Default build for the ADC config, use for testing.
ADC_Selected_Config ADC_Config(void)
{
        ADC_io_conf GP_io_configP0 = {
                .port = ADC_GPIO_PORT_1,
                .pin  = ADC_GPIO_PIN_12,
                .on   = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_ADC,  INPUT_TRUE},
                .off  = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_GPIO, INPUT_TRUE}
        };

        ADC_io_conf GP_io_configP1 = {
                .port = ADC_GPIO_PORT_MAX,
                .pin  = ADC_GPIO_PIN_MAX,
                .on   = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_GPIO,  INPUT_TRUE},
                .off  = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_GPIO, INPUT_TRUE}
        };

        ADC_BUS GP_ADC_io_bus = {
                .input0        = GP_io_configP0,
                .input1        = GP_io_configP1,
                .voltage_level = ADC_GPIO_POWER_VDD1V8P,
        };

        GP_ADC_driver_config GP_Driver = {
                .clock                  = HW_GPADC_CLOCK_INTERNAL,
                .input_mode             = HW_GPADC_INPUT_MODE_SINGLE_ENDED,
                .input                  = HW_GPADC_INPUT_SE_TEMPSENS,
                .temp_sensor            = HW_GPADC_TEMP_SENSOR_NEAR_RADIO,
                .sample_time            = 2,
                .chopping               = false,
                .oversampling           = HW_GPADC_OVERSAMPLING_8_SAMPLES,
                .input_attenuator       = HW_GPADC_INPUT_VOLTAGE_UP_TO_1V2,
        };

        GP_ADC_Config GP_Config = {
                .error      = 0,
                .io_bus     = GP_ADC_io_bus,
                .driver     = GP_Driver,
                .configured = false,
        };

        ADC_io_conf SD_io_configP0 = {
                .port = ADC_GPIO_PORT_0,
                .pin  = ADC_GPIO_PIN_25,
                .on   = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_ADC,  INPUT_TRUE},
                .off  = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_GPIO, INPUT_TRUE}
        };

        ADC_io_conf SD_io_configP1 = {
                .port = ADC_GPIO_PORT_MAX,
                .pin  = ADC_GPIO_PIN_MAX,
                .on   = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_GPIO,  INPUT_TRUE},
                .off  = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_GPIO, INPUT_TRUE}
        };

        ADC_BUS SD_ADC_io_bus = {
                .input0        = SD_io_configP0,
                .input1        = SD_io_configP1,
                .voltage_level = ADC_GPIO_POWER_VDD1V8P,
        };

        SD_ADC_driver_config SD_Driver = {
                .inn                    = SD_ADC_IN_ADC1_P0_25,
                .inp                    = SD_ADC_IN_ADC1_P0_25,
                .clock                  = SD_ADC_CLOCK_0,
                .freq                   = SD_ADC_CLOCK_FREQ_1M,
                .use_dma                = SD_ADC_DMA_FALSE,
                .mask_int               = SD_ADC_MASK_FALSE,
                .input_mode             = SD_ADC_INPUT_MODE_SINGLE_ENDED,
                .continuous             = ADC_CONTINUOUS_FALSE,
                .vref_voltage           = SD_ADC_VREF_VOLTAGE_MAX,
                .over_sampling          = SD_ADC_OSR_128,
                .vref_selection         = SD_ADC_VREF_INTERNAL,
        };

        SD_ADC_Config SD_Config = {
                .error      = 0,
                .io_bus     = SD_ADC_io_bus,
                .driver     = SD_Driver,
                .configured = false,
        };

        ADC_Selected_Config ADC_Master = {
                .ADC_to_use    = SD_ADC,
                .GP_ADC_config = GP_Config,
                .SD_ADC_config = SD_Config,
        };

        return ADC_Master;
}

/**
 * @brief Template main creates a SysInit task, which creates a Template task
 */

int main( void ){
        OS_BASE_TYPE status;

//        Local_ADC_Config = ADC_Config();

        /* Start the two tasks as described in the comments at the top of this
        file. */
        status = OS_TASK_CREATE("SysInit",              /* The text name assigned to the task, for
                                                           debug only; not used by the kernel. */
                        system_init,                    /* The System Initialization task. */
                        ( void * ) 0,                   /* The parameter passed to the task. */
                        configMINIMAL_STACK_SIZE * OS_STACK_WORD_SIZE,
                                                        /* The number of bytes to allocate to the
                                                           stack of the task. */
                        OS_TASK_PRIORITY_HIGHEST,       /* The priority assigned to the task. */
                        xHandle );                      /* The task handle */
        OS_ASSERT(status == OS_TASK_CREATE_SUCCESS);

        /* Start the tasks and timer running. */
        vTaskStartScheduler();

        /* If all is well, the scheduler will now be running, and the following
        line will never be reached.  If the following line does execute, then
        there was insufficient FreeRTOS heap memory available for the idle and/or
        timer tasks to be created.  See the memory management section on the
        FreeRTOS web site for more details. */
        for ( ;; );

}

void print_return(uint32_t reading)
{
        hw_gpio_set_inactive(HW_GPIO_PORT_0, HW_GPIO_PIN_1);

        SetUP = true;

        printf("%d\n\r", reading);
        fflush(stdout);
}

void print_return_temp(uint32_t reading)
{
        hw_gpio_set_inactive(HW_GPIO_PORT_0, HW_GPIO_PIN_1);

//        if (reading == 65535)
//        {
//                reading = 0;
//        }

        SetUP = true;

        uint32_t val32 = reading << (6 - MIN(6, 3));

        uint32_t test = (int32_t)((val32 - 41024) * 1000) / 82048;

        printf("%d\n\r", test);
        fflush(stdout);

//        if (reading != 0)
//        {
//                printf("%d\n\r", reading);
//                fflush(stdout);
//        }
}

bool Triggered = false;

/**
 * @brief Template task increases a counter every mainCOUNTER_FREQUENCY_MS ms
 * Cheange to run the desired code
 */
static void prvTemplateTask( void *pvParameters )
{
        SetUP = true;

//        Pressure_Setup();
//        Interupt_setup();

//        Accelerometer_Setup();
//        Interupt_SetUp();

        for (;;) {
                /*
                 * Suspend task execution - As soon as WKUP callback function
                 * is triggered, the task resumes its execution.
                 */

//                Still_State();

//                SPI_IO_configure();
//
//                SPI_Init(HW_SPI1);
//
//                while(1)
//                {
//                        Set_Power_Mode_SPI();
//                }

//                while(1)
//                {
//                        Read_Pressure();
//                }

//                Accelerometer_Setup();
//
////                Knock_Knock();
//

                OS_EVENT_WAIT(signal_pot, OS_EVENT_FOREVER);

//                Read_StartHandle();

//                ReadStart = true;

//                Read_Pressure();

//                while(1)
//                {
////                       Accelerometer_Setup();
////
////                       Tap_Setup();
//
////                       uint16_t *ArrayPointer;
////
////                       volatile uint16_t CheckX[3];
////
////                       ArrayPointer = Read_X_Y_Z();
////
////                       volatile uint16_t CheckG[3];
////                       Read_Gyro();
////
//                       Pressure_Setup();
//
////                       Read_Pressure();
//
//                       Interupt_setup();
//
//                       //Read_8Bit(0x0F);
////                       Read_Pressure();
//                }
        }
}

///* WKUP KEY interrupt handler */
//static void wkup_cb(void)
//{
//        hw_wkup_reset_interrupt();
//
//        OS_EVENT_SIGNAL_FROM_ISR(signal_pot);
//}

void Wake_Up_Int_1(void)
{
        hw_wkup_unregister_interrupts();


        if (hw_gpio_get_pin_status(HW_GPIO_PORT_1, HW_GPIO_PIN_14))
        {
                Pin_Cause(HW_GPIO_PIN_14);
                Triggered = true;
        }

        else if(hw_gpio_get_pin_status(HW_GPIO_PORT_1, HW_GPIO_PIN_15))
        {
                Pin_Cause(HW_GPIO_PIN_15);
                Triggered = true;
        }

        wkup_Reset();
}

/* Initialize the WKUP controller */
void wkup_init(void)
{
        /* Initialize the WKUP controller */
        hw_wkup_init(NULL);

        /*
         * Set debounce time expressed in ms. Maximum allowable value is 63 ms.
         * A value set to 0 disables the debounce functionality.
         */
        hw_wkup_set_debounce_time(10);

//        /*
//         * Enable interrupts produced by the KEY block of the wakeup controller (debounce
//         * circuitry) and register a callback function to hit following a KEY event.
//         */
//        hw_wkup_register_key_interrupt(wkup_cb, 1);
//
//
//        /*
//         * Set the polarity (rising/falling edge) that triggers the WKUP controller.
//         *
//         * \note The polarity is applied both to KEY and GPIO blocks of the controller
//         *
//         */
//        hw_wkup_configure_pin(HW_GPIO_PORT_0, HW_GPIO_PIN_1, 1, HW_WKUP_PIN_STATE_HIGH);

        hw_wkup_register_gpio_p1_interrupt(Wake_Up_Int_1, 1);

        hw_wkup_configure_port(HW_GPIO_PORT_1, 1, 1, 0b11111111100000001100000000000001);

        hw_wkup_gpio_configure_pin(HW_GPIO_PORT_1, HW_GPIO_PIN_14, 1, HW_WKUP_PIN_STATE_HIGH);

        hw_wkup_gpio_configure_pin(HW_GPIO_PORT_1, HW_GPIO_PIN_15, 1, HW_WKUP_PIN_STATE_HIGH);

        /* Enable interrupts of WKUP controller */
        hw_wkup_enable_irq();
}

void wkup_Reset(void)
{
        hw_wkup_init(NULL);

        hw_wkup_set_debounce_time(10);

        hw_wkup_register_gpio_p1_interrupt(Wake_Up_Int_1, 1);

        hw_wkup_configure_port(HW_GPIO_PORT_1, 1, 1, 0b11111111100000001100000000000001);

        hw_wkup_gpio_configure_pin(HW_GPIO_PORT_1, HW_GPIO_PIN_14, 1, HW_WKUP_PIN_STATE_HIGH);

        hw_wkup_gpio_configure_pin(HW_GPIO_PORT_1, HW_GPIO_PIN_15, 1, HW_WKUP_PIN_STATE_HIGH);

        hw_wkup_enable_irq();
}

/**
 * @brief Initialize the peripherals domain after power-up.
 *
 */
static void periph_init(void)
{

}

/**
 * @brief Hardware Initialization
 */
static void prvSetupHardware( void )
{
        /* Init hardware */
        pm_system_init(periph_init);

        Accelerometer_Setup();
        Interupt_SetUp();

        hw_sys_pd_com_enable();

//        hw_gpio_set_pin_function(HW_GPIO_PORT_0, HW_GPIO_PIN_1, HW_GPIO_MODE_OUTPUT, HW_GPIO_FUNC_GPIO);
//        hw_gpio_configure_pin_power(HW_GPIO_PORT_0, HW_GPIO_PIN_1, HW_GPIO_POWER_VDD1V8P);
//        hw_gpio_pad_latch_enable(HW_GPIO_PORT_0, HW_GPIO_PIN_1);

//        ADC_Hardware_setup(&Local_ADC_Config);
        //SPI_IO_configure();

        hw_gpio_set_pin_function(HW_GPIO_PORT_1, HW_GPIO_PIN_14, HW_GPIO_MODE_INPUT_PULLUP, HW_GPIO_FUNC_GPIO);
        hw_gpio_pad_latch_enable(HW_GPIO_PORT_1, HW_GPIO_PIN_14);
        hw_gpio_pad_latch_disable(HW_GPIO_PORT_1, HW_GPIO_PIN_14);

        hw_gpio_set_pin_function(HW_GPIO_PORT_1, HW_GPIO_PIN_15, HW_GPIO_MODE_INPUT_PULLUP, HW_GPIO_FUNC_GPIO);
        hw_gpio_pad_latch_enable(HW_GPIO_PORT_1, HW_GPIO_PIN_15);
        hw_gpio_pad_latch_disable(HW_GPIO_PORT_1, HW_GPIO_PIN_15);

        wkup_init();

        hw_sys_pd_com_disable();
}

/**
 * @brief Malloc fail hook
 */
void vApplicationMallocFailedHook( void )
{
        /* vApplicationMallocFailedHook() will only be called if
        configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.  It is a hook
        function that will get called if a call to OS_MALLOC() fails.
        OS_MALLOC() is called internally by the kernel whenever a task, queue,
        timer or semaphore is created.  It is also called by various parts of the
        demo application.  If heap_1.c or heap_2.c are used, then the size of the
        heap available to OS_MALLOC() is defined by configTOTAL_HEAP_SIZE in
        FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
        to query the size of free heap space that remains (although it does not
        provide information on how the remaining heap might be fragmented). */
        ASSERT_ERROR(0);
}

/**
 * @brief Application idle task hook
 */
void vApplicationIdleHook( void )
{
        /* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
        to 1 in FreeRTOSConfig.h.  It will be called on each iteration of the idle
        task.  It is essential that code added to this hook function never attempts
        to block in any way (for example, call OS_QUEUE_GET() with a block time
        specified, or call OS_DELAY()).  If the application makes use of the
        OS_TASK_DELETE() API function (as this demo application does) then it is also
        important that vApplicationIdleHook() is permitted to return to its calling
        function, because it is the responsibility of the idle task to clean up
        memory allocated by the kernel to any task that has since been deleted. */
}

/**
 * @brief Application stack overflow hook
 */
void vApplicationStackOverflowHook( OS_TASK pxTask, char *pcTaskName )
{
        ( void ) pcTaskName;
        ( void ) pxTask;

        /* Run time stack overflow checking is performed if
        configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
        function is called if a stack overflow is detected. */
        ASSERT_ERROR(0);
}

void TickHandel(void)
{
        OS_EVENT_SIGNAL_FROM_ISR(signal_pot);
}

bool signal = false;

/**
 * @brief Application tick hook
 */
void vApplicationTickHook( void )
{
//        Pressure_Ticks();

//        Accel_Gryo_Pwr_Up_Read();

//        if (!signal)
//        {
//                TickHandel();
//
//                signal = true;
//        }
//
//        Tick_increments();
//
//        if (Triggered)
//        {
//                Triggered = Accel_Ticks();
//
//                if (!Triggered)
//                {
//                        wkup_Reset();
//                }
//        }
}

