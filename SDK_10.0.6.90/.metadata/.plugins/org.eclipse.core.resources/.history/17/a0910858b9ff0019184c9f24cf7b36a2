/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: GPADCAdapter.c
 * Purpose:
 * Version: V1.0
 * Author: E Denton
 * Version
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/

#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include "osal.h"
#include "hw_cpm.h"
#include "hw_pdc.h"
#include "hw_sys.h"
#include "hw_wkup.h"
#include "resmgmt.h"
#include "hw_gpio.h"
#include "ad_gpadc.h"
#include "hw_gpadc.h"
#include "hw_watchdog.h"
#include "sys_clock_mgr.h"
#include "sys_power_mgr.h"

#include "ADCAdapter.h"

// Type structure required for the General purpose ADC
typedef const void* gp_adc_device;
gp_adc_device GP_ADC_DEVICE;

ad_gpadc_handle_t GP_DEV;

ad_gpadc_io_conf_t GP_ADC_BUS = {
        .input0 = {
                .port = HW_GPIO_PORT_NONE,
                .pin  = ADC_GPIO_PIN_MAX,
                .on   = {HW_GPIO_MODE_NONE, ADC_GPIO_FUNCTION_LAST,  INPUT_TRUE},
                .off  = {HW_GPIO_MODE_NONE, ADC_GPIO_FUNCTION_LAST, INPUT_TRUE}
        },
        .input1 = {
                .port = HW_GPIO_PORT_NONE,
                .pin  = ADC_GPIO_PIN_MAX,
                .on   = {HW_GPIO_MODE_NONE, ADC_GPIO_FUNCTION_LAST,  INPUT_TRUE},
                .off  = {HW_GPIO_MODE_NONE, ADC_GPIO_FUNCTION_LAST, INPUT_TRUE}
        },
        .voltage_level = ADC_GPIO_POWER_NONE
};

//Set to a clear default
ad_gpadc_driver_conf_t GP_ADC_DRV = {
        .clock                  = GP_ADC_CLOCK_INTERNAL,
        .input                  = GP_ADC_INPUT_SE_P0_25,
        .chopping               = GP_ADC_CHOPPING_TRUE,
        .continous              = ADC_CONTINUOUS_FALSE,
        .input_mode             = GP_ADC_INPUT_MODE_SINGLE_ENDED,
        .temp_sensor            = GP_ADC_NO_TEMP_SENSOR,
        .sample_time            = GP_ADC_SAMPLE_5,
        .oversampling           = GP_ADC_OVERSAMPLING_128_SAMPLES,
        .input_attenuator       = GP_ADC_INPUT_VOLTAGE_UP_TO_1V2
};

//Set to a clear default
ad_gpadc_controller_conf_t GP_ADC_DEV = {
        .id  = HW_GPADC_1,
        .io  = &GP_ADC_BUS,
        .drv = &GP_ADC_DRV
};

static ad_gpadc_handle_t GP_Open_dev;

gp_adc_device GP_ADC_DEVICE = &GP_ADC_DEV;

void (*Loacl_callback)(int Reading);

//-----------[O]----------//

int GP_ADC_Configure(GP_ADC_Config *Config)
{
        if (Config != NULL && Config->io_bus.input0.pin != ADC_GPIO_PIN_MAX)
        {
                ad_gpadc_io_conf_t Temp_BUS = {
                        .input0 = {
                                .pin  = Config->io_bus.input0.pin,
                                .port = Config->io_bus.input0.port,
                                .on   = {
                                        Config->io_bus.input0.on.mode,
                                        Config->io_bus.input0.on.function,
                                        Config->io_bus.input0.on.high
                                },
                                .off  = {
                                        Config->io_bus.input0.off.mode,
                                        Config->io_bus.input0.off.function,
                                        Config->io_bus.input0.off.high
                                }
                        },
                        .input1 = {
                                .pin  = Config->io_bus.input1.pin,
                                .port = Config->io_bus.input1.port,
                                .on   = {
                                        Config->io_bus.input1.on.mode,
                                        Config->io_bus.input1.on.function,
                                        Config->io_bus.input1.on.high
                                },
                                .off  = {
                                        Config->io_bus.input1.off.mode,
                                        Config->io_bus.input1.off.function,
                                        Config->io_bus.input1.off.high
                                }
                        },
                        .voltage_level = Config->io_bus.voltage_level
                };

                GP_ADC_BUS = Temp_BUS;

                ad_gpadc_driver_conf_t Temp_Driver = {
                        .clock            = Config->driver.clock,
                        .input            = Config->driver.input,
                        .chopping         = Config->driver.chopping,
                        .continous        = Config->driver.continous,
                        .input_mode       = Config->driver.input_mode,
                        .temp_sensor      = Config->driver.temp_sensor,
                        .sample_time      = Config->driver.sample_time,
                        .oversampling     = Config->driver.oversampling,
                        .input_attenuator = Config->driver.input_attenuator,
                };

                GP_ADC_DRV = Temp_Driver;

                GP_ADC_DEV.io  = &GP_ADC_BUS;
                GP_ADC_DEV.drv = &GP_ADC_DRV;

                GP_ADC_DEVICE = &GP_ADC_DEV;

                Config->configured = true;

                return true;
        }
        return false;
}

//-----------[O]----------//

void GP_ADC_Clear_Configuration(void)
{
        ad_gpadc_io_conf_t Temp_BUS = {
                .input0 = {
                                .port = HW_GPIO_PORT_NONE,
                                .pin  = ADC_GPIO_PIN_MAX,
                                .on   = {HW_GPIO_MODE_NONE, ADC_GPIO_FUNCTION_LAST,  INPUT_TRUE},
                                .off  = {HW_GPIO_MODE_NONE, ADC_GPIO_FUNCTION_LAST, INPUT_TRUE}
                        },
                        .input1 = {
                                .port = HW_GPIO_PORT_NONE,
                                .pin  = ADC_GPIO_PIN_MAX,
                                .on   = {HW_GPIO_MODE_NONE, ADC_GPIO_FUNCTION_LAST,  INPUT_TRUE},
                                .off  = {HW_GPIO_MODE_NONE, ADC_GPIO_FUNCTION_LAST, INPUT_TRUE}
                        },
                        .voltage_level = ADC_GPIO_POWER_NONE
        };

        GP_ADC_BUS = Temp_BUS;

        ad_gpadc_driver_conf_t Temp_Driver = {
                .input                  = GP_ADC_INPUT_SE_P0_25,
                .clock                  = GP_ADC_CLOCK_INTERNAL,
                .chopping               = GP_ADC_CHOPPING_TRUE,
                .continous              = ADC_CONTINUOUS_FALSE,
                .input_mode             = GP_ADC_INPUT_MODE_SINGLE_ENDED,
                .temp_sensor            = GP_ADC_NO_TEMP_SENSOR,
                .sample_time            = GP_ADC_SAMPLE_1,
                .oversampling           = GP_ADC_OVERSAMPLING_1_SAMPLE,
                .input_attenuator       = GP_ADC_INPUT_VOLTAGE_UP_TO_1V2
        };

        GP_ADC_DRV = Temp_Driver;

        GP_ADC_DEV.io  = &GP_ADC_BUS;
        GP_ADC_DEV.drv = &GP_ADC_DRV;

        GP_ADC_DEVICE = &GP_ADC_DEV;
}

//-----------[O]----------//

int GP_ADC_convert(gp_adc_device src, int value)
{
        ad_gpadc_controller_conf_t *cfg = (ad_gpadc_controller_conf_t *)src;

        const uint16 adc_src_max =  ad_gpadc_get_source_max(cfg->drv);
        uint32_t mv_src_max = (cfg->drv->input_attenuator ==
                                     HW_GPADC_INPUT_VOLTAGE_UP_TO_1V2) ? 1200 : 3600;
        int ret = 0;

        switch (cfg->drv->input_mode) {
        case HW_GPADC_INPUT_MODE_SINGLE_ENDED:
                if (cfg->drv->input == HW_GPADC_INPUT_SE_VBAT) {
                        mv_src_max = 5000;
                }
                ret =  (mv_src_max * value) / adc_src_max;
                break;
        case HW_GPADC_INPUT_MODE_DIFFERENTIAL:
                ret = ((int)mv_src_max * (value - (adc_src_max >> 1))) /
                                                         (adc_src_max >> 1);
                break;
        }

        return ret;
}

//-----------[O]----------//

//int GP_min   = 0;
//int GP_max   = 0;
//int GP_pass  = 0;
//int GP_total = 0;
//
//void GP_ADC_Adverage(void *user_data, int value)
//{
//        if (GP_pass > 6)
//        {
//                GP_total = (GP_total - (GP_max + GP_min)) / 6;
//                GP_ADC_Value_set(&user_data, GP_total);
//        }
//
//        if (value > 0)
//        {
//                if (GP_min == 0 || GP_min > value)
//                {
//                        GP_min = value;
//                }
//
//                if (GP_max < value)
//                {
//                        GP_max = value;
//                }
//
//                GP_total += value;
//
//                GP_pass++;
//        }
//
//        if (GP_pass < 7)
//        {
//                GP_ADC_Callback();
//        }
//}
//
//void GP_ADC_Callback(void)
//{
//        ad_gpadc_acquire();
//
//        hw_gpadc_register_interrupt(ad_gpadc_cb());
//
//        /* Start actual conversion */
//        hw_gpadc_start();
//}

void GPADC_SetupHardware(void)
{
        ad_gpadc_io_config(((ad_gpadc_controller_conf_t *)GP_ADC_DEVICE)->id,
                ((ad_gpadc_controller_conf_t *)GP_ADC_DEVICE)->io, AD_IO_CONF_OFF);
}

//-----------[O]----------//

void GP_ADC_Value_set(void *user_data, int value)
{
        int *pData = (int*)user_data;

        *pData = value;

        int send = GP_ADC_convert(GP_ADC_DEVICE, value);

        OS_DELAY(1);

        ad_gpadc_close(GP_Open_dev, true);

        (*Loacl_callback)(send);
}

//-----------[O]----------//

void GP_ADC_Read(void(*Call)(int reading))
{
        Loacl_callback = Call;

        int Read_Error; // Used to check if an error occurred
        int Value_read; // The library requires an integer pointer, which is what this value is used for

        ad_gpadc_handle_t GP_Device = ad_gpadc_open((ad_gpadc_controller_conf_t *)GP_ADC_DEVICE);

        GP_Open_dev = GP_Device;

        Read_Error = ad_gpadc_read_async(GP_Device, GP_ADC_Value_set, (void *)&Value_read);

        if (Read_Error < 0)
        {
                // An error has occured
        }

        //OS_DELAY(1);

        ad_gpadc_close(GP_Open_dev, true);
}

//-----------[O]----------//

/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/

