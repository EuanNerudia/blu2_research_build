/*
 * I2CAdapter.c
 *
 *  Created on: 21 Nov 2019
 *      Author: Euan Denton
 */

#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include "hw_pdc.h"
#include "hw_i2c.h"
#include "ad_i2c.h"
#include "hw_wkup.h"
#include "hw_sys.h"

const ad_i2c_io_conf_t i2c_Io_Con = {
        .scl = {
                .port = HW_GPIO_PORT_0, .pin = HW_GPIO_PIN_30,
                .on =  { HW_GPIO_MODE_OUTPUT_OPEN_DRAIN, HW_GPIO_FUNC_I2C_SCL, false },
                .off = { HW_GPIO_MODE_INPUT,             HW_GPIO_FUNC_GPIO,    true  }
        },
        .sda = {
                .port = HW_GPIO_PORT_0, .pin =HW_GPIO_PIN_31,
                .on =  { HW_GPIO_MODE_OUTPUT_OPEN_DRAIN, HW_GPIO_FUNC_I2C_SDA, false },
                .off = { HW_GPIO_MODE_INPUT,             HW_GPIO_FUNC_GPIO,    true  }
        },
        .voltage_level = HW_GPIO_POWER_VDD1V8P
};

const ad_i2c_driver_conf_t drv_EEPROM_24FC256 = {
        I2C_DEFAULT_CLK_CFG,
        .i2c.speed              = HW_I2C_SPEED_STANDARD,
        .i2c.mode               = HW_I2C_MODE_MASTER,
        .i2c.addr_mode          = HW_I2C_ADDRESSING_7B,
        .i2c.address            = 0b1101000,
        .dma_channel            = HW_DMA_CHANNEL_2
};

/* EEPROM 24FC256 I2C controller configuration */
const ad_i2c_controller_conf_t dev_24FC256 = {
        .id     = HW_I2C1,
        .io     = &i2c_Io_Con,
        .drv    = &drv_EEPROM_24FC256
};

#define CTRL3_ADDR 0x09
#define CTRL3_COMM 0b00110011

#define CTRL4_ADDR 0x0A
#define CTRL4_COMM 0b00000110

#define CTRL1_XL_A 0x10
#define CTRL1_XL_C 0b01000100

#define Read_ADDR 0xD7
#define Default_COMM 0x00

const HW_I2C_ID Local_I2C_ID = HW_I2C1;

uint8_t Read_8Bit(uint8_t Register)
{
        IBA(Local_I2C_ID)->I2C_DATA_CMD_REG = Register & I2C_I2C_DATA_CMD_REG_I2C_DAT_Msk | I2C_I2C_DATA_CMD_REG_I2C_RESTART_Msk;
        IBA(Local_I2C_ID)->I2C_DATA_CMD_REG = I2C_I2C_DATA_CMD_REG_I2C_CMD_Msk | I2C_I2C_DATA_CMD_REG_I2C_STOP_Msk;
        uint8_t Value = HW_I2C_REG_GETF(Local_I2C_ID, I2C_DATA_CMD, I2C_DAT);

        return Value;
}

uint16_t Read_16Bit(uint8_t Register1, uint8_t Register2)
{
        uint8_t Buffer[2];

        Buffer[0] = Read_8Bit(Register1);
        Buffer[1] = Read_8Bit(Register2);

        // the int Word is used to build the full 16bit word from the two 8bit register reads.
        volatile uint16_t Word = Buffer[1] << 8 | Buffer[0];

        return Word;
}

uint32_t Read_24Bit(uint8_t Register1, uint8_t Register2, uint8_t Register3)
{
        uint8_t Buffer[3];

        Buffer[0] = Read_8Bit(Register1);
        Buffer[1] = Read_8Bit(Register2);
        Buffer[2] = Read_8Bit(Register3);

        // the int Word is used to build the full 16bit word from the two 8bit register reads.
        volatile uint32_t Word = Buffer [2] << 16 | Buffer[1] << 8 | Buffer[0];

        return Word;
}

void Send_Command(uint8_t Register, uint8_t Command, bool restart)
{
        if (restart)
        {
                IBA(Local_I2C_ID)->I2C_DATA_CMD_REG = Register & I2C_I2C_DATA_CMD_REG_I2C_DAT_Msk;
                IBA(Local_I2C_ID)->I2C_DATA_CMD_REG = Command & I2C_I2C_DATA_CMD_REG_I2C_DAT_Msk | I2C_I2C_DATA_CMD_REG_I2C_STOP_Msk;
        }
        else
        {
                IBA(Local_I2C_ID)->I2C_DATA_CMD_REG = Register & I2C_I2C_DATA_CMD_REG_I2C_DAT_Msk | I2C_I2C_DATA_CMD_REG_I2C_RESTART_Msk;
                IBA(Local_I2C_ID)->I2C_DATA_CMD_REG = I2C_I2C_DATA_CMD_REG_I2C_CMD_Msk; //| I2C_I2C_DATA_CMD_REG_I2C_RESTART_Msk ;
        }
}

void Accelerometer_Set_Up_And_Read(void)
{
        Send_Command(0x7E, 0x11, true);
//        Send_Command(CTRL4_ADDR, CTRL4_COMM, true);
//        Send_Command(CTRL1_XL_A, CTRL1_XL_C, true);
//        Send_Command(Read_ADDR, Default_COMM, false);

        volatile uint16_t AccBuff[3];

        while(true)
        {
                AccBuff[0] = Read_16Bit(0x12, 0x13);
                AccBuff[1] = Read_16Bit(0x14, 0x15);
                AccBuff[2] = Read_16Bit(0x16, 0x17);
        }
}

void PressureSensor_Set_UP_And_Read(void)
{
        volatile uint32_t Current = 0;

        Send_Command(0x1B, 0x31, false);

        volatile uint8_t Test = 0;

        Test = Read_8Bit(0x1B);

        while(true)
        {
                Current = Read_24Bit(0x04, 0x05, 0x06);
                if (Current > 0)
                {
                        printf("Check");
                }
        }
}

void Change_Slave_address(uint8_t address)
{
       while (hw_i2c_is_master_busy(Local_I2C_ID));

        /* Now is safe to disable the I2C to change the Target Address */
       hw_i2c_disable(Local_I2C_ID);

       /* Change the Target Address */
       HW_I2C_REG_SETF(Local_I2C_ID, I2C_TAR, IC_TAR, address);

       /* Enable again the I2C to use the new address */
       hw_i2c_enable(Local_I2C_ID);
}

void I2C_Configure(void)
{
        hw_sys_pd_com_enable();

        HW_GPIO_FUNC scl_function = ((Local_I2C_ID == HW_I2C1) ? HW_GPIO_FUNC_I2C_SCL :
                                                               HW_GPIO_FUNC_I2C2_SCL);
        HW_GPIO_FUNC sda_function = ((Local_I2C_ID == HW_I2C1) ? HW_GPIO_FUNC_I2C_SDA :
                                                       HW_GPIO_FUNC_I2C2_SDA);
        /* Configure SCL */
        hw_gpio_set_pin_function(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin, HW_GPIO_MODE_OUTPUT,
                                 scl_function);
        hw_gpio_configure_pin_power(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin, i2c_Io_Con.voltage_level);
        /* Configure SDA */
        hw_gpio_set_pin_function(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin, HW_GPIO_MODE_OUTPUT,
                                 sda_function);
        hw_gpio_configure_pin_power(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin, i2c_Io_Con.voltage_level);

        hw_gpio_pad_latch_enable(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin);
        hw_gpio_pad_latch_enable(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin);

        IRQn_Type irq_type = I2C_IRQn;

        if (Local_I2C_ID == HW_I2C1) {
                CRG_COM->RESET_CLK_COM_REG = CRG_COM_RESET_CLK_COM_REG_I2C_CLK_SEL_Msk;
                CRG_COM->SET_CLK_COM_REG = CRG_COM_SET_CLK_COM_REG_I2C_ENABLE_Msk;
        } else {
                irq_type = I2C2_IRQn;
                CRG_COM->RESET_CLK_COM_REG = CRG_COM_RESET_CLK_COM_REG_I2C2_CLK_SEL_Msk;
                CRG_COM->SET_CLK_COM_REG = CRG_COM_SET_CLK_COM_REG_I2C2_ENABLE_Msk;
        }

        hw_i2c_master_abort_transfer(Local_I2C_ID);

        hw_i2c_disable(Local_I2C_ID);

        while (hw_i2c_get_enable_status(Local_I2C_ID) & I2C_I2C_ENABLE_STATUS_REG_IC_EN_Msk) {
                hw_clk_delay_usec(500);
        }

        IBA(Local_I2C_ID)->I2C_INTR_MASK_REG = 0x0000;

        hw_i2c_configure(Local_I2C_ID, &drv_EEPROM_24FC256.i2c);

        NVIC_EnableIRQ(irq_type);

        hw_i2c_init(Local_I2C_ID, &(drv_EEPROM_24FC256.i2c));

        hw_i2c_enable(Local_I2C_ID);

        hw_i2c_reset_abort_source(Local_I2C_ID);
        hw_i2c_reset_int_all(Local_I2C_ID);

        //Accelerometer_Set_Up_And_Read();
        PressureSensor_Set_UP_And_Read();
}

void I2C_IO_Setup(void)
{
        hw_sys_pd_com_enable();

        hw_gpio_set_pin_function(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin, i2c_Io_Con.scl.off.mode, i2c_Io_Con.scl.off.function);
        if (i2c_Io_Con.scl.off.mode == HW_GPIO_MODE_OUTPUT || i2c_Io_Con.scl.off.mode == HW_GPIO_MODE_OUTPUT_PUSH_PULL ||
                i2c_Io_Con.scl.off.mode == HW_GPIO_MODE_OUTPUT_OPEN_DRAIN) {
                if (i2c_Io_Con.scl.off.high) {
                        hw_gpio_set_active(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin);
                } else {
                        hw_gpio_set_inactive(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin);
                }
        }
        hw_gpio_configure_pin_power(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin, i2c_Io_Con.voltage_level);

        hw_gpio_set_pin_function(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin, i2c_Io_Con.sda.off.mode, i2c_Io_Con.sda.off.function);
        if (i2c_Io_Con.sda.off.mode == HW_GPIO_MODE_OUTPUT || i2c_Io_Con.sda.off.mode == HW_GPIO_MODE_OUTPUT_PUSH_PULL ||
                i2c_Io_Con.sda.off.mode == HW_GPIO_MODE_OUTPUT_OPEN_DRAIN) {
                if (i2c_Io_Con.sda.off.high) {
                        hw_gpio_set_active(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin);
                } else {
                        hw_gpio_set_inactive(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin);
                }
        }
        hw_gpio_configure_pin_power(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin, i2c_Io_Con.voltage_level);

        hw_gpio_pad_latch_enable(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin);
        hw_gpio_pad_latch_disable(i2c_Io_Con.scl.port, i2c_Io_Con.scl.pin);
        hw_gpio_pad_latch_enable(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin);
        hw_gpio_pad_latch_disable(i2c_Io_Con.sda.port, i2c_Io_Con.sda.pin);

        hw_sys_pd_com_disable();

        I2C_Configure();
}
