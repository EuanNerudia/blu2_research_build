/*
 * SPIAdapter.c
 *
 *  Created on: 13 Dec 2019
 *      Author: Euan Denton
 */

#include "hw_spi.h"

/* SPI chip-select pins */
static const ad_io_conf_t spi_master_cs[] = {{

        .port = DAC_MC4822_CS_PORT,
        .pin  = DAC_MC4822_CS_PIN,
        .on = {
                .mode     = HW_GPIO_MODE_OUTPUT_PUSH_PULL,
                .function = HW_GPIO_FUNC_SPI_EN,
                .high     = true
        },
        .off = {
                .mode     = HW_GPIO_MODE_OUTPUT_PUSH_PULL,
                .function = HW_GPIO_FUNC_SPI_EN,
                .high     = true
        }},
};

/* SPI1 IO */
const ad_spi_io_conf_t bus_SPI1 = {

        .spi_do = {
                .port = DAC_MC4822_DO_PORT,
                .pin  = DAC_MC4822_DO_PIN,
                .on   = {HW_GPIO_MODE_OUTPUT_PUSH_PULL, HW_GPIO_FUNC_SPI_DO, false},
                .off  = {HW_GPIO_MODE_INPUT,            HW_GPIO_FUNC_GPIO,   true},
        },
        .spi_di = {
                .port = DAC_MC4822_DI_PORT,
                .pin  = DAC_MC4822_DI_PIN,
                .on   = {HW_GPIO_MODE_INPUT, HW_GPIO_FUNC_SPI_DI, false},
                .off  = {HW_GPIO_MODE_INPUT, HW_GPIO_FUNC_GPIO,   true},
        },
        .spi_clk = {
                .port = DAC_MC4822_CLK_PORT,
                .pin  = DAC_MC4822_CLK_PIN,
                .on   = {HW_GPIO_MODE_OUTPUT_PUSH_PULL, HW_GPIO_FUNC_SPI_CLK, false},
                .off  = {HW_GPIO_MODE_INPUT,            HW_GPIO_FUNC_GPIO,    true},
        },

        /*
         * The number of pins in spi_master_cs array.
         *
         * \warning When the SPI bus is used by SNC \p cs_cnt must be always 1
         */
        .cs_cnt = 1,
        .spi_cs = spi_master_cs,

        .voltage_level = HW_GPIO_POWER_V33
};


/* External sensor/module SPI driver */
const ad_spi_driver_conf_t drv_SPI1 = {
        .spi = {
                .cs_pad = {DAC_MC4822_CS_PORT, DAC_MC4822_CS_PIN},
                .word_mode = HW_SPI_WORD_16BIT, /* 2-byte mode */
                .smn_role  = HW_SPI_MODE_MASTER,
                .polarity_mode = HW_SPI_POL_LOW,
                .phase_mode    = HW_SPI_PHA_MODE_0,
                .mint_mode = HW_SPI_MINT_DISABLE,
                .xtal_freq = HW_SPI_FREQ_DIV_8,
                .fifo_mode = HW_SPI_FIFO_RX_TX,
                .disabled  = 0, /* Should be disabled during initialization phase */
                .ignore_cs = false,
                .use_dma   = true,
                .rx_dma_channel = HW_DMA_CHANNEL_0,
                .tx_dma_channel = HW_DMA_CHANNEL_1
        }
};


/* Sensor/module device configuration */
const ad_spi_controller_conf_t dev_SPI_CUSTOM_DEVICE = {
        .id  = HW_SPI1,
        .io  = &bus_SPI1,
        .drv = &drv_SPI1
};
