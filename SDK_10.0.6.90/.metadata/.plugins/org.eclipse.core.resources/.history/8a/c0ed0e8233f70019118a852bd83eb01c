/*
 * ADC_init.c
 *
 *  Created on: 24 Oct 2019
 *      Author: Euan Denton
 */

#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include "osal.h"
#include "resmgmt.h"
#include "hw_cpm.h"
#include "hw_gpio.h"
#include "hw_watchdog.h"
#include "sys_clock_mgr.h"
#include "sys_power_mgr.h"

// Includes required for the ADC
#include "hw_pdc.h"
#include "hw_wkup.h"
#include "hw_sys.h"
#include "peripheral_setup.h"
#include "platform_devices.h"

//includes for sdadc
#include "SDADC_main.h"

//includes for gpadc
#include "GPADC_main.h"

/* Task priorities */
#define  mainGPADC_TASK_PRIORITY    ( OS_TASK_PRIORITY_NORMAL )

/* The rate at which data is template task counter is incremented. */
#define mainCOUNTER_FREQUENCY_MS                OS_MS_2_TICKS(200)

/*
 * Perform any application specific hardware configuration.  The clocks,
 * memory, etc. are configured before main() is called.
 */
static void prvSetupHardware( void );

/*
 * Task functions .
 */
static OS_TASK xHandle;

// If set to false the output will be constant,
// if true then the ADC read will be condcuted on the
// press of the K1 button, this can be changed to
// another interrupt.
#define ON_INTER                    (0)

#define RUN_SDADC                   (0)

#define OHM                         (0)

bool SET_UP = false;

/* Retained symbols */
__RETAINED static OS_EVENT signal_pot;

/* GPADC Task handle */
__RETAINED static OS_TASK prvGPADCTask_h;

uint32_t pdc_wkup_combo_id  __attribute__((unused));

int LastArrayPosition = 0;

/*
 * Task functions
 */
static void ADC_prvGPADCTask_POT(void *pvParameters);

static void ADC_system_init(void *pvParameters)
{
        OS_BASE_TYPE status;

        REG_SETF(GPREG, DEBUG_REG, SYS_CPU_FREEZE_EN, 0);

#if defined CONFIG_RETARGET
        extern void retarget_init(void);
#endif /* CONFIG_RETARGET */

        /*
         * Prepare clocks. Note: cm_cpu_clk_set() and cm_sys_clk_set() can be called only
         * from a task since they will suspend the task until the XTAL32M has settled and,
         * maybe, the PLL is locked.
         */
        cm_sys_clk_init(sysclk_XTAL32M);
        cm_apb_set_clock_divider(apb_div1);
        cm_ahb_set_clock_divider(ahb_div1);
        cm_lp_clk_init();

        /* Prepare the hardware to run this demo */
        prvSetupHardware();

#if defined CONFIG_RETARGET
        retarget_init();
#endif /* CONFIG_RETARGET */

        OS_EVENT_CREATE(signal_pot);

        if(ON_INTER)
        {
                /*
                 * Upon a wakeup cycle, wait for the XTAL32M crystal to settle.
                 * BLE, USB and UART blocks require the XTAL32M to be up and
                 * running to work properly.
                 */
                pm_set_wakeup_mode(true);

                /* Set the desired sleep mode. */
                pm_sleep_mode_set(pm_mode_extended_sleep);

                /*
                 * Set the desired wakeup mode.
                 *
                 * \warning When set is Ultra-Fast wakeup mode, sleep voltage should be 0.9V
                 *          and not less than that.
                 *
                 **/
                pm_set_sys_wakeup_mode(pm_sys_wakeup_mode_fast);
        }

        /* GPADC task  */
        status = OS_TASK_CREATE("GPADC",     /* The text name assigned to the task, for
                                             debug only; not used by the kernel. */
                        ADC_prvGPADCTask_POT,    /* The function that implements the task. */
                        NULL,               /* The parameter passed to the task. */
                        1024 * OS_STACK_WORD_SIZE,  /* Stack size allocated for the task
                                                    in bytes. */
                        mainGPADC_TASK_PRIORITY, /* The priority assigned to the task. */
                        prvGPADCTask_h );       /* The task handle. */

        OS_ASSERT(status == OS_TASK_CREATE_SUCCESS);

        SET_UP = true;

        /* The work of the SysInit task is done */
        OS_TASK_DELETE(xHandle);
}

/* Perform a GPADC read operation */
static void pot_gpadc_reader()
{
        int display = 0;

        hw_gpio_set_active(HW_GPIO_PORT_0, HW_GPIO_PIN_0);

#if RUN_SDADC
        display = SDADC_pot_gpadc_reader(SDADC_DEVICE);
#else
        display = GPADC_pot_gpadc_reader(POT_DEVICE);
#endif

#if OHM
        int Adverages[10];

        int resistance;

        int Total = 0;

        if (Adverages[9] != 0)
        {
                for (int i=0; i<10; i++)
                {
                        Total += Adverages[i];
                }

                Total = Total/10;

                if (Total < 570)
                {
                        display = 1;
                }
                else if (Total < 580)
                {
                        display = 2;
                }
                else if (Total < 587)
                {
                        display = 3;
                }
                else if (Total < 594)
                {
                        display = 4;
                }
                else if (Total < 601)
                {
                        display = 5;
                }

                printf("%d\n\r", //POT value (analog): %d mV\n\r",
                                                         display);

                fflush(stdout);
        }
        else
        {
                Adverages[LastArrayPosition] = display;
                LastArrayPosition++;
        }
#else
         /* Print on the serial console the status of the GPADC operation */
         if (display > -1) {
//                 printf("\n\rPOT value (raw): %d\n\r", //POT value (analog): %d mV\n\r",
//                         display);
                 printf("%d\n\r", //POT value (analog): %d mV\n\r",
                                         display);
         } else {
                 printf("\n\rUnsuccessful GPADC write operation with error code: \n\r");
         }
         fflush(stdout);
#endif
}

/*
 * This task is used to run the
 * ADC read at the right time in the
 * running of the program, either
 * on an interupt or after a previous read
 * has completed.
 */
static void ADC_prvGPADCTask_POT(void *pvParameters)
{
        //Not Getting into here
        printf("\n\r***GPADC Demonstration Example***\n\r\n");

        /*
         * GPADC adapter initialization should be done once at the beginning.
         * Alternatively, this function could be called during system
         * initialization in ADC_system_init().
         */
        ad_gpadc_init();

        for (;;) {
                /*
                 * Suspend task execution - As soon as WKUP callback function
                 * is triggered, the task resumes its execution.
                 */
                OS_EVENT_WAIT(signal_pot, OS_EVENT_FOREVER);

                /* Perform a GPADC read operation */
                pot_gpadc_reader(); // Change to change the operation
        }
}

/*
 * The below method creates the ADC initialisation task
 * and starts the the task scheduler to start the program.
 */
void ADC_start(void)
{
        OS_BASE_TYPE status;

        /* Start the two tasks as described in the comments at the top of this
        file. */
        status = OS_TASK_CREATE("SysInit",              /* The text name assigned to the task, for
                                                           debug only; not used by the kernel. */
                        ADC_system_init,                    /* The System Initialization task. */
                        ( void * ) 0,                   /* The parameter passed to the task. */
                        configMINIMAL_STACK_SIZE * OS_STACK_WORD_SIZE,
                                                        /* The number of bytes to allocate to the
                                                           stack of the task. */
                        OS_TASK_PRIORITY_HIGHEST,       /* The priority assigned to the task. */
                        xHandle );                      /* The task handle */
        OS_ASSERT(status == OS_TASK_CREATE_SUCCESS);

        /* Start the tasks and timer running. */
        vTaskStartScheduler();

        /* If all is well, the scheduler will now be running, and the following
        line will never be reached.  If the following line does execute, then
        there was insufficient FreeRTOS heap memory available for the idle and/or
        timer tasks to be created.  See the memory management section on the
        FreeRTOS web site for more details. */
        for ( ;; );
}

/* WKUP KEY interrupt handler */
static void wakeup_cb(void)
{

        /* Clear the WKUP interrupt flag!!! */
        hw_wkup_reset_interrupt();

        /*
         * Notify [ADC_prvGPADCTask_POT] task that time for performing GPADC operations
         * has elapsed.
         */
        OS_EVENT_SIGNAL_FROM_ISR(signal_pot);
}

/* Initialise the WKUP controller */
static void wakeup_init(void)
{

        /* Initialize the WKUP controller */
        hw_wkup_init(NULL);

        /*
         * Set debounce time expressed in ms. Maximum allowable value is 63 ms.
         * A value set to 0 disables the debounce functionality.
         */
        hw_wkup_set_debounce_time(10);

        /*
         * Enable interrupts produced by the KEY block of the wakeup controller (debounce
         * circuitry) and register a callback function to hit following a KEY event.
         */
        hw_wkup_register_key_interrupt(wakeup_cb, 1);

        /*
         * Set the polarity (rising/falling edge) that triggers the WKUP controller.
         * \note The polarity is applied both to KEY and GPIO blocks of the controller
         */
        hw_wkup_configure_pin(KEY1_PORT, KEY1_PIN, 1, HW_WKUP_PIN_STATE_LOW);

        /* Enable interrupts of WKUP controller */
        hw_wkup_enable_irq();
}


/**
 * @brief Initialize the peripherals domain after power-up.
 *
 */
static void periph_init(void)
{

}

/**
 * @brief Hardware Initialization
 */
static void prvSetupHardware(void)
{
        /*
         * The IRQ produced by the KEY sub block of the wakeup controller
         * (debounced IO IRQ) is multiplexed with other trigger sources
         * (VBUS IRQ, SYS2CMAC IRQ, JTAG present) in a single PDC peripheral
         * trigger ID (HW_PDC_PERIPH_TRIG_ID_COMBO).
         */
#if !defined(CONFIG_USE_BLE) && (!dg_configENABLE_DEBUGGER) && (!dg_configUSE_SYS_CHARGER)

        pdc_wkup_combo_id = hw_pdc_add_entry(HW_PDC_LUT_ENTRY_VAL(
                                                         HW_PDC_TRIG_SELECT_PERIPHERAL,
                                                         HW_PDC_PERIPH_TRIG_ID_COMBO,
                                                         HW_PDC_MASTER_CM33, 0));
        OS_ASSERT(pdc_wkup_combo_id != HW_PDC_INVALID_LUT_INDEX);

        /*  */
        hw_pdc_set_pending(pdc_wkup_combo_id);
        hw_pdc_acknowledge(pdc_wkup_combo_id);
#endif

        /* Initialise the hardware */
        pm_system_init(periph_init);

        /* Enable the COM power domain before handling any GPIO pin */
        hw_sys_pd_com_enable();

#if RUN_SDADC
        SDADC_prvSetupHardware(); // Set up the SDADC hardware
#else
        GPADC_prvSetupHardware(); // Set up the GPADC hardware
#endif
        // If an interrupt is required to read the ADC then set up
        // the interrupt pin.
        if(ON_INTER)
        {
                wakeup_init();

                /* Configure the KEY1 push button on Pro DevKit */
                HW_GPIO_SET_PIN_FUNCTION(KEY1);
                HW_GPIO_PAD_LATCH_ENABLE(KEY1);

                /* Lock the mode of the target GPIO pin */
                HW_GPIO_PAD_LATCH_DISABLE(KEY1);
        }

        // Pin to check the speed of the ADC read, P0_0
        hw_gpio_set_pin_function(HW_GPIO_PORT_0, HW_GPIO_PIN_0, HW_GPIO_MODE_OUTPUT, HW_GPIO_FUNC_GPIO);
        hw_gpio_configure_pin_power(HW_GPIO_PORT_0, HW_GPIO_PIN_0, HW_GPIO_POWER_VDD1V8P);
        hw_gpio_pad_latch_enable(HW_GPIO_PORT_0, HW_GPIO_PIN_0);

        /* Disable the COM power domain after handling the GPIO pins */
        hw_sys_pd_com_disable();
}

void Tick_Handle(void)
{
        if (!ON_INTER && SET_UP)
        {
                wakeup_cb();
        }
}


