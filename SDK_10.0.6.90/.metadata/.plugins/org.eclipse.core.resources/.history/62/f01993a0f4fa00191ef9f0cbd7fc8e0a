/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: .c
 * Purpose:
 * Version: V1.0
 * Author: E Denton
 * Version
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/

#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include "osal.h"
#include "resmgmt.h"
#include "hw_cpm.h"
#include "hw_gpio.h"
#include "hw_watchdog.h"
#include "sys_clock_mgr.h"
#include "sys_power_mgr.h"

// Includes required for the ADC
#include "hw_pdc.h"
#include "hw_wkup.h"
#include "hw_sys.h"

#include "ADCAdapter.h"

#include <ad_gpadc.h>
#include <ad_sdadc.h>

// Type structure required for the General purpose ADC
typedef const void* gp_adc_device;
gp_adc_device GP_ADC_DEVICE;

ad_gpadc_io_conf_t GP_ADC_BUS = {
        .input0 = {
                .port = ADC_GPIO_PORT_MAX,
                .pin  = ADC_GPIO_PIN_MAX,
                .on   = {ADC_GPIO_MODE_INVALID, ADC_GPIO_FUNCTION_LAST,  INPUT_FALSE},
                .off  = {ADC_GPIO_MODE_INVALID, ADC_GPIO_FUNCTION_LAST, INPUT_FALSE}
        },
        .input1 = {
                .port = ADC_GPIO_PORT_MAX,
                .pin  = ADC_GPIO_PIN_MAX,
                .on   = {ADC_GPIO_MODE_INVALID, ADC_GPIO_FUNCTION_LAST, INPUT_FALSE},
                .off  = {ADC_GPIO_MODE_INVALID, ADC_GPIO_FUNCTION_LAST, INPUT_FALSE}
        },
        .voltage_level = ADC_GPIO_POWER_NONE
};

ad_gpadc_driver_conf_t GP_ADC_DRV = {
        .clock                  = GP_ADC_CLOCK_INTERNAL,
        .input_mode             = GP_ADC_INPUT_MODE_SINGLE_ENDED,
        .input                  = GP_ADC_INPUT_SE_P0_25,
        .temp_sensor            = GP_ADC_NO_TEMP_SENSOR,
        .sample_time            = GP_ADC_SAMPLE_1,
        .continous              = ADC_CONTINUOUS_FALSE,
        .chopping               = GP_ADC_CHOPPING_TRUE,
        .oversampling           = GP_ADC_OVERSAMPLING_1_SAMPLE,
        .input_attenuator       = GP_ADC_INPUT_VOLTAGE_UP_TO_1V2
};

ad_gpadc_controller_conf_t GP_ADC_DEV = {
        .id  = HW_GPADC_1,
        .io  = GP_ADC_BUS,
        .drv = GP_ADC_DRV
};

gp_adc_device GP_ADC_DEVICE = &GP_ADC_DEV;

// Type structure required for the Sigma Delta ADC
typedef const void* sd_adc_device;
sd_adc_device SD_ADC_DEVICE;

ad_sdadc_io_conf_t SD_ADC_BUS = {
        .input0 = {
                .port = ADC_GPIO_PORT_MAX,
                .pin  = ADC_GPIO_PIN_MAX,
                .on   = {ADC_GPIO_MODE_INVALID, ADC_GPIO_FUNCTION_LAST,  INPUT_FALSE},
                .off  = {ADC_GPIO_MODE_INVALID, ADC_GPIO_FUNCTION_LAST, INPUT_FALSE}
        },
        .input1 = {
                .port = ADC_GPIO_PORT_MAX,
                .pin  = ADC_GPIO_PIN_MAX,
                .on   = {ADC_GPIO_MODE_INVALID, ADC_GPIO_FUNCTION_LAST, INPUT_FALSE},
                .off  = {ADC_GPIO_MODE_INVALID, ADC_GPIO_FUNCTION_LAST, INPUT_FALSE}
        },
        .voltage_level = ADC_GPIO_POWER_NONE
};

ad_sdadc_driver_conf_t SD_ADC_DRV = {
        .clock                  = SD_ADC_CLOCK_0,
        .input_mode             = SD_ADC_INPUT_MODE_SINGLE_ENDED,
        .inn                    = HW_SDADC_IN_ADC1_P0_25,
        .inp                    = SD_ADC_IN_ADC1_P0_25,
        .continuous             = ADC_CONTINUOUS_FALSE,
        .over_sampling          = SD_ADC_OSR_128,
        .vref_selection         = SD_ADC_VREF_INTERNAL,
        .vref_voltage           = SD_ADC_VREF_VOLTAGE_INTERNAL,
        .use_dma                = SD_ADC_DMA_FALSE,
        .mask_int               = SD_ADC_MASK_FALSE,
        .freq                   = SD_ADC_CLOCK_FREQ_1M
};

ad_sdadc_controller_conf_t SD_ADC_DEV = {
        .id  = HW_SDADC,
        .io  = &SD_ADC_BUS,
        .drv = &SD_ADC_DRV,
};

sdadc_device SDADC_DEVICE = &SD_ADC_DEV;



/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/
