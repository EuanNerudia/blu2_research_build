
#define Address_Pre 0b1011101

#define Interrupt_CFG    0x0B

#define Threshold_Low    0x0C
#define Threshold_Hig    0x0D

#define Control_Reg_1    0x10
#define Control_Reg_2    0x11
#define Control_Reg_3    0x12

#define Press_Reg_L      0x28
#define Press_Reg_M      0x29
#define Press_Reg_H      0x2A

#define Interrupt_CMD 0b11001010

#define Control3_CMD 0b00000010

#define THS_LSB_Val  0b00000011
#define THS_MSB_val  0b00000000

#define SWRESET_CMD  0b00000100

#define Register_Rs  0b00000000

#define Reading_CMD  0b01010010

#define OneShot_CMD  0b00000001

#include <stdio.h>

#include "I2CAdapter.h"

#include "..\UART\UARTAdapterPrototypes.h"
#include "..\UART\UARTAdapterTypes.h"
#include "..\UART\UARTAdapterDefines.h"

N_BOOL CommandSent = false;

void Pressure_Setup(void)
{
        if (!ConfiguredCheck())
        {
                I2C_IO_Setup();
        }

        Change_Slave_address(Address_Pre);
}

/*
 * The below sets up the threshold interrupt to allow the
 * sensor to produce an interrupt on a low pressure event.
 */
void Interupt_setup(void)
{
        Send_Command(Interrupt_CFG, Interrupt_CMD); // Interrupt Setup
        Send_Command(Control_Reg_3, Control3_CMD); // Interrupt pin Set
        Send_Command(Threshold_Low, THS_LSB_Val);  // lower section of the threshold
        Send_Command(Threshold_Hig, THS_MSB_val);  // Higher section of the threshold

        CommandSent = false;
}

N_U32 NominalStore[10] = {0}; // Rolling average value store for the nominal pressure
N_U32 Collected[6] = {0};     // Rolling average value store for the current pressure

long Nominal = 0;             // Nominal rolling average total
long Sum = 0;                 // Current rollign average total

/*
 * The below is used to keep track of and return the value for
 * both the nominal and current rolling average pressure
 */
N_U32 Ring_Average(int Possition, int Length, N_U32 NewNumber)
{
        if (Length == (sizeof(NominalStore) / sizeof(int)))
        {
                Nominal = Nominal - NominalStore[Possition] + NewNumber;

                NominalStore[Possition] = NewNumber;

                if (NominalStore[9] != 0)
                {
                        return Nominal / Length;
                }
        }
        else if (Length == (sizeof(Collected) / sizeof(int)))
        {
                Sum = Sum - Collected[Possition] + NewNumber;

                Collected[Possition] = NewNumber;

                if (Collected[5] != 0)
                {
                        return Sum / Length;
                }
        }

        return NewNumber;
}

// Returns the current average value for the nominal pressure
long Get_Nominal()
{
        int Len = sizeof(NominalStore) / sizeof(int);

        return Nominal / Len;
}

// Returns the current average value for the current pressure
long Get_Current()
{
        int Len = sizeof(Collected) / sizeof(int);

        return Sum / Len;
}

/*
 * The below sets up the pressure sensor and tells it
 * to read a single result, this allows the reading to be
 * accurate for that single reading.
 * The value held in the pressure out registers are then
 * read and returned as a single 24bit (32) number
 */
N_U32 Read_Pressure(void)
{
        if (!CommandSent)
        {
                Send_Command(Control_Reg_2, SWRESET_CMD);
                Send_Command(Interrupt_CFG, Register_Rs);
                Send_Command(Control_Reg_1, Reading_CMD);


                CommandSent = true;
        }

//        Send_Command(Control_Reg_2, OneShot_CMD);

        N_U08 Top = Read_8Bit(Press_Reg_H);
        N_U08 Mid = Read_8Bit(Press_Reg_M);
        N_U08 Low = Read_8Bit(Press_Reg_L);

        N_U32 ReturnVal = (Top << 16) | (Mid << 8) | Low;

        return ReturnVal;
}

/*
 * The below is used to take a 32bit value in and convert
 * this to hex. The UART comms are set up to allow the
 * chip to communicate over the UART pins. The hex value
 * produced is then sent over UART so it can be recoreded.
 */
void UART_Print(N_U32 Print)
{
        ConfigureUART_GPIO(N_UART1, 0, 3, 0, 4, HW_GPIO_POWER_V33);
        SetUARTBaudRate(N_UART1, 4358);
        SetOffsetToLength(N_UART1, 0);
        UART_SetTimeout(N_UART1, 200);

        char DataSend[8];

        char HexVal[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

        DataSend[0] = HexVal[((Print & 0xF00000) >> 20)];
        DataSend[1] = HexVal[((Print & 0x0F0000) >> 16)];
        DataSend[2] = HexVal[((Print & 0x00F000) >> 12)];
        DataSend[3] = HexVal[((Print & 0x000F00) >> 8)];
        DataSend[4] = HexVal[((Print & 0x0000F0) >> 4)];
        DataSend[5] = HexVal[((Print & 0x00000F))];
        DataSend[6] = '\r';
        DataSend[7] = '\n';

        SendUARTData(N_UART1, 8, &DataSend);
}

// Keeps track of the current position in the nominal pressure rolling average array
int NominalPos = 0;

/*
 * Reads the nominal pressure and updates the rolling average
 * while printing the average returned for the nominal pressure
 */
void ReadNominal(void)
{
        int Length = sizeof(NominalStore) / sizeof(int);

        N_U32 NominalRead = Read_Pressure();

        N_U32 Sending = Ring_Average(NominalPos, Length, NominalRead);

        NominalPos++;

        if (NominalPos == Length)
        {
                NominalPos = 0;
        }

        if (Sending > 0)
        {
                UART_Print(Sending);
        }
}

// Keeps track of the current position in the current pressure rolling average array
int OneShotPos = 0;

/*
 * Reads the current pressure and both prints this value and
 * returns it.
 */
long OneShotRead(void)
{
        int Length = sizeof(Collected) / sizeof(int);

        N_U32 Reading = Read_Pressure();

        N_U32 Sending = Ring_Average(OneShotPos, Length, Reading);

        OneShotPos++;

        if (OneShotPos == Length)
        {
                OneShotPos = 0;
        }

        UART_Print(Sending);

        return Sending;
}

/*
 * Resets the current pressure array so that the
 * rolling average is accurate for the specific
 * pressure read.
 */
void Reset_Current()
{
        for(int i = 0; i < 6; i++)
        {
                Collected[i] = 0;
        }

        Sum = 0;
        OneShotPos = 0;
}

// Used to ensure that the rolling average for the current pressure has filled the array
N_U64 Count = 0;
// Used to keep track of the number of ms that have passed to allow for the right timings to be used
N_U64 Tick = 0;
// Used to keep track of the number of nominal readings that have been made
N_U64 nominal = 0;
// The current reading flag which ensure the pressure is read at the right time
N_BOOL ReadStart = false;

/*
 * The below is called from the tick main application tick hook which
 * means it is called every ms. This is used to keep track of the
 * correct timings for both reading the pressure and setting the
 * threshold interrupt and updating the nominal pressure.
 */
void Pressure_Ticks(void)
{
        if (ReadStart)
        {
                /*
                 * The below if runs every 14ms if the read
                 * flag is true, it is used to read the current
                 * pressure and keep track of if the user
                 * is currently inhaling and allowing for the
                 * end of an inhale to be flagged when needed
                 */
                if (Tick%15 == 0 && nominal > 20)
                {
                        OneShotRead();

                        if(Count > 8 && (Get_Nominal() - Get_Current()) < 400)
                        {
                                Reset_Current();

                                Count = 0;
                                Tick = 0;
                                nominal = 20;
                                ReadStart = false;
                        }

                        Count++;

                        Tick = 0;
                }
        }
        else
        {
                /*
                 * If the number of nominal reads is below 20
                 * then a new nominal reading is taken every 14ms
                 * until this number is equal to 20
                 */
                if(Tick%15 == 0 && nominal < 20)
                {
                        nominal++;

                        ReadNominal();

                        Tick = 0;
                }

                /*
                 * After an inhale or nominal reading update the
                 * interrupt threshold is reset to allow for the
                 * device to act as required. This waits 200ms to
                 * allow the pressure to return closer to the nominal
                 * pressure after an inhale.
                 */
                else if (Tick > 0 && Tick%200 == 0 && nominal == 20)
                {
                        Interupt_setup();
                        Tick = 0 ;
                        nominal = 21;
                }

                /*
                 * Every second, as long as an inhale is not occurring,
                 * the nominal pressure rolling average has three new
                 * values added to it. This is used to allow for normal
                 * pressure changes (e.g. tunnels or elevation change)
                 * to have minimal to no effect on the operation of the
                 * device.
                 */
                else if (Tick > 1000)
                {
                        nominal = 17;
                        Tick = 0;
                }
        }

        Tick++;
}

// Allows the wakeup handler to update the read status flag.
void Read_StartHandle(void)
{
        ReadStart = true;
}
