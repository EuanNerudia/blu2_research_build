/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: ADCAdapter.c
 * Purpose: This acts as the ADC driver for both the GP and SD ADCs.
 * Version: V1.0
 * Author: E Denton
 * Version
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/

#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "osal.h"
#include "hw_pdc.h"
#include "hw_sys.h"
#include "hw_cpm.h"
#include "hw_wkup.h"
#include "resmgmt.h"
#include "hw_gpio.h"
#include "hw_watchdog.h"
#include "sys_clock_mgr.h"
#include "sys_power_mgr.h"

#include "ADCAdapter.h"
#include "GPADCAdapter.h"
#include "SDADCAdapter.h"

void (*GP_Loacl_callback)(uint16_t Reading);

void (*SD_Loacl_callback)(uint16_t Reading);

GP_ADC_Config Local_GP_ADC_Config;

SD_ADC_Config Local_SD_ADC_Config;

static int GP_ADC_Pin;

bool GP_Scanning = false;

volatile static uint16_t GP_ADC_Readings[7];

void Store_Read(void)
{
        hw_gpio_set_inactive(HW_GPIO_PORT_0, HW_GPIO_PIN_1);
        for(int i=0; i<8; i++)
        {
                int Print = GP_ADC_Readings[i];

                printf("%d\n\r", Print);
        }

        printf("\n\r");
}

void ADCAdapter_GP_ADC_Callback_Scanning(void)
{
        uint16_t value = GPADC->GP_ADC_RESULT_REG;

        GP_ADC_Close(Local_GP_ADC_Config);

        GP_ADC_Readings[GP_ADC_Pin] = value;

        if (GP_ADC_Pin > 7)
        {
                GP_ADC_Pin = 0;
                Store_Read();
        }
        else
        {
                GP_ADC_Pin++;
        }


        //REG_SETF(GPADC, GP_ADC_CTRL_REG, GP_ADC_MINT, 0);



        //REG_SETF(GPADC, GP_ADC_CTRL_REG, GP_ADC_MINT, 1);

        ADCAdapter_GP_ADC_Scanning();
}

void ADCAdapter_GP_ADC_Scanning(void)
{
        hw_gpio_set_active(HW_GPIO_PORT_0, HW_GPIO_PIN_1);

        GP_IO_Con(); // Move to a differnt Local

        int pin = 0;

        int input = 0;

        if (GP_ADC_Pin == 0)
        {
                pin = GP_PIN_12;
                input = GP_ADC_INPUT_SE_P0_08;
        }
        else if (GP_ADC_Pin == 1)
        {
                pin = GP_PIN_13;
                input = GP_ADC_INPUT_SE_P0_09;
        }
        else if (GP_ADC_Pin == 2)
        {
                pin = GP_PIN_18;
                input = GP_ADC_INPUT_SE_P0_25;
        }
        else if (GP_ADC_Pin == 3)
        {
                pin = GP_PIN_19;
                input = GP_ADC_INPUT_SE_P1_09;
        }
        else if (GP_ADC_Pin == 4)
        {
                pin = GP_PIN_13;
                input = GP_ADC_INPUT_SE_P1_12;
        }
        else if (GP_ADC_Pin == 5)
        {
                pin = GP_PIN_18;
                input = GP_ADC_INPUT_SE_P1_13;
        }
        else if (GP_ADC_Pin == 6)
        {
                pin = GP_PIN_19;
                input = GP_ADC_INPUT_SE_P1_18;
        }
        else if (GP_ADC_Pin == 7)
        {
                pin = GP_PIN_19;
                input = GP_ADC_INPUT_SE_P1_19;
        }

//        ADC_io_conf GP_io_configP0 = {
//                .port = ADC_GPIO_PORT_1,
//                .pin  = ADC_GPIO_PIN_12,
//                .on   = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_ADC,  INPUT_TRUE},
//                .off  = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_GPIO, INPUT_TRUE}
//        };
//
//        ADC_io_conf GP_io_configP1 = {
//                .port = ADC_GPIO_PORT_MAX,
//                .pin  = ADC_GPIO_PIN_MAX,
//                .on   = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_GPIO,  INPUT_TRUE},
//                .off  = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_GPIO, INPUT_TRUE}
//        };
//
//        ADC_BUS GP_ADC_io_bus = {
//                .input0        = GP_io_configP0,
//                .input1        = GP_io_configP1,
//                .voltage_level = ADC_GPIO_POWER_VDD1V8P,
//        };

        GP_ADC_driver_config TempDriver = {
                .clock                  = GP_ADC_CLOCK_INTERNAL,
                .input                  = input,
                .chopping               = GP_ADC_CHOPPING_TRUE,
                .continous              = GP_ADC_CHOPPING_FALSE,
                .input_mode             = GP_ADC_INPUT_MODE_SINGLE_ENDED,
                .sample_time            = GP_ADC_SAMPLE_5,
                .temp_sensor            = GP_ADC_NO_TEMP_SENSOR,
                .oversampling           = GP_ADC_OVERSAMPLING_8_SAMPLES,
                .input_attenuator       = GP_ADC_INPUT_VOLTAGE_UP_TO_1V2,
        };

        GP_ADC_Config GP_DefaultTemp = {
                .configured = false,
                .error      = false,
                .driver     = TempDriver,
                //.io_bus     = GP_ADC_io_bus,
        };

        Local_GP_ADC_Config = GP_DefaultTemp;

        GP_Config(GP_DefaultTemp);

        GP_ADC_Start(ADCAdapter_GP_ADC_Callback_Scanning);
}

/*
 * Order to Run
 *
 * ADC_Hardware_setup
 *
 * ADC_Select_Read
 *
 * ADC_Select_Clear
 */

//-----------[O]----------//

/*
 * The below firstly sets up the IO pins that
 * either the GP or SD ADC will be able to use
 * this means that any of these pins can be used
 * only for the ADC.
 *
 * The second function is to configure the specific
 * ADC with the sent configuration data.
 */
void ADC_Hardware_setup(ADC_Selected_Config *ADC_Config)
{
        if (ADC_Config->ADC_to_use == GP_ADC)
        {
                Local_GP_ADC_Config = ADC_Config->GP_ADC_config;

                GP_IO_Con();
                GP_Config(ADC_Config->GP_ADC_config);
        }
        else if (ADC_Config->ADC_to_use == SD_ADC)
        {
                Local_SD_ADC_Config = ADC_Config->SD_ADC_config;

                SD_IO_Con();
                SD_Config(ADC_Config->SD_ADC_config);
        }
}

//-----------[O]----------//

/*
 * This callback is used specifically for the GP ADC
 * it reads the GPADC register and passes this
 * value to the callback set for the GPADC.
 */
void GP_ADC_Read_New(void)
{
        uint32_t value = GPADC->GP_ADC_RESULT_REG;

        GP_ADC_Close(Local_GP_ADC_Config);

        (*GP_Loacl_callback)(value);
}

//-----------[O]----------//

/*
 * This callback is the same as the above
 * but acts for the SDADC rather than the
 * GPADC.
 */
void SD_ADC_Read_New(void)
{
        uint32_t value = SDADC->SDADC_RESULT_REG;

//        double Test = value * 0.0183;//(value * 3300) / 65535;
//
//        value = (int)(Test * 10.0);

        SD_ADC_Close(Local_SD_ADC_Config);

        (*SD_Loacl_callback)(value);
}

//-----------[O]----------//

/*
 * The below is used to set up the callback
 * and select the ADC to read, this means that only
 * the desired ADC will be read.
 */
void ADC_Select_Read(void (*Call)(uint16_t reading), ADC_Selected_Config *ADC_Config)
{
        hw_gpio_set_active(HW_GPIO_PORT_0, HW_GPIO_PIN_1);

        if (ADC_Config->ADC_to_use == GP_ADC)
        {
                GP_Loacl_callback = Call;
                GP_ADC_Start(GP_ADC_Read_New);
        }
        else if (ADC_Config->ADC_to_use  == SD_ADC)
        {
                SD_Loacl_callback = Call;
                SD_ADC_Start(SD_ADC_Read_New);
        }
}

//-----------[O]----------//

/*
 * The below is used to stop the ADC and clear / reset
 * everything associated with it so that it won't
 * be able to cause issue with any other
 * part of the system.
 */
void ADC_Select_Clear(ADC_Selected_Config *ADC_Config)
{
        if (ADC_Config->ADC_to_use == GP_ADC)
        {
                GP_ADC_Stop(ADC_Config->GP_ADC_config);
        }
        else if (ADC_Config->ADC_to_use == SD_ADC)
        {
                SD_ADC_Stop(ADC_Config->SD_ADC_config);
        }
}

//-----------[O]----------//

/**
 * E Denton
 *
 * The below is an example of how the ADC config
 * should be built, this allows for the above methods to
 * use either the GPADC or the SDADC.
 */
// Default build for the ADC config, use for testing.
//ADC_Selected_Config ADC_Config(void)
//{
//        ADC_io_conf io_configP0 = {
//                .port = ADC_GPIO_PORT_0,
//                .pin  = ADC_GPIO_PIN_25,
//                .on   = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_ADC,  INPUT_TRUE},
//                .off  = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_GPIO, INPUT_TRUE}
//        };
//
//        ADC_io_conf io_configP1 = {
//                .port = ADC_GPIO_PORT_MAX,
//                .pin  = ADC_GPIO_PIN_MAX,
//                .on   = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_GPIO,  INPUT_TRUE},
//                .off  = {ADC_GPIO_MODE_INPUT, ADC_GPIO_FUNCTION_GPIO, INPUT_TRUE}
//        };
//
//        ADC_BUS ADC_io_bus = {
//                .input0        = io_configP0,
//                .input1        = io_configP1,
//                .voltage_level = ADC_GPIO_POWER_VDD1V8P,
//        };
//
//        GP_ADC_driver_config GP_Driver = {
//                .clock                  = GP_ADC_CLOCK_INTERNAL,
//                .input                  = GP_ADC_INPUT_SE_P0_25,
//                .chopping               = GP_ADC_CHOPPING_TRUE,
//                .continous              = ADC_CONTINUOUS_FALSE,
//                .input_mode             = GP_ADC_INPUT_MODE_SINGLE_ENDED,
//                .sample_time            = GP_ADC_SAMPLE_5,
//                .temp_sensor            = GP_ADC_NO_TEMP_SENSOR,
//                .oversampling           = GP_ADC_OVERSAMPLING_8_SAMPLES,
//                .input_attenuator       = GP_ADC_INPUT_VOLTAGE_UP_TO_1V2,
//        };
//
//        GP_ADC_Config GP_Config = {
//                .error      = 0,
//                .io_bus     = ADC_io_bus,
//                .driver     = GP_Driver,
//                .configured = false,
//        };
//
//        SD_ADC_driver_config SD_Driver = {
//                .inn                    = SD_ADC_IN_ADC1_P0_25,
//                .inp                    = SD_ADC_IN_ADC1_P0_25,
//                .clock                  = SD_ADC_CLOCK_0,
//                .freq                   = SD_ADC_CLOCK_FREQ_1M,
//                .use_dma                = SD_ADC_DMA_FALSE,
//                .mask_int               = SD_ADC_MASK_FALSE,
//                .input_mode             = SD_ADC_INPUT_MODE_SINGLE_ENDED,
//                .continuous             = ADC_CONTINUOUS_FALSE,
//                .vref_voltage           = SD_ADC_VREF_VOLTAGE_INTERNAL,
//                .over_sampling          = SD_ADC_OSR_128,
//                .vref_selection         = SD_ADC_VREF_INTERNAL,
//        };
//
//        SD_ADC_Config SD_Config = {
//                .error      = 0,
//                .io_bus     = ADC_io_bus,
//                .driver     = SD_Driver,
//                .configured = false,
//        };
//
//        ADC_Selected_Config ADC_Master = {
//                .ADC_to_use    = GP_ADC,
//                .GP_ADC_config = GP_Config,
//                .SD_ADC_config = SD_Config,
//        };
//
//        return ADC_Master;
//}

/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/
