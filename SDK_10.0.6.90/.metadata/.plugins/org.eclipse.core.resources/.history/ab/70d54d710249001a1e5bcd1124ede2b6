#define Address_ACC 0b1101000

#define CommandReg 0x7E

#define AccNormalPower  0x11
#define AccLowPower     0x12

#define GyroNormal      0x15
#define GyroFastStartUp 0x17

#define X_AccL 0x12
#define X_AccH 0x13

#define Y_AccL 0x14
#define Y_AccH 0x15

#define Z_AccL 0x16
#define Z_AccH 0x17

#define X_GyroL 0x0C
#define X_GyroH 0x0D

#define Y_GyroL 0x0E
#define Y_GyroH 0x0f

#define Z_GyroL 0x10
#define Z_GyroH 0x11

// Int status
#define Int_St_1 0x1C
#define Int_St_2 0x1D
#define Int_St_3 0x1E
#define Int_St_4 0x1F

#define Int_En_1 0x50 //0b00110000 Single tap + Double tap
#define Int_En_2 0x51 //0b00000111 High g for X, Y, Z
#define Int_En_3 0x52 //0b00000111 No Motion X, Y, Z

#define Int_OutC 0x53 //0b10101010 Int 1 & 2 active high push pull

#define Int_LaCh 0x54 //0b00000101 Temp latch of 5ms

#define Int_Map1 0x55 //0b00100110 Int1 Single Tap + Any Motion + High G
#define Int_Map2 0x56 //0b00000000 Int1 + Int2 Data ready, FIFO, PMU
#define Int_Map3 0x57 //0b00011000 Int2 Double Tap + No Motion

#define Int_Data1 0x58 //0b00000000 Allows for pre filtered data to be used but only in normal pwr mode
#define Int_Data2 0x59 //0b00000000 Allows for pre filtered data to be used but only in normal pwr mode

#define Int_D1_LH 0x5A //0b

#define Int_Dx_LH 0x5E

// Motion registers 0x5F - 0x62 controls the number of samples evalueated
#define Int_Mot_1 0x5F //0b00001100 Time frame of 1.28 seconds for no motion and 3 data points for any motion
#define Int_Mot_2 0x61 //0b00010100 Default Any motion change of 1.95, changes depending on the range selected
#define Int_Mot_3 0x62 //0b00010100 Default for the slow/ no motion g change, 1.95mg depending on range

#define Int_Tap_1 0x63 //0b11000001 Sets Tap quiet duration as 20ms, bit 7, Tap shock duration of 75ms, bit 6, Duration for double tap detection of 100ms bit 2 - 0
#define Int_Tap_2 0x64 //0b00000000 Sets the Tap threshold to default of 31.25mg

#define Int_Ori_1 0x65 //
#define Int_Ori_2 0x66 //

#define Int_Flat1 0x67 //0b00111111 Sets the angle for the flat interrupt to 0 degrees
#define Int_Flat2 0x68 //0b00110111 Sets the hold time, bits 5:4, defines the flat interrupt hysteresis bits 2:0

#define Int_Face 0x6B //0b00000000 Sets the interface as auto config with secondary off and SPI set at 4 wire

#define PMU_Trig 0x6C //

#define Tap_1 0b00100000
#define Tap_2 0b00010000


#include "I2CAdapter.h"

#include "..\UART\UARTAdapterPrototypes.h"
#include "..\UART\UARTAdapterTypes.h"
#include "..\UART\UARTAdapterDefines.h"

void Accelerometer_Setup(void)
{
        if (!ConfiguredCheck())
        {
                I2C_IO_Setup();
        }

        Change_Slave_address(Address_ACC);
}

N_BOOL UART_Set = false;

void Full_UART_Print(N_S16 X, N_S16 Y, N_S16 Z, N_BOOL end)
{
        ConfigureUART_GPIO(N_UART1, 0, 3, 0, 4, HW_GPIO_POWER_V33);
        SetUARTBaudRate(N_UART1, 4358);
        SetOffsetToLength(N_UART1, 0);
        UART_SetTimeout(N_UART1, 200);

        char DataSend[16];

        char HexVal[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

        DataSend[0] = HexVal[((X & 0xF000) >> 12)];
        DataSend[1] = HexVal[((X & 0x0F00) >> 8)];
        DataSend[2] = HexVal[((X & 0x00F0) >> 4)];
        DataSend[3] = HexVal[((X & 0x000F))];
        DataSend[4] = ',';
        DataSend[5] = HexVal[((Y & 0xF000) >> 12)];
        DataSend[6] = HexVal[((Y & 0x0F00) >> 8)];
        DataSend[7] = HexVal[((Y & 0x00F0) >> 4)];
        DataSend[8] = HexVal[((Y & 0x000F))];
        DataSend[9] = ',';
        DataSend[10] = HexVal[((Z & 0xF000) >> 12)];
        DataSend[11] = HexVal[((Z & 0x0F00) >> 8)];
        DataSend[12] = HexVal[((Z & 0x00F0) >> 4)];
        DataSend[13] = HexVal[((Z & 0x000F))];

        if (end)
        {
                DataSend[14] = '\r';
                DataSend[15] = '\n';

                SendUARTData(N_UART1, 16, &DataSend);
        }
        else
        {
                DataSend[14] = ',';

                SendUARTData(N_UART1, 15, &DataSend);
        }
}

void Print_Uart(N_U08 PrintState)
{
        if (!UART_Set)
        {
                ConfigureUART_GPIO(N_UART1, 0, 3, 0, 4, HW_GPIO_POWER_V33);
                SetUARTBaudRate(N_UART1, 4358);
                SetOffsetToLength(N_UART1, 0);
                UART_SetTimeout(N_UART1, 200);

                UART_Set = true;
        }

        char DataSend[7];

        if (PrintState == 0) //Still
        {
                DataSend[0] = 'S';
                DataSend[1] = 't';
                DataSend[2] = 'i';
                DataSend[3] = 'l';
        }

        if (PrintState == 1) //Still
        {
                DataSend[0] = 'P';
                DataSend[1] = 'i';
                DataSend[2] = 'c';
                DataSend[3] = 'k';
        }

        if (PrintState == 2) //Still
        {
                DataSend[0] = 'B';
                DataSend[1] = 'a';
                DataSend[2] = 't';
                DataSend[3] = 't';
        }

        if (PrintState == 3)
        {
                DataSend[0] = 'B';
                DataSend[1] = 'u';
                DataSend[2] = 'r';
                DataSend[3] = 'n';
        }

        if (PrintState == 4)
        {
                DataSend[0] = 'W';
                DataSend[1] = 'i';
                DataSend[2] = 'g';
                DataSend[3] = 'g';
        }

        DataSend[4] = '\r';
        DataSend[5] = '\n';

        SendUARTData(N_UART1, 6, &DataSend);
}

void Check_Inter(N_U32 Status)
{
        if (!UART_Set)
        {
                ConfigureUART_GPIO(N_UART1, 0, 3, 0, 4, HW_GPIO_POWER_V33);
                SetUARTBaudRate(N_UART1, 4358);
                SetOffsetToLength(N_UART1, 0);
                UART_SetTimeout(N_UART1, 200);

                UART_Set = true;
        }

        char DataSend[10];

        char HexVal[16] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

        DataSend[0] = HexVal[((Status & 0xF0000000) >> 28)];
        DataSend[1] = HexVal[((Status & 0x0F000000) >> 24)];
        DataSend[2] = HexVal[((Status & 0x00F00000) >> 20)];
        DataSend[3] = HexVal[((Status & 0x000F0000) >> 16)];
        DataSend[4] = HexVal[((Status & 0x0000F000) >> 12)];
        DataSend[5] = HexVal[((Status & 0x00000F00) >> 8)];
        DataSend[6] = HexVal[((Status & 0x000000F0) >> 4)];
        DataSend[7] = HexVal[((Status & 0x0000000F))];
        DataSend[8] = '\r';
        DataSend[9] = '\n';

        SendUARTData(N_UART1, 10, &DataSend);

}

// Sets up all interupt prams as well as starting the device.

// Set up pins and latching, set power mode#
// Set duration and thres?
void Interupt_SetUp(void)
{
        Send_Command(0x7E, 0b00010000);

        Send_Command(0x53, 0b10101010); // Allow both interupt pins
        Send_Command(0x54, 0b00001011); // Set int latch for 10ms (Could be lower)

        Send_Command(0x55, 0b00001010); // Interputs connected to pin 1
        Send_Command(0x56, 0b00000000);
        Send_Command(0x57, 0b00110000); // interputs connected to pin 2

        Send_Command(0x5C, 0b00000000); // High G Inter hysteresis
        Send_Command(0x5D, 0b00010000); // High G Duration
        Send_Command(0x5E, 0b00100000); // High G threshold

        Send_Command(0x5F, 0b00000001); // int motion 0  int slow / no duration
        Send_Command(0x60, 0b00010000); // int motion 1 any thresh
        Send_Command(0x61, 0b00000000); // int motion 2 slow / no thresh
        Send_Command(0x62, 0b00000100); // int motion 3 sig motion skip/ proof and no / sig select

        Send_Command(0x63, 0b01000111);
        Send_Command(0x64, 0b00001000);

        // Page 74

//        Send_Command(0x65, 0b11000001); // Int orient
//        Send_Command(0x66, 0b00000100);

//        Send_Command(0x67, 0b00111111); // Int flat
//        Send_Command(0x68, 0b00110000);


        Send_Command(0x50, 0b00000000);
        Send_Command(0x51, 0b00000001);
        Send_Command(0x52, 0b00000000);

        Send_Command(0x7E, 0b00010010);
}

N_U08 State = 0;

N_U08 TapState = 2;

void Held_Set(void)
{
        Send_Command(0x51, 0b00000000);
        Send_Command(0x52, 0b00000001);
}

void Still_Set(void)
{
        Send_Command(0x51, 0b00000001);
        Send_Command(0x52, 0b00000000);
}

void Not_Tapped_Set(void)
{
        Send_Command(0x61, 0b00000000);
//        Send_Command(0x62, 0b00001011);

        if (State == 0)
        Send_Command(0x50, 0b00010000);

        else
        Send_Command(0x50, 0b00000001);
}

void Tapped_Set(void)
{
        Send_Command(0x61, 0b00100000);
//        Send_Command(0x62, 0b00001001);

        if (State == 0)
        Send_Command(0x50, 0b00100000);

        else
        Send_Command(0x50, 0b00000011);
}

N_U08 Int_Status1;
N_U08 Int_Status2;
N_U08 Int_Status3;
N_U08 Int_Status4;

int Inter = 0;

int FlagRead = 0;

void Interput_State_Flag(void)
{
//        Send_Command(0x7E, 0b00010000);

        if (Int_Status2 ==0)
        {
                if (State == 0) // Still
                {
                     State = 1; // held

                //                        Not_Tapped_Set();
                     Held_Set();

                     Print_Uart(State);
                }
                else if (State == 1)
                {
                     State = 0;

                //                        Not_Tapped_Set();
                     Still_Set();

                     Print_Uart(State);
                }
        }
        else
        {
                if (Int_Status2 == 4) // Still
                {
                        State = 1; // held

                        //                        Not_Tapped_Set();
                        Held_Set();

                        Print_Uart(State);
                }
                else if (Int_Status2 == 128)
                {
                        State = 0;

                        //                        Not_Tapped_Set();
                        Still_Set();

                        Print_Uart(State);
                }

                FlagRead++;

                Int_Status2 = 0;
        }

        if (FlagRead == 15)
        {
                FlagRead = 0;
        }

        Send_Command(0x7E, 0b00010001);

}

void Accel_State(N_U08 Pin)
{
//        Send_Command(0x7E, 0b00010000);

        if (Pin == HW_GPIO_PIN_14)
        {
//                if (TapState > 2)
//                {
//                        TapState -= 1;
//
//                        if (State == 0)
//                        {
//                                Not_Tapped_Set();
//                                Still_Set();
//                        }
//
//                        if (State == 1)
//                        {
//                                Not_Tapped_Set();
//                                Held_Set();
//                        }
//
//                        Print_Uart(TapState);
//
//                        TapState = 2;
//                }
                if (State == 0) // Still
                {
                        State = 1; // held

//                        Not_Tapped_Set();
                        Held_Set();

                        Print_Uart(State);
                }
                else if (State == 1)
                {
                        State = 0;

//                        Not_Tapped_Set();
                        Still_Set();

                        Print_Uart(State);
                }
        }

        if (Pin == HW_GPIO_PIN_15)
        {
                if (TapState == 2) // DoubleTap battery
                {
                        TapState = 3;

                        Tapped_Set();
                       // Held_Set();
                }
                else if (TapState == 3) // Triple tap
                {
//                        if (State == 0)
//                        {
//                                Not_Tapped_Set();
//                                Still_Set();
//                        }
//
//                        if (State == 1)
//                        {
//                                Not_Tapped_Set();
//                                Held_Set();
//                        }
//
//                        Print_Uart(TapState);

                        TapState = 4;

                        Tapped_Set();
                       // Held_Set();
                }
                else if (TapState == 4)
                {
                        if (State == 0)
                        {
                                Not_Tapped_Set();
                                Still_Set();
                        }

                        if (State == 1)
                        {
                                Not_Tapped_Set();
                                Held_Set();
                        }

//                        Print_Uart(TapState);

                        TapState = 5;
                }
        }

        Send_Command(0x7E, 0b00010001);
}

int Ticks = 0;


void Pin_Cause(N_U08 Pin)
{
        Inter = Pin;
}

N_BOOL Printed = false;

N_U32 Status = 0;

N_U32 StatusBuff[100];

N_U08 Status1Buff[100];
N_U08 Status2Buff[100];
N_U08 Status3Buff[100];
N_U08 Status4Buff[100];

int BuffCount = 0;

int NoRead = 0;

N_BOOL Accel_Ticks(void)
{
        if (Int_Status1 == 0)
        Int_Status1 = Read_8Bit(0x1C);

        if (Int_Status2 == 0)
        Int_Status2 = Read_8Bit(0x1D);

//        if (Int_Status2 != 0)
//        {
//                Int_Status2 = 0;
//        }

        if (Ticks > 320 && Inter != 0)
        {
                Interput_State_Flag();

//                Accel_State(Inter);

                Ticks = 0;
                Inter = 0;

                Status = 0;

                return false;
        }




//        if (Status == 0)
//        {
////                Int_Status1 = Read_8Bit(0x1C);
////                Int_Status2 = Read_8Bit(0x1D);
////                Int_Status3 = Read_8Bit(0x1E);
////                Int_Status4 = Read_8Bit(0x1F);
//
//                Status1Buff[BuffCount] = Read_8Bit(0x1C);
//                Status2Buff[BuffCount] = Read_8Bit(0x1D);
////                        Status3Buff[BuffCount] = Read_8Bit(0x1E);
////                        Status4Buff[BuffCount] = Read_8Bit(0x1F);
//
//                if (Status1Buff[BuffCount] != 0 || Status2Buff[BuffCount] != 0 || Status3Buff[BuffCount] != 0 || Status4Buff[BuffCount] != 0)
//                {
//                        BuffCount++;
//                }
////                else
////                {
////                        NoRead++;
////                }
//
//                Status = 1;
////
////                if (BuffCount == 100)
////                {
////                        BuffCount = 0;
////                }
//        }

//        if (NoRead == 250)
//        {
//                NoRead = 0;
//        }


//                if (Ticks > 320)
//                {
//                        Ticks = 0;
//                        return false;
//                }


//                if (Status != 0)
//                {
//                        StatusBuff[BuffCount] = (Status); //  & 0b00000000000000001000010000000110);
//
//                        BuffCount++;
//
////                        StatusBuff[BuffCount] = Ticks;
////
////                        BuffCount++;
//
//                        if (BuffCount == 100)
//                        {
//                                BuffCount = 0;
//                        }
//
//                        Status = 0;
//                }

//        }

        Ticks++;

        return true;
}

void Accel_Config(void)
{
        // Check Config
        Read_8Bit(0x40); // Check it is set to the reset val (0b00101000)

        Send_Command(0x40, 0b00101000); // 100Hz setup, normal pwr mode

        Send_Command(0x41, 0b00000011); // Set g-range as + or - 2g
}

void Gyro_Config(void)
{
        Read_8Bit(0x42);

        Send_Command(0x42, 0b00101000); // sets the odr to 100Hz

        Send_Command(0x43, 0b00000010); // Angular rate range and resolution of + or - 500 degrees a second
}

void Set_Pwr_1(void)
{
        Send_Command(0x7E, 0x11);
}

void Set_Pwr_2(void)
{
        Send_Command(0x7E, 0x15);
}

int AccelPos = 0;

N_S16 Accel_X_Avge[5] = {0};
N_S16 Accel_Y_Avge[5] = {0};
N_S16 Accel_Z_Avge[5] = {0};

N_S32 Accel_X_Sum = 0;
N_S32 Accel_Y_Sum = 0;
N_S32 Accel_Z_Sum = 0;

void Accel_Rolling(N_S16 X, N_S16 Y, N_S16 Z)
{
        Accel_X_Sum = Accel_X_Sum - Accel_X_Avge[AccelPos] + X;

        Accel_X_Avge[AccelPos] = X;

        Accel_Y_Sum = Accel_Y_Sum - Accel_Y_Avge[AccelPos] + Y;

        Accel_Y_Avge[AccelPos] = Y;

        Accel_Z_Sum = Accel_Z_Sum - Accel_Z_Avge[AccelPos] + Z;

        Accel_Z_Avge[AccelPos] = Z;

        AccelPos++;

        if (AccelPos == 5)
        {
                AccelPos = 0;
        }
}

void Read_Accel(void)
{
        volatile N_S16 X_Accel = Read_16Bit(0x12, 0x13);
        volatile N_S16 Y_Accel = Read_16Bit(0x14, 0x15);
        volatile N_S16 Z_Accel = Read_16Bit(0x16, 0x17);

        Accel_Rolling(X_Accel, Y_Accel, Z_Accel);

}

void PrintRolling(void)
{
//        if (Accel_X_Avge[4] != 0)
//        {
                Full_UART_Print((Accel_X_Sum / 5), (Accel_Y_Sum / 5), (Accel_Z_Sum / 5), true);
//        }
}

int GyroPos = 0;

N_U16 Gyro_X_Avge[5] = {0};
N_U16 Gyro_Y_Avge[5] = {0};
N_U16 Gyro_Z_Avge[5] = {0};

N_U32 Gyro_X_Sum = 0;
N_U32 Gyro_Y_Sum = 0;
N_U32 Gyro_Z_Sum = 0;

void Gyro_Rolling(N_U16 X, N_U16 Y, N_U16 Z)
{
        Gyro_X_Sum = Gyro_X_Sum - Gyro_X_Avge[GyroPos] + X;

        Gyro_X_Avge[GyroPos] = X;

        Gyro_Y_Sum = Gyro_Y_Sum - Gyro_Y_Avge[GyroPos] + Y;

        Gyro_Y_Avge[GyroPos] = Y;

        Gyro_Z_Sum = Gyro_Z_Sum - Gyro_Z_Avge[GyroPos] + Z;

        Gyro_Z_Avge[GyroPos] = Z;

        GyroPos++;

        if (GyroPos == 5)
        {
                GyroPos = 0;
        }
}

void Read_Gyro(void)
{
        volatile N_U16 X_Gyro = Read_16Bit(0x18, 0x19);
        volatile N_U16 Y_Gyro = Read_16Bit(0x20, 0x21);
        volatile N_U16 Z_Gyro = Read_16Bit(0x22, 0x23);

        Gyro_Rolling(X_Gyro, Y_Gyro, Z_Gyro);

//        Full_UART_Print(X_Gyro, Y_Gyro, Z_Gyro, true);
}

void PrintRolling_Gyro(void)
{
        if (Gyro_X_Avge[4] != 0)
        {
                Full_UART_Print((Gyro_X_Sum / 5), (Gyro_Y_Sum / 5), (Gyro_Z_Sum / 5), true);
        }
        else
        {
                Full_UART_Print(Gyro_X_Avge[0], Gyro_Y_Avge[0], Gyro_Z_Avge[0], true);
        }
}

int Ticks_1 = 0;

N_BOOL Set = false;

void Accel_Gryo_Pwr_Up_Read(void)
{
        if (!Set && Ticks_1 == 1)
        {
                Gyro_Config();
                Accel_Config();

                Set_Pwr_1();
        }

        if (!Set && Ticks_1 == 5)
        {
                Set_Pwr_2();


                Set = true;

                Ticks_1 = 0;
        }

        if (Ticks_1 == 100)
        {
                Read_Accel();
        }

        if (Ticks_1 == 101)
        {
//                Read_Gyro();
        }

        if (Ticks_1 == 102)
        {
                PrintRolling();
                Ticks_1 = 93;
        }

        if (Ticks_1 == 104)
        {
                //PrintRolling_Gyro();
                Ticks_1 = 95;
        }

        Ticks_1++;
}

/*
 * Sensor Config
 *
 * Set Accel Conf
 *
 * check result
 *
 * Set Gyro Conf
 *
 * Check result
 *
 * set Power mode
 *
 * Check result
 *
 * check invalid setting
 */

/*
 * Set Accel conf
 *
 * Read_8Bit(0x40) // Check current config
 *
 * Send_Command(0x40, 0x00)
 */


/*
 * Shows the current power mode
 * PMU_Status 0x03
 *
 * Data registers for Mag, RHALL, Gyr X Y Z and ACC X Y Z
 * Gyr and Acc split between two registers
 * 0x04 - 0x17
 *
 * Sensor time register, 24bit counter
 * 0x18 - 0x1A
 *
 * Status register displays data rediness
 * 0x1B
 *
 * Interrupt status register, holds the interrupt status flags
 * 0x1C - 0x1F
 *
 * Interrupt set up registers
 * 0x50 - 0x52
 *
 * Tap interupt Config register, tap duration of either
 * 0x63 - 0x64
 */
