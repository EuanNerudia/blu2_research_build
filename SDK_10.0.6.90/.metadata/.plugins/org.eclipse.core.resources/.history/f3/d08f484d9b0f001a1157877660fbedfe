/*
 * I2CAdapter.c
 *
 *  Created on: 21 Nov 2019
 *      Author: Euan Denton
 */

#include "hw_pdc.h"
#include "hw_i2c.h"
#include "ad_i2c.h"
#include "hw_wkup.h"
#include "hw_sys.h"

// SCL Port == 0
// SCL Pin == 30

// SDA Port == 0
// SDA Pin == 31

// ACl Addr = 0x6B

const ad_i2c_io_conf_t i2c_Io_Con = {
        .scl = {
                .port = HW_GPIO_PORT_0, .pin = HW_GPIO_PIN_30,
                .on =  { HW_GPIO_MODE_OUTPUT_OPEN_DRAIN, HW_GPIO_FUNC_I2C_SCL, false },
                .off = { HW_GPIO_MODE_INPUT,             HW_GPIO_FUNC_GPIO,    true  }
        },
        .sda = {
                .port = HW_GPIO_PORT_0, .pin =HW_GPIO_PIN_31,
                .on =  { HW_GPIO_MODE_OUTPUT_OPEN_DRAIN, HW_GPIO_FUNC_I2C_SDA, false },
                .off = { HW_GPIO_MODE_INPUT,             HW_GPIO_FUNC_GPIO,    true  }
        },
        .voltage_level = HW_GPIO_POWER_VDD1V8P
};

const ad_i2c_driver_conf_t drv_EEPROM_24FC256 = {
        I2C_DEFAULT_CLK_CFG,
        .i2c.speed              = HW_I2C_SPEED_STANDARD,
        .i2c.mode               = HW_I2C_MODE_MASTER,
        .i2c.addr_mode          = HW_I2C_ADDRESSING_7B,
        .i2c.address            = 0x6B,
        .dma_channel            = HW_DMA_CHANNEL_2
};

/* EEPROM 24FC256 I2C controller configuration */
const ad_i2c_controller_conf_t dev_24FC256 = {
        .id     = HW_I2C1,
        .io     = &i2c_Io_Con,
        .drv    = &drv_EEPROM_24FC256
};

void I2C_IO_Setup(ad_i2c_io_conf_t I2C_IO)
{
        hw_sys_pd_com_enable();

        if (state == AD_IO_CONF_ON) {
                ad_i2c_configure_pins(id, io_config);
        } else {
                ad_i2c_deconfigure_pin(&(io_config->scl), io_config->voltage_level);
                ad_i2c_deconfigure_pin(&(io_config->sda), io_config->voltage_level);
        }
        hw_gpio_pad_latch_enable(io_config->scl.port, io_config->scl.pin);
        hw_gpio_pad_latch_disable(io_config->scl.port, io_config->scl.pin);
        hw_gpio_pad_latch_enable(io_config->sda.port, io_config->sda.pin);
        hw_gpio_pad_latch_disable(io_config->sda.port, io_config->sda.pin);

        hw_sys_pd_com_disable();
}

