/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: .c
 * Purpose:
 * Version: V1.0
 * Author: E Denton
 * Version
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/

#include "hw_gpadc.h"
#include <ad_gpadc.h>

typedef enum {
        GP_ADC_CLOCK_INTERNAL = HW_GPADC_CLOCK_INTERNAL,       /**< internal high-speed clock (default) */
        GP_ADC_CLOCK_DIGITAL  = HW_GPADC_CLOCK_DIGITAL,         /**< digital clock (16/96MHz) */
} GP_ADC_CLOCK;

typedef enum {
        GP_ADC_INPUT_MODE_DIFFERENTIAL = HW_GPADC_INPUT_MODE_DIFFERENTIAL,    /**< differential mode (default) */
        GP_ADC_INPUT_MODE_SINGLE_ENDED = HW_GPADC_INPUT_MODE_SINGLE_ENDED,     /**< single ended mode */
} GP_ADC_INPUT_MODE;

typedef enum {
        GP_ADC_INPUT_SE_P1_09         = HW_GPADC_INPUT_SE_P1_09,            /**< GPIO P1_09 */
        GP_ADC_INPUT_SE_P0_25         = HW_GPADC_INPUT_SE_P0_25,            /**< GPIO P0_25 */
        GP_ADC_INPUT_SE_P0_08         = HW_GPADC_INPUT_SE_P0_08,            /**< GPIO P0_08 */
        GP_ADC_INPUT_SE_P0_09         = HW_GPADC_INPUT_SE_P0_09,            /**< GPIO P0_09 */
        GP_ADC_INPUT_SE_VDD           = HW_GPADC_INPUT_SE_VDD,            /**< VDD supply of the ADC circuit */
        GP_ADC_INPUT_SE_V30_1         = HW_GPADC_INPUT_SE_V30_1,            /**< V30 supply rail */
        GP_ADC_INPUT_SE_V30_2         = HW_GPADC_INPUT_SE_V30_2,            /**< V30 supply rail */
        GP_ADC_INPUT_SE_VBAT          = HW_GPADC_INPUT_SE_VBAT,            /**< Battery voltage, scaled from 5V to 1.2V */
        GP_ADC_INPUT_SE_VSSA          = HW_GPADC_INPUT_SE_VSSA,            /**< ADC ground */
        GP_ADC_INPUT_SE_P1_13         = HW_GPADC_INPUT_SE_P1_13,           /**< GPIO P1_13 */
        GP_ADC_INPUT_SE_P1_12         = HW_GPADC_INPUT_SE_P1_12,           /**< GPIO P1_12 */
        GP_ADC_INPUT_SE_P1_18         = HW_GPADC_INPUT_SE_P1_18,           /**< GPIO P1_18 */
        GP_ADC_INPUT_SE_P1_19         = HW_GPADC_INPUT_SE_P1_19,           /**< GPIO P1_19 */
        GP_ADC_INPUT_SE_TEMPSENS      = HW_GPADC_INPUT_SE_TEMPSENS,        /**< temperature sensor */
        GP_ADC_INPUT_DIFF_P1_09_P0_25 = HW_GPADC_INPUT_DIFF_P1_09_P0_25,    /**< GPIO P1_09 vs P0_25 */
        GP_ADC_INPUT_DIFF_P0_08_P0_09 = HW_GPADC_INPUT_DIFF_P0_08_P0_09,    /**< GPIO P0_08 vs P0_09 - all other values */
} GP_ADC_INPUT;

typedef enum {
        /* Temperature selection for GP_ADC_DIFF_TEMP_EN = 0 follows this line */
        GP_ADC_CHARGER_TEMPSENS_GND        = HW_GPADC_CHARGER_TEMPSENS_GND,    /**< Ground (no sensor) */
        GP_ADC_CHARGER_TEMPSENS_Z          = HW_GPADC_CHARGER_TEMPSENS_Z,    /**< Z from charger */
        GP_ADC_CHARGER_TEMPSENS_VNTC       = HW_GPADC_CHARGER_TEMPSENS_VNTC,    /**< V(ntc) from charger */
        GP_ADC_CHARGER_TEMPSENS_VTEMP      = HW_GPADC_CHARGER_TEMPSENS_VTEMP,    /**< V(temp) from charger */
        GP_ADC_NO_TEMP_SENSOR              = HW_GPADC_NO_TEMP_SENSOR,    /**< No on-chip temperature sensor selected (default) */
        GP_ADC_TEMP_SENSOR_NEAR_RADIO      = HW_GPADC_TEMP_SENSOR_NEAR_RADIO,    /**< Diode temperature sensor near radio */
        GP_ADC_TEMP_SENSOR_NEAR_CHARGER    = HW_GPADC_TEMP_SENSOR_NEAR_CHARGER,    /**< Diode temperature sensor near charger */
        GP_ADC_TEMP_SENSOR_NEAR_BANDGAP    = HW_GPADC_TEMP_SENSOR_NEAR_BANDGAP,    /**< Diode temperature sensor near bandgap */
        GP_ADC_TEMPSENSOR_MAX              = HW_GPADC_TEMPSENSOR_MAX,    /**< Invalid */
} GP_ADC_TEMP_SENSORS;

typedef enum {
        GP_ADC_SAMPLE_0 = 0,
        GP_ADC_SAMPLE_1 = 1,
        GP_ADC_SAMPLE_2 = 2,
        GP_ADC_SAMPLE_3 = 3,
        GP_ADC_SAMPLE_4 = 4,
        GP_ADC_SAMPLE_5 = 5,
} GP_ADC_SAMPLE_TIME;

typedef enum {
        GP_ADC_CHOPPING_TRUE  = true,
        GP_ADC_CHOPPING_FALSE = false,
} GP_ADC_CHOPPING;

typedef enum {
        GP_ADC_OVERSAMPLING_1_SAMPLE          = HW_GPADC_OVERSAMPLING_1_SAMPLE,    /**< 1 sample is taken or 2 in case chopping is enabled */
        GP_ADC_OVERSAMPLING_2_SAMPLES         = HW_GPADC_OVERSAMPLING_2_SAMPLES,    /**< 2 samples are taken */
        GP_ADC_OVERSAMPLING_4_SAMPLES         = HW_GPADC_OVERSAMPLING_4_SAMPLES,    /**< 4 samples are taken */
        GP_ADC_OVERSAMPLING_8_SAMPLES         = HW_GPADC_OVERSAMPLING_8_SAMPLES,    /**< 8 samples are taken */
        GP_ADC_OVERSAMPLING_16_SAMPLES        = HW_GPADC_OVERSAMPLING_16_SAMPLES,    /**< 16 samples are taken */
        GP_ADC_OVERSAMPLING_32_SAMPLES        = HW_GPADC_OVERSAMPLING_32_SAMPLES,    /**< 32 samples are taken */
        GP_ADC_OVERSAMPLING_64_SAMPLES        = HW_GPADC_OVERSAMPLING_64_SAMPLES,    /**< 64 samples are taken */
        GP_ADC_OVERSAMPLING_128_SAMPLES       = HW_GPADC_OVERSAMPLING_128_SAMPLES    /**< 128 samples are taken */
} GP_ADC_OVERSAMPLING;

typedef enum {
        GP_ADC_INPUT_VOLTAGE_UP_TO_1V2 = HW_GPADC_INPUT_VOLTAGE_UP_TO_1V2,    /**< input voltages up to 1.2 V are allowed */
        GP_ADC_INPUT_VOLTAGE_UP_TO_3V6 = HW_GPADC_INPUT_VOLTAGE_UP_TO_3V6     /**< input voltages up to 3.6 V are allowed */
} GP_ADC_INPUT_VOLTAGE;



/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/

