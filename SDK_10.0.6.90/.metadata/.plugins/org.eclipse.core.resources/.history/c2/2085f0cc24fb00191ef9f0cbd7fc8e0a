/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: .c
 * Purpose:
 * Version: V1.0
 * Author: E Denton
 * Version
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/

#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include "osal.h"
#include "resmgmt.h"
#include "hw_cpm.h"
#include "hw_gpio.h"
#include "hw_watchdog.h"
#include "sys_clock_mgr.h"
#include "sys_power_mgr.h"

// Includes required for the ADC
#include "hw_pdc.h"
#include "hw_wkup.h"
#include "hw_sys.h"

#include "ADCAdapter.h"

#include <ad_gpadc.h>
#include <ad_sdadc.h>

// Type structure required for the General purpose ADC
typedef const void* gp_adc_device;
gp_adc_device GP_ADC_DEVICE;

ad_gpadc_io_conf_t GP_ADC_BUS = {
        .input0 = {
                .port = ADC_GPIO_PORT_MAX,
                .pin  = ADC_GPIO_PIN_MAX,
                .on   = {ADC_GPIO_MODE_INVALID, ADC_GPIO_FUNCTION_LAST,  INPUT_FALSE},
                .off  = {ADC_GPIO_MODE_INVALID, ADC_GPIO_FUNCTION_LAST, INPUT_FALSE}
        },
        .input1 = {
                .port = ADC_GPIO_PORT_MAX,
                .pin  = ADC_GPIO_PIN_MAX,
                .on   = {ADC_GPIO_MODE_INVALID, ADC_GPIO_FUNCTION_LAST, INPUT_FALSE},
                .off  = {ADC_GPIO_MODE_INVALID, ADC_GPIO_FUNCTION_LAST, INPUT_FALSE}
        },
        .voltage_level = ADC_GPIO_POWER_NONE
};

ad_gpadc_driver_conf_t GP_ADC_DRV = {
        .clock                  = GP_ADC_CLOCK_INTERNAL,
        .input_mode             = GP_ADC_INPUT_MODE_SINGLE_ENDED,
        .input                  = GP_ADC_INPUT_SE_P0_25,
        .temp_sensor            = GP_ADC_NO_TEMP_SENSOR,
        .sample_time            = GP_ADC_SAMPLE_1,
        .continous              = ADC_CONTINUOUS_FALSE,
        .chopping               = GP_ADC_CHOPPING_TRUE,
        .oversampling           = GP_ADC_OVERSAMPLING_1_SAMPLE,
        .input_attenuator       = GP_ADC_INPUT_VOLTAGE_UP_TO_1V2
};

ad_gpadc_controller_conf_t GP_ADC_DEV = {
        .id  = HW_GPADC_1,
        .io  = &GP_ADC_BUS,
        .drv = &GP_ADC_DRV
};

gp_adc_device GP_ADC_DEVICE = &GP_ADC_DEV;

//-----------[O]----------//

// Type structure required for the Sigma Delta ADC
typedef const void* sd_adc_device;
sd_adc_device SD_ADC_DEVICE;

ad_sdadc_io_conf_t SD_ADC_BUS = {
        .input0 = {
                .port = ADC_GPIO_PORT_MAX,
                .pin  = ADC_GPIO_PIN_MAX,
                .on   = {ADC_GPIO_MODE_INVALID, ADC_GPIO_FUNCTION_LAST,  INPUT_FALSE},
                .off  = {ADC_GPIO_MODE_INVALID, ADC_GPIO_FUNCTION_LAST, INPUT_FALSE}
        },
        .input1 = {
                .port = ADC_GPIO_PORT_MAX,
                .pin  = ADC_GPIO_PIN_MAX,
                .on   = {ADC_GPIO_MODE_INVALID, ADC_GPIO_FUNCTION_LAST, INPUT_FALSE},
                .off  = {ADC_GPIO_MODE_INVALID, ADC_GPIO_FUNCTION_LAST, INPUT_FALSE}
        },
        .voltage_level = ADC_GPIO_POWER_NONE
};

ad_sdadc_driver_conf_t SD_ADC_DRV = {
        .clock                  = SD_ADC_CLOCK_0,
        .input_mode             = SD_ADC_INPUT_MODE_SINGLE_ENDED,
        .inn                    = SD_ADC_IN_ADC1_P0_25,
        .inp                    = SD_ADC_IN_ADC1_P0_25,
        .continuous             = ADC_CONTINUOUS_FALSE,
        .over_sampling          = SD_ADC_OSR_128,
        .vref_selection         = SD_ADC_VREF_INTERNAL,
        .vref_voltage           = SD_ADC_VREF_VOLTAGE_INTERNAL,
        .use_dma                = SD_ADC_DMA_FALSE,
        .mask_int               = SD_ADC_MASK_FALSE,
        .freq                   = SD_ADC_CLOCK_FREQ_1M
};

ad_sdadc_controller_conf_t SD_ADC_DEV = {
        .id  = HW_SDADC,
        .io  = &SD_ADC_BUS,
        .drv = &SD_ADC_DRV,
};

sd_adc_device SD_ADC_DEVICE = &SD_ADC_DEV;

GP_ADC_Config Local_GP_ADC_Config;

SD_ADC_Config Local_SD_ADC_Config;

ADC_Config Local_ADC_Config;

//-----------[O]----------//

bool GP_ADC_Configure(GP_ADC_Config *Config)
{
        // Add in check for the config pass;

        // Configure the IO Bus for the GP ADC
        GP_ADC_BUS.input0.pin           = Config->io_bus.input0.pin;
        GP_ADC_BUS.input0.port          = Config->io_bus.input0.port;
        GP_ADC_BUS.input0.on.mode       = Config->io_bus.input0.on.mode;
        GP_ADC_BUS.input0.on.high       = Config->io_bus.input0.on.high;
        GP_ADC_BUS.input0.on.function   = Config->io_bus.input0.on.function;
        GP_ADC_BUS.input0.on.mode       = Config->io_bus.input0.off.mode;
        GP_ADC_BUS.input0.on.high       = Config->io_bus.input0.off.high;
        GP_ADC_BUS.input0.on.function   = Config->io_bus.input0.off.function;

        GP_ADC_BUS.input1.pin           = Config->io_bus.input1.pin;
        GP_ADC_BUS.input1.port          = Config->io_bus.input1.port;
        GP_ADC_BUS.input1.on.mode       = Config->io_bus.input1.on.mode;
        GP_ADC_BUS.input1.on.high       = Config->io_bus.input1.on.high;
        GP_ADC_BUS.input1.on.function   = Config->io_bus.input1.on.function;
        GP_ADC_BUS.input1.on.mode       = Config->io_bus.input1.off.mode;
        GP_ADC_BUS.input1.on.high       = Config->io_bus.input1.off.high;
        GP_ADC_BUS.input1.on.function   = Config->io_bus.input1.off.function;

        GP_ADC_BUS.voltage_level = Config->io_bus.voltage_level;

        GP_ADC_DRV.clock            = Config->driver.clock;
        GP_ADC_DRV.input            = Config->driver.input;
        GP_ADC_DRV.chopping         = Config->driver.chopping;
        GP_ADC_DRV.continous        = Config->driver.continous;
        GP_ADC_DRV.input_mode       = Config->driver.input_mode;
        GP_ADC_DRV.temp_sensor      = Config->driver.temp_sensor;
        GP_ADC_DRV.sample_time      = Config->driver.sample_time;
        GP_ADC_DRV.oversampling     = Config->driver.oversampling;
        GP_ADC_DRV.input_attenuator = Config->driver.input_attenuator;

        GP_ADC_DEV.io  = &GP_ADC_BUS;
        GP_ADC_DEV.drv = &GP_ADC_DRV;

        gp_adc_device GP_ADC_DEVICE = &GP_ADC_DEV;

        bool GP_ADC_CHECK = ad_gpadc_io_config(((ad_gpadc_controller_conf_t *)GP_ADC_DEVICE)->id,
                ((ad_gpadc_controller_conf_t *)GP_ADC_DEVICE)->io, AD_IO_CONF_OFF);

        if (!GP_ADC_CHECK) // No error found in the adc configure
        {

                Config->configured = true;
                return true;
        }

        Config->configured = false;
        return false;
}

bool SD_AD_Configure(SD_ADC_Config *Config)
{
        SD_ADC_BUS.input0.pin           = Config->io_bus.input0.pin;
        SD_ADC_BUS.input0.port          = Config->io_bus.input0.port;
        SD_ADC_BUS.input0.on.mode       = Config->io_bus.input0.on.mode;
        SD_ADC_BUS.input0.on.high       = Config->io_bus.input0.on.high;
        SD_ADC_BUS.input0.on.function   = Config->io_bus.input0.on.function;
        SD_ADC_BUS.input0.on.mode       = Config->io_bus.input0.off.mode;
        SD_ADC_BUS.input0.on.high       = Config->io_bus.input0.off.high;
        SD_ADC_BUS.input0.on.function   = Config->io_bus.input0.off.function;

        SD_ADC_BUS.input1.pin           = Config->io_bus.input1.pin;
        SD_ADC_BUS.input1.port          = Config->io_bus.input1.port;
        SD_ADC_BUS.input1.on.mode       = Config->io_bus.input1.on.mode;
        SD_ADC_BUS.input1.on.high       = Config->io_bus.input1.on.high;
        SD_ADC_BUS.input1.on.function   = Config->io_bus.input1.on.function;
        SD_ADC_BUS.input1.on.mode       = Config->io_bus.input1.off.mode;
        SD_ADC_BUS.input1.on.high       = Config->io_bus.input1.off.high;
        SD_ADC_BUS.input1.on.function   = Config->io_bus.input1.off.function;

        SD_ADC_BUS.voltage_level = Config->io_bus.voltage_level;

        SD_ADC_DRV.inn            = Config->driver_config.inn;
        SD_ADC_DRV.inp            = Config->driver_config.inp;
        SD_ADC_DRV.freq           = Config->driver_config.freq;
        SD_ADC_DRV.clock          = Config->driver_config.clock;
        SD_ADC_DRV.use_dma        = Config->driver_config.use_dma;
        SD_ADC_DRV.mask_int       = Config->driver_config.mask_int;
        SD_ADC_DRV.input_mode     = Config->driver_config.input_mode;
        SD_ADC_DRV.continuous     = Config->driver_config.continuous;
        SD_ADC_DRV.vref_voltage   = Config->driver_config.vref_voltage;
        SD_ADC_DRV.over_sampling  = Config->driver_config.over_sampling;
        SD_ADC_DRV.vref_selection = Config->driver_config.vref_selection;

        return true;
}

bool ADC_Configure (ADC_Config Config)
{
        Local_ADC_Config = Config;

        if (Config.ADC_Selected == ADC_DEFAULT)
        {
                // No ADC Selected
                Config.ADC_ERROR_CODE = 0;
                return false;
        }
        else if (Config.ADC_Selected == GP_ADC_SELECT)
        {
                // GP_ADC selected
                if (GP_ADC_Configure(&Config.GP_ADC))
                {
                        Config.ADC_ERROR_CODE = 2;
                        return true;
                }
        }
        else if (Config.ADC_Selected == SD_ADC_SELECT)
        {
                // SD_ADC selected
                if (SD_AD_Configure(&Config.SD_ADC))
                {
                        Config.ADC_ERROR_CODE = 2;
                        return true;
                }
        }
        else
        {
                // Error in selection
                Config.ADC_ERROR_CODE = 1;
                return false;
        }
        return true;
}

/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/
