/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: .c
 * Purpose:
 * Version: V1.0
 * Author: E Denton
 * Version
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/

#include "hw_gpadc.h"
#include "hw_sdadc.h"

typedef enum {
        SD_ADC = 1,
        GP_ADC = 2,
} ADC_TYPE;

typedef enum {
        ADC_GPIO_PORT_0   = HW_GPIO_PORT_0,     /**< GPIO Port 0 */
        ADC_GPIO_PORT_1   = HW_GPIO_PORT_1,     /**< GPIO Port 1 */
        ADC_GPIO_PORT_MAX = HW_GPIO_PORT_MAX,        /**< GPIO Port max */
} ADC_GPIO_PORT;

typedef enum {
        ADC_GPIO_PIN_8   = HW_GPIO_PIN_8,      /**< GPIO Pin 8 */
        ADC_GPIO_PIN_9   = HW_GPIO_PIN_9,      /**< GPIO Pin 9 */
        ADC_GPIO_PIN_12  = HW_GPIO_PIN_12,    /**< GPIO Pin 12 */
        ADC_GPIO_PIN_13  = HW_GPIO_PIN_13,    /**< GPIO Pin 13 */
        ADC_GPIO_PIN_18  = HW_GPIO_PIN_18,    /**< GPIO Pin 18 */
        ADC_GPIO_PIN_19  = HW_GPIO_PIN_19,    /**< GPIO Pin 19 */
        ADC_GPIO_PIN_25  = HW_GPIO_PIN_25,    /**< GPIO Pin 25 */
        ADC_GPIO_PIN_MAX = HW_GPIO_PIN_MAX   /**< GPIO Pin max */
} ADC_GPIO_PIN;

typedef enum {
        ADC_GPIO_FUNCTION_GPIO = HW_GPIO_FUNC_GPIO,                  /**< GPIO */
        ADC_GPIO_FUNCTION_ADC  = HW_GPIO_FUNC_ADC,                  /**< GPIO as ADC (dedicated pin) */
        ADC_GPIO_FUNCTION_LAST = HW_GPIO_FUNC_LAST,
} ADC_GPIO_FUNC;

typedef enum {
        ADC_GPIO_MODE_INPUT   = HW_GPIO_MODE_INPUT,                 /**< GPIO as an input */
        ADC_GPIO_MODE_INVALID = HW_GPIO_MODE_INVALID,           /**< GPIO configured as nothing */
} ADC_GPIO_MODE;

typedef enum {
        INPUT_FALSE = 0,
        INPUT_TRUE  = 1,
} ADC_INPUT_STATE;

typedef enum {
        ADC_GPIO_POWER_V33     = HW_GPIO_POWER_V33,          /**< V33 (3.3 V) power rail */
        ADC_GPIO_POWER_VDD1V8P = HW_GPIO_POWER_VDD1V8P,      /**< VDD1V8P (1.8 V) power rail */
        ADC_GPIO_POWER_NONE    = HW_GPIO_POWER_NONE,         /**< Invalid power rail */
} ADC_GPIO_POWER;

//-----------[O]----------//

typedef enum {
        GP_ADC_CLOCK_INTERNAL = HW_GPADC_CLOCK_INTERNAL,       /**< internal high-speed clock (default) */
        GP_ADC_CLOCK_DIGITAL  = HW_GPADC_CLOCK_DIGITAL,         /**< digital clock (16/96MHz) */
} GP_ADC_CLOCK;

typedef enum {
        GP_ADC_INPUT_MODE_DIFFERENTIAL = HW_GPADC_INPUT_MODE_DIFFERENTIAL,    /**< differential mode (default) */
        GP_ADC_INPUT_MODE_SINGLE_ENDED = HW_GPADC_INPUT_MODE_SINGLE_ENDED,     /**< single ended mode */
} GP_ADC_INPUT_MODE;

typedef enum {
        GP_ADC_INPUT_SE_P1_09         = HW_GPADC_INPUT_SE_P1_09,            /**< GPIO P1_09 */
        GP_ADC_INPUT_SE_P0_25         = HW_GPADC_INPUT_SE_P0_25,            /**< GPIO P0_25 */
        GP_ADC_INPUT_SE_P0_08         = HW_GPADC_INPUT_SE_P0_08,            /**< GPIO P0_08 */
        GP_ADC_INPUT_SE_P0_09         = HW_GPADC_INPUT_SE_P0_09,            /**< GPIO P0_09 */
        GP_ADC_INPUT_SE_VDD           = HW_GPADC_INPUT_SE_VDD,            /**< VDD supply of the ADC circuit */
        GP_ADC_INPUT_SE_V30_1         = HW_GPADC_INPUT_SE_V30_1,            /**< V30 supply rail */
        GP_ADC_INPUT_SE_V30_2         = HW_GPADC_INPUT_SE_V30_2,            /**< V30 supply rail */
        GP_ADC_INPUT_SE_VBAT          = HW_GPADC_INPUT_SE_VBAT,            /**< Battery voltage, scaled from 5V to 1.2V */
        GP_ADC_INPUT_SE_VSSA          = HW_GPADC_INPUT_SE_VSSA,            /**< ADC ground */
        GP_ADC_INPUT_SE_P1_13         = HW_GPADC_INPUT_SE_P1_13,           /**< GPIO P1_13 */
        GP_ADC_INPUT_SE_P1_12         = HW_GPADC_INPUT_SE_P1_12,           /**< GPIO P1_12 */
        GP_ADC_INPUT_SE_P1_18         = HW_GPADC_INPUT_SE_P1_18,           /**< GPIO P1_18 */
        GP_ADC_INPUT_SE_P1_19         = HW_GPADC_INPUT_SE_P1_19,           /**< GPIO P1_19 */
        GP_ADC_INPUT_SE_TEMPSENS      = HW_GPADC_INPUT_SE_TEMPSENS,        /**< temperature sensor */
        GP_ADC_INPUT_DIFF_P1_09_P0_25 = HW_GPADC_INPUT_DIFF_P1_09_P0_25,    /**< GPIO P1_09 vs P0_25 */
        GP_ADC_INPUT_DIFF_P0_08_P0_09 = HW_GPADC_INPUT_DIFF_P0_08_P0_09,    /**< GPIO P0_08 vs P0_09 - all other values */
} GP_ADC_INPUT;

typedef enum {
        /* Temperature selection for GP_ADC_DIFF_TEMP_EN = 0 follows this line */
        GP_ADC_CHARGER_TEMPSENS_GND        = HW_GPADC_CHARGER_TEMPSENS_GND,    /**< Ground (no sensor) */
        GP_ADC_CHARGER_TEMPSENS_Z          = HW_GPADC_CHARGER_TEMPSENS_Z,    /**< Z from charger */
        GP_ADC_CHARGER_TEMPSENS_VNTC       = HW_GPADC_CHARGER_TEMPSENS_VNTC,    /**< V(ntc) from charger */
        GP_ADC_CHARGER_TEMPSENS_VTEMP      = HW_GPADC_CHARGER_TEMPSENS_VTEMP,    /**< V(temp) from charger */
        GP_ADC_NO_TEMP_SENSOR              = HW_GPADC_NO_TEMP_SENSOR,    /**< No on-chip temperature sensor selected (default) */
        GP_ADC_TEMP_SENSOR_NEAR_RADIO      = HW_GPADC_TEMP_SENSOR_NEAR_RADIO,    /**< Diode temperature sensor near radio */
        GP_ADC_TEMP_SENSOR_NEAR_CHARGER    = HW_GPADC_TEMP_SENSOR_NEAR_CHARGER,    /**< Diode temperature sensor near charger */
        GP_ADC_TEMP_SENSOR_NEAR_BANDGAP    = HW_GPADC_TEMP_SENSOR_NEAR_BANDGAP,    /**< Diode temperature sensor near bandgap */
        GP_ADC_TEMPSENSOR_MAX              = HW_GPADC_TEMPSENSOR_MAX,    /**< Invalid */
} GP_ADC_TEMP_SENSORS;

typedef enum {
        GP_ADC_SAMPLE_0 = 0,
        GP_ADC_SAMPLE_1 = 1,
        GP_ADC_SAMPLE_2 = 2,
        GP_ADC_SAMPLE_3 = 3,
        GP_ADC_SAMPLE_4 = 4,
        GP_ADC_SAMPLE_5 = 5,
} GP_ADC_SAMPLE_TIME;

typedef enum {
        ADC_CONTINUOUS_TRUE  = true,
        ADC_CONTINUOUS_FALSE = false,
} ADC_CONTINUOUS;

typedef enum {
        GP_ADC_CHOPPING_TRUE  = true,
        GP_ADC_CHOPPING_FALSE = false,
} GP_ADC_CHOPPING;

typedef enum  {
        GP_ADC_OVERSAMPLING_1_SAMPLE          = HW_GPADC_OVERSAMPLING_1_SAMPLE,    /**< 1 sample is taken or 2 in case chopping is enabled */
        GP_ADC_OVERSAMPLING_2_SAMPLES         = HW_GPADC_OVERSAMPLING_2_SAMPLES,    /**< 2 samples are taken */
        GP_ADC_OVERSAMPLING_4_SAMPLES         = HW_GPADC_OVERSAMPLING_4_SAMPLES,    /**< 4 samples are taken */
        GP_ADC_OVERSAMPLING_8_SAMPLES         = HW_GPADC_OVERSAMPLING_8_SAMPLES,    /**< 8 samples are taken */
        GP_ADC_OVERSAMPLING_16_SAMPLES        = HW_GPADC_OVERSAMPLING_16_SAMPLES,    /**< 16 samples are taken */
        GP_ADC_OVERSAMPLING_32_SAMPLES        = HW_GPADC_OVERSAMPLING_32_SAMPLES,    /**< 32 samples are taken */
        GP_ADC_OVERSAMPLING_64_SAMPLES        = HW_GPADC_OVERSAMPLING_64_SAMPLES,    /**< 64 samples are taken */
        GP_ADC_OVERSAMPLING_128_SAMPLES       = HW_GPADC_OVERSAMPLING_128_SAMPLES    /**< 128 samples are taken */
} GP_ADC_OVERSAMPLING;

typedef enum {
        GP_ADC_INPUT_VOLTAGE_UP_TO_1V2 = HW_GPADC_INPUT_VOLTAGE_UP_TO_1V2,    /**< input voltages up to 1.2 V are allowed */
        GP_ADC_INPUT_VOLTAGE_UP_TO_3V6 = HW_GPADC_INPUT_VOLTAGE_UP_TO_3V6     /**< input voltages up to 3.6 V are allowed */
} GP_ADC_INPUT_VOLTAGE;

//-----------[O]----------//

typedef enum {
        SD_ADC_CLOCK_0 = 0,
        SD_ADC_CLOCK_1 = 1,
        SD_ADC_CLOCK_2 = 2,
        SD_ADC_CLOCK_3 = 3,
        SD_ADC_CLOCK_4 = 4,
        SD_ADC_CLOCK_5 = 5,
} SD_ADC_CLOCK;

typedef enum {
        SD_ADC_INPUT_MODE_DIFFERENTIAL = HW_SDADC_INPUT_MODE_DIFFERENTIAL,    /**< Differential mode (default) */
        SD_ADC_INPUT_MODE_SINGLE_ENDED = HW_SDADC_INPUT_MODE_SINGLE_ENDED     /**< Single ended mode. Input selection negative side is ignored */
} SD_ADC_INPUT_MODE;

typedef enum {
        SD_ADC_IN_ADC0_P1_09 = HW_SDADC_IN_ADC0_P1_09,    /**< GPIO P1_09 */
        SD_ADC_IN_ADC1_P0_25 = HW_SDADC_IN_ADC1_P0_25,    /**< GPIO P0_25 */
        SD_ADC_IN_ADC2_P0_08 = HW_SDADC_IN_ADC2_P0_08,    /**< GPIO P0_08 */
        SD_ADC_IN_ADC3_P0_09 = HW_SDADC_IN_ADC3_P0_09,    /**< GPIO P0_09 */
        SD_ADC_IN_ADC4_P1_14 = HW_SDADC_IN_ADC4_P1_14,    /**< GPIO P1_14 */
        SD_ADC_IN_ADC5_P1_20 = HW_SDADC_IN_ADC5_P1_20,    /**< GPIO P1_20 */
        SD_ADC_IN_ADC6_P1_21 = HW_SDADC_IN_ADC6_P1_21,    /**< GPIO P1_21 */
        SD_ADC_IN_ADC7_P1_22 = HW_SDADC_IN_ADC7_P1_22,    /**< GPIO P1_22 */
        SD_ADC_INP_VBAT      = HW_SDADC_INP_VBAT,         /**< VBAT via 4x attenuator, negative side (INN) connected to ground */
} SD_ADC_INPUT;

typedef enum {
        SD_ADC_OSR_128     = HW_SDADC_OSR_128,
        SD_ADC_OSR_256     = HW_SDADC_OSR_256,
        SD_ADC_OSR_512     = HW_SDADC_OSR_512,
        SD_ADC_OSR_1024    = HW_SDADC_OSR_1024,
} SD_ADC_OSR;

typedef enum {
        SD_ADC_VREF_INTERNAL = HW_SDADC_VREF_INTERNAL,    /**< Internal bandgap reference (default) */
        SD_ADC_VREF_EXTERNAL = HW_SDADC_VREF_EXTERNAL     /**< External reference */
} SD_ADC_VREF_SEL;

typedef enum {
        SD_ADC_VREF_VOLTAGE_MIN      = HW_SDADC_VREF_VOLTAGE_MIN,
        SD_ADC_VREF_VOLTAGE_INTERNAL = HW_SDADC_VREF_VOLTAGE_INTERNAL,
        SD_ADC_VREF_VOLTAGE_MAX      = HW_SDADC_VREF_VOLTAGE_MAX
} SD_ADC_VREF_VOLTAGE_RANGE;

typedef enum {
        SD_ADC_DMA_FALSE = 0,
        SD_ADC_DMA_TRUE  = 1,
} SD_ADC_DMA;

typedef enum {
        SD_ADC_MASK_FALSE = 0,
        SD_ADC_MASK_TRUE  = 1,
} SD_ADC_MASK;

typedef enum {
        SD_ADC_CLOCK_FREQ_250K  = HW_SDADC_CLOCK_FREQ_250K,           /**< 00: 250KHz */
        SD_ADC_CLOCK_FREQ_500K  = HW_SDADC_CLOCK_FREQ_500K,           /**< 01: 500KHz */
        SD_ADC_CLOCK_FREQ_1M    = HW_SDADC_CLOCK_FREQ_1M,           /**< 10: 1MHz (default for DA1469x-00) */
        SD_ADC_CLOCK_FREQ_2M    = HW_SDADC_CLOCK_FREQ_2M,           /**< 11: 2MHz */
} SD_ADC_CLOCK_FREQ;

//-----------[O]----------//

typedef enum {
        ADC_DEFAULT   = 0,
        GP_ADC_SELECT = 1,
        SD_ADC_SELECT = 2,
} ADC_SELECT;

//-----------[O]----------//

typedef struct {

} GP_ADC_Config;


/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/

