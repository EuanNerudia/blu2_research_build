/*----------------------------------------------------------------------------
 * Nerudia Vape System
 *----------------------------------------------------------------------------
 * Name: .c
 * Purpose:
 * Version: V1.0
 * Author: E Denton
 * Version
 *----------------------------------------------------------------------------
 * Copyright @2019. Nerudia. All Rights Reserved.
 *----------------------------------------------------------------------------*/

#include <string.h>
#include <stdio.h>
#include <stdbool.h>

#include "osal.h"
#include "resmgmt.h"
#include "hw_cpm.h"
#include "hw_gpio.h"
#include "hw_watchdog.h"
#include "sys_clock_mgr.h"
#include "sys_power_mgr.h"

// Includes required for the ADC
#include "hw_pdc.h"
#include "hw_wkup.h"
#include "hw_sys.h"

#include "ADCAdapter.h"

#include <ad_gpadc.h>

// Type structure required for the General purpose ADC
typedef const void* gp_adc_device;
gp_adc_device GP_ADC_DEVICE;

ad_gpadc_io_conf_t GP_ADC_BUS = {
        .input0 = {
                .port = ADC_GPIO_PORT_MAX,
                .pin  = ADC_GPIO_PIN_MAX,
                .on   = {ADC_GPIO_MODE_INVALID, ADC_GPIO_FUNCTION_LAST,  INPUT_FALSE},
                .off  = {ADC_GPIO_MODE_INVALID, ADC_GPIO_FUNCTION_LAST, INPUT_FALSE}
        },
        .input1 = {
                .port = ADC_GPIO_PORT_MAX,
                .pin  = ADC_GPIO_PIN_MAX,
                .on   = {ADC_GPIO_MODE_INVALID, ADC_GPIO_FUNCTION_LAST, INPUT_FALSE},
                .off  = {ADC_GPIO_MODE_INVALID, ADC_GPIO_FUNCTION_LAST, INPUT_FALSE}
        },
        .voltage_level = ADC_GPIO_POWER_NONE
};

ad_gpadc_driver_conf_t GP_ADC_DRV = {
        .clock                  = GP_ADC_CLOCK_INTERNAL,
        .input_mode             = GP_ADC_INPUT_MODE_SINGLE_ENDED,
        .input                  = GP_ADC_INPUT_SE_P0_25,
        .temp_sensor            = GP_ADC_NO_TEMP_SENSOR,
        .sample_time            = GP_ADC_SAMPLE_1,
        .continous              = ADC_CONTINUOUS_FALSE,
        .chopping               = GP_ADC_CHOPPING_TRUE,
        .oversampling           = GP_ADC_OVERSAMPLING_1_SAMPLE,
        .input_attenuator       = GP_ADC_INPUT_VOLTAGE_UP_TO_1V2
};

ad_gpadc_controller_conf_t GP_ADC_DEV = {
        .id  = HW_GPADC_1,
        .io  = &GP_ADC_BUS,
        .drv = &GP_ADC_DRV
};

gp_adc_device GP_ADC_DEVICE = &GP_ADC_DEV;

GP_ADC_Config Local_GP_ADC_Config;


void (*Loacl_callback)(int Reading);

int  GP_ADC_Configure(GP_ADC_Config *Config)
{
        // Add in check for the config pass;

        ad_gpadc_io_conf_t Temp_BUS = {
                .input0 = {
                        .pin  = Config->io_bus.input0.pin,
                        .port = Config->io_bus.input0.port,
                        .on   = {
                                Config->io_bus.input0.on.mode,
                                Config->io_bus.input0.on.function,
                                Config->io_bus.input0.on.high
                        },
                        .off  = {
                                Config->io_bus.input0.off.mode,
                                Config->io_bus.input0.off.function,
                                Config->io_bus.input0.off.high
                        }
                },
                .input1 = {
                        .pin  = Config->io_bus.input1.pin,
                        .port = Config->io_bus.input1.port,
                        .on   = {
                                Config->io_bus.input1.on.mode,
                                Config->io_bus.input1.on.function,
                                Config->io_bus.input1.on.high
                        },
                        .off  = {
                                Config->io_bus.input1.off.mode,
                                Config->io_bus.input1.off.function,
                                Config->io_bus.input1.off.high
                        }
                },
                .voltage_level = Config->io_bus.voltage_level
        };

        GP_ADC_BUS = Temp_BUS;

        GP_ADC_DRV.clock            = Config->driver.clock;
        GP_ADC_DRV.input            = Config->driver.input;
        GP_ADC_DRV.chopping         = Config->driver.chopping;
        GP_ADC_DRV.continous        = Config->driver.continous;
        GP_ADC_DRV.input_mode       = Config->driver.input_mode;
        GP_ADC_DRV.temp_sensor      = Config->driver.temp_sensor;
        GP_ADC_DRV.sample_time      = Config->driver.sample_time;
        GP_ADC_DRV.oversampling     = Config->driver.oversampling;
        GP_ADC_DRV.input_attenuator = Config->driver.input_attenuator;

        GP_ADC_DEV.io  = &GP_ADC_BUS;
        GP_ADC_DEV.drv = &GP_ADC_DRV;

        GP_ADC_DEVICE = &GP_ADC_DEV;

        bool GP_ADC_CHECK = ad_gpadc_io_config(((ad_gpadc_controller_conf_t *)GP_ADC_DEVICE)->id,
                ((ad_gpadc_controller_conf_t *)GP_ADC_DEVICE)->io, AD_IO_CONF_OFF);

        if (!GP_ADC_CHECK) // No error found in the adc configure
        {

                Config->configured = true;
                return true;
        }

        Local_GP_ADC_Config = Config;

        Config->configured = false;
        return false;
}

void GP_ADC_Value_set(void *user_data, int value)
{
        int *pData = (int*)user_data;

        *pData = value;

        // Second function.
}

void GP_ADC_Read(void(*Call)(reading))
{
        if (Local_GP_ADC_Config.configured)
        {
                int Read_error;
                int Read_data;

                ad_gpadc_handle_t GP_Device = ad_gpadc_opend((ad_gpadc_controller_conf_t *)GP_ADC_DEVICE);

                Read_error = ad_gpadc_read_async(GP_Device, GP_ADC_Value_set, (void *)&Read_data);
        }
        else
        {
                // Set error flag
        }
}

/***************************************************************************
 EOF Copyright (C)2019. Nerudia. All Rights Reserved.
 ***************************************************************************/

