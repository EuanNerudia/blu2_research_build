/**
 ****************************************************************************************
 *
 * @file gpio.c
 *
 * @brief GPIO CMD API implementation
 *
 * Copyright (C) 2016-2019 Dialog Semiconductor.
 * This computer program includes Confidential, Proprietary Information
 * of Dialog Semiconductor. All Rights Reserved.
 *
 ****************************************************************************************
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <hw_gpio.h>
#include "debug_utils.h"
#include "gpio.h"

static bool gpio_pin_init(const char *argv[], HW_GPIO_PORT *gpio_port, HW_GPIO_PIN *gpio_pin)
{
        int number;

        if (*argv[2] != 'p') {
                printf("pin = pXY where X is number of port, Y is number of pin\r\n");
                return false;
        }

        /* Convert from ascii to int */
        number = atoi(argv[2] + 1);

        /* isolate port and pin */
        *gpio_port = number / 10;
        *gpio_pin = number % 10;

        /* Range of number of ports */
        if (*gpio_port > (HW_GPIO_NUM_PORTS - 1)) {
                printf("Port numbers must be in range from 0 to %d\r\n", HW_GPIO_NUM_PORTS - 1);
                return false;
        }

        /* Range of pins in port */
        if (*gpio_pin  > (hw_gpio_port_num_pins[*gpio_port] - 1)) {
                printf("Pin numbers must be in range from 0 to %d\r\n",
                                                         hw_gpio_port_num_pins[*gpio_port] - 1);
                return false;
        }

        return true;
}

static bool gpio_configure(int argc, const char **argv)
{
        HW_GPIO_PORT gpio_port;
        HW_GPIO_PIN gpio_pin;
        HW_GPIO_MODE gpio_mode;
        HW_GPIO_FUNC gpio_function;

        if (argc < 4) {
                return false;
        }

        if (!gpio_pin_init(argv, &gpio_port, &gpio_pin)) {
                return false;
        }

        if (!strcmp(argv[3], "input")) {
                gpio_mode = HW_GPIO_MODE_INPUT;
        } else if (!strcmp(argv[3], "input_pu")) {
                gpio_mode = HW_GPIO_MODE_INPUT_PULLUP;
        } else if (!strcmp(argv[3], "input_pd")) {
                gpio_mode = HW_GPIO_MODE_INPUT_PULLDOWN;
        } else if (!strcmp(argv[3], "output")) {
                gpio_mode = HW_GPIO_MODE_OUTPUT;
        } else if (!strcmp(argv[3], "output_pp")) {
                gpio_mode = HW_GPIO_MODE_OUTPUT_PUSH_PULL;
        } else if (!strcmp(argv[3], "output_od")) {
                gpio_mode = HW_GPIO_MODE_OUTPUT_OPEN_DRAIN;
        } else {
                printf("mode = input | input_pu | input_pd | output | output_pp | output_od\r\n");
                return false;
        }

        /* if user does not type a function parameter then gpio is default */
        if (argc > 4) {
                if (!strcmp(argv[4], "gpio")) {
                        gpio_function = HW_GPIO_FUNC_GPIO;
                }
        } else {
                gpio_function = HW_GPIO_FUNC_GPIO;
        }

        hw_gpio_configure_pin(gpio_port, gpio_pin, gpio_mode, gpio_function, false);

        printf("Port %d Pin %d Mode %s \r\n", gpio_port, gpio_pin, argv[3]);

        return true;
}

static bool gpio_set(int argc, const char **argv)
{
        HW_GPIO_PORT gpio_port;
        HW_GPIO_PIN gpio_pin;

        if (argc < 4) {
                return false;
        }

        if (!gpio_pin_init(argv, &gpio_port, &gpio_pin)) {
                return false;
        }

        if (!strcmp(argv[3], "1")) {
                hw_gpio_set_active(gpio_port, gpio_pin);
                hw_gpio_pad_latch_enable(gpio_port, gpio_pin);
                hw_gpio_pad_latch_disable(gpio_port, gpio_pin);
        } else if (!strcmp(argv[3], "0")) {
                hw_gpio_set_inactive(gpio_port, gpio_pin);
                hw_gpio_pad_latch_enable(gpio_port, gpio_pin);
                hw_gpio_pad_latch_disable(gpio_port, gpio_pin);
        } else {
                printf("state = 0 | 1\r\n");
                return false;
        }

        printf("Set Port %d Pin %d State %s \r\n", gpio_port, gpio_pin, argv[3]);

        return true;
}

static bool gpio_get(int argc, const char **argv)
{
        HW_GPIO_PORT gpio_port;
        HW_GPIO_PIN gpio_pin;

        if (argc < 3) {
                return false;
        }

        if (!gpio_pin_init(argv, &gpio_port, &gpio_pin)) {
                return false;
        }

        printf("Port %d Pin %d State %d \r\n", gpio_port, gpio_pin,
                                                      hw_gpio_get_pin_status(gpio_port, gpio_pin));

        return true;
}

static bool gpio_toggle(int argc, const char **argv)
{
        HW_GPIO_PORT gpio_port;
        HW_GPIO_PIN gpio_pin;

        if (argc < 3) {
                return false;
        }

        if (!gpio_pin_init(argv, &gpio_port, &gpio_pin)) {
                return false;
        }

        hw_gpio_toggle(gpio_port, gpio_pin);
        hw_gpio_pad_latch_enable(gpio_port, gpio_pin);
        hw_gpio_pad_latch_disable(gpio_port, gpio_pin);

        printf("Port %d Pin %d Toggle\r\n", gpio_port, gpio_pin);

        return true;
}

static bool gpio_power(int argc, const char **argv)
{
        HW_GPIO_PORT gpio_port;
        HW_GPIO_PIN gpio_pin;
        HW_GPIO_POWER gpio_power;
        const char *power_str;

        if (argc < 4) {
                return false;
        }

        if (!gpio_pin_init(argv, &gpio_port, &gpio_pin)) {
                return false;
        }

        if (!strcmp(argv[3], "v33")) {
                gpio_power = HW_GPIO_POWER_V33;
                power_str = "3.3V";
        } else if (!strcmp(argv[3], "vdd1v8p")) {
                gpio_power = HW_GPIO_POWER_VDD1V8P;
                power_str = "1.8V";
        } else {
                printf("power = v33 | vdd1v8p\r\n");
                return false;
        }

        hw_gpio_configure_pin_power(gpio_port, gpio_pin, gpio_power);

        printf("Port %d Pin %d Power %s\r\n", gpio_port, gpio_pin, power_str);

        return true;
}

static const debug_handler_t gpio_handlers[] = {
        { "configure", "<pin> <mode> [<function>]", gpio_configure },
        { "set", "<pin> <state>", gpio_set },
        { "get", "<pin>", gpio_get },
        { "toggle", "<pin>", gpio_toggle },
        { "power", "<pin> <power>", gpio_power },
        { NULL },
};

void gpio_command(int argc, const char *argv[], void *user_data)
{
        debug_handle_message(argc, argv, gpio_handlers);
}
