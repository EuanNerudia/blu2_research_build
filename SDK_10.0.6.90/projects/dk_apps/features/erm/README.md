Eccentric Rotating Mass application {#erm}
=====================================

## Overview

This application can be used to drive an Eccentric Rotating Mass (ERM) motor connected to ProDK's J10 (pins 1 and 3 for positive and negative differential output respectively).

### Demo description

In `system_init` task ERM is configured and initialized. The user should provide ERM's driving strength in permille (see DA14697/DA14699 datasheet) and which signal output/s
will be used (HDRVM or HDRVP or both). In case of differential output (both HDRVP and HDRVM are used), motor's frequency should also be configured.

e.g.
~~~{.c}
        haptic_config_t erm_cfg = {
                .duty_cycle = 500,
                .resonant_frequency = 200,
                .signal_out = HW_ERM_OUTPUT_HDRVM_HDRVP
        };
~~~

The K1 user button is used in order to start/stop the ERM feedback.

This project redirects I/O (e.g. printf) to SEGGER's Real Time Terminal.
To access the terminal, open a telnet terminal to locahost:19021.