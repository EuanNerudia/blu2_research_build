################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../snc_demo/drivers/bh1750/src/bh1750.c \
../snc_demo/drivers/bh1750/src/bh1750_snc_demo.c \
../snc_demo/drivers/bh1750/src/bh1750_ucodes.c 

OBJS += \
./snc_demo/drivers/bh1750/src/bh1750.o \
./snc_demo/drivers/bh1750/src/bh1750_snc_demo.o \
./snc_demo/drivers/bh1750/src/bh1750_ucodes.o 

C_DEPS += \
./snc_demo/drivers/bh1750/src/bh1750.d \
./snc_demo/drivers/bh1750/src/bh1750_snc_demo.d \
./snc_demo/drivers/bh1750/src/bh1750_ucodes.d 


# Each subdirectory must supply rules for building sources it contributes
snc_demo/drivers/bh1750/src/%.o: ../snc_demo/drivers/bh1750/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\features\snc_test\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\OS" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\Config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\portable\GCC\DA1469x" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\SEGGER" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\features\snc_test\snc_demo\drivers\adxl362\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\features\snc_test\snc_demo\drivers\24fc256_eeprom\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\features\snc_test\snc_demo\drivers\bh1750\include" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\features\snc_test\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


