Linear Resonance Actuator application {#lra}
=====================================

## Overview

This application can be used to drive a Linear Frequency Actuator (LRA) motor connected to ProDK's J10 (pins 1 and 3 for positive and negative differential output respectively).

### Demo description

In `system_init` task LRA is configured and initialized.

The application provides three predefined LRA motors

- LVM61530B

- LVM61930B 

- G0825001

with their respective optimal, min and max frequencies. In case one of the predefined motors is used, `LRA_MOTOR` definition should match one of the aforementioned names.

e.g.
~~~{.c}
#define LRA_MOTOR      LVM61530B
~~~

Otherwise the user should specify LRA's frequencies (optimal, min and max), the driving strength in permille
and the preamplifier's trim gain (see DA14697/DA14699 datasheet).

The K1 user button is used in order to start/stop the LRA feedback.

This project redirects I/O (e.g. printf) to SEGGER's Real Time Terminal.
To access the terminal, open a telnet terminal to locahost:19021.