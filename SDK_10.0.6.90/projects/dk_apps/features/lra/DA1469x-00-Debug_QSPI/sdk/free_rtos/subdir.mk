################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/croutine.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/event_groups.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/list.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/queue.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/stream_buffer.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/tasks.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/timers.c 

OBJS += \
./sdk/free_rtos/croutine.o \
./sdk/free_rtos/event_groups.o \
./sdk/free_rtos/list.o \
./sdk/free_rtos/queue.o \
./sdk/free_rtos/stream_buffer.o \
./sdk/free_rtos/tasks.o \
./sdk/free_rtos/timers.o 

C_DEPS += \
./sdk/free_rtos/croutine.d \
./sdk/free_rtos/event_groups.d \
./sdk/free_rtos/list.d \
./sdk/free_rtos/queue.d \
./sdk/free_rtos/stream_buffer.d \
./sdk/free_rtos/tasks.d \
./sdk/free_rtos/timers.d 


# Each subdirectory must supply rules for building sources it contributes
sdk/free_rtos/croutine.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/croutine.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\OS" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\Config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\SEGGER" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\features\lra\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/free_rtos/event_groups.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/event_groups.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\OS" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\Config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\SEGGER" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\features\lra\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/free_rtos/list.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/list.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\OS" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\Config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\SEGGER" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\features\lra\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/free_rtos/queue.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/queue.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\OS" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\Config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\SEGGER" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\features\lra\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/free_rtos/stream_buffer.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/stream_buffer.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\OS" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\Config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\SEGGER" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\features\lra\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/free_rtos/tasks.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/tasks.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\OS" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\Config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\SEGGER" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\features\lra\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/free_rtos/timers.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/timers.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g3 -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\OS" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\Config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\segger_tools\SEGGER" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\features\lra\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


