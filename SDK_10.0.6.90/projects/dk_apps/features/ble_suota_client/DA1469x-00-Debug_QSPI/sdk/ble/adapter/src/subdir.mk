################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/interfaces/ble/adapter/src/ad_ble.c 

OBJS += \
./sdk/ble/adapter/src/ad_ble.o 

C_DEPS += \
./sdk/ble/adapter/src/ad_ble.d 


# Each subdirectory must supply rules for building sources it contributes
sdk/ble/adapter/src/ad_ble.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/interfaces/ble/adapter/src/ad_ble.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g3 -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\features\ble_suota_client\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\adapter\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\api\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\clients\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\manager\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\stack\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\stack\da14690\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\portable\GCC\DA1469x" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\cli\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\console\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\features\ble_suota_client\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


