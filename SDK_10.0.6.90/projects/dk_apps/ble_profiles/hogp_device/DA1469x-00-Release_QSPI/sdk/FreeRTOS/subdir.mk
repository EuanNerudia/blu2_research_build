################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/croutine.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/event_groups.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/list.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/queue.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/stream_buffer.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/tasks.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/timers.c 

OBJS += \
./sdk/FreeRTOS/croutine.o \
./sdk/FreeRTOS/event_groups.o \
./sdk/FreeRTOS/list.o \
./sdk/FreeRTOS/queue.o \
./sdk/FreeRTOS/stream_buffer.o \
./sdk/FreeRTOS/tasks.o \
./sdk/FreeRTOS/timers.o 

C_DEPS += \
./sdk/FreeRTOS/croutine.d \
./sdk/FreeRTOS/event_groups.d \
./sdk/FreeRTOS/list.d \
./sdk/FreeRTOS/queue.d \
./sdk/FreeRTOS/stream_buffer.d \
./sdk/FreeRTOS/tasks.d \
./sdk/FreeRTOS/timers.d 


# Each subdirectory must supply rules for building sources it contributes
sdk/FreeRTOS/croutine.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/croutine.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -DRELEASE_BUILD -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\ble_profiles\hogp_device\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\adapter\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\api\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\manager\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\services\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\stack\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\stack\da14690\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\ble_profiles\hogp_device\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/FreeRTOS/event_groups.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/event_groups.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -DRELEASE_BUILD -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\ble_profiles\hogp_device\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\adapter\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\api\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\manager\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\services\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\stack\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\stack\da14690\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\ble_profiles\hogp_device\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/FreeRTOS/list.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/list.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -DRELEASE_BUILD -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\ble_profiles\hogp_device\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\adapter\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\api\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\manager\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\services\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\stack\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\stack\da14690\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\ble_profiles\hogp_device\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/FreeRTOS/queue.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/queue.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -DRELEASE_BUILD -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\ble_profiles\hogp_device\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\adapter\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\api\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\manager\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\services\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\stack\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\stack\da14690\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\ble_profiles\hogp_device\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/FreeRTOS/stream_buffer.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/stream_buffer.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -DRELEASE_BUILD -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\ble_profiles\hogp_device\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\adapter\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\api\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\manager\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\services\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\stack\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\stack\da14690\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\ble_profiles\hogp_device\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/FreeRTOS/tasks.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/tasks.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -DRELEASE_BUILD -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\ble_profiles\hogp_device\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\adapter\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\api\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\manager\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\services\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\stack\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\stack\da14690\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\ble_profiles\hogp_device\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/FreeRTOS/timers.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/free_rtos/timers.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -mfloat-abi=hard -mfpu=fpv5-sp-d16 -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Wall  -g3 -Ddg_configDEVICE=DEVICE_DA1469x -Ddg_configBLACK_ORCA_IC_REV=BLACK_ORCA_IC_REV_A -Ddg_configBLACK_ORCA_IC_STEP=BLACK_ORCA_IC_STEP_A -DRELEASE_BUILD -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\ble_profiles\hogp_device\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\adapter\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\api\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\manager\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\services\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\stack\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\interfaces\ble\stack\da14690\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\free_rtos\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\snc\src" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\projects\dk_apps\ble_profiles\hogp_device\config\custom_config_qspi.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


