/**
 * \addtogroup BSP
 * \{
 * \addtogroup SYSTEM
 * \{
 * \addtogroup SYS_USB
 *
 * \brief System USB
 *
 * \{
 */

/**
 ****************************************************************************************
 *
 * @file sys_usb_da1468x.h
 *
 * @brief System USB header file.
 *
 * Copyright (C) 2018 Dialog Semiconductor.
 * This computer program includes Confidential, Proprietary Information
 * of Dialog Semiconductor. All Rights Reserved.
 *
 ****************************************************************************************
 */


/**
 \}
 \}
 \}
 */
