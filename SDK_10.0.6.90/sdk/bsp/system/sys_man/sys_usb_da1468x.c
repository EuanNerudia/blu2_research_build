/**
 ****************************************************************************************
 *
 * @file sys_usb_da1468x.c
 *
 * @brief System USB
 *
 * Copyright (C) 2018 Dialog Semiconductor.
 * This computer program includes Confidential, Proprietary Information
 * of Dialog Semiconductor. All Rights Reserved.
 *
 ****************************************************************************************
 */

/**
 \addtogroup BSP
 \{
 \addtogroup SYSTEM
 \{
 \addtogroup SYS_USB
 \{
 */

/**
 \}
 \}
 \}
 */
