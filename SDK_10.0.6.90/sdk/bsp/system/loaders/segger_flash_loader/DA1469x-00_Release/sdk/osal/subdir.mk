################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/osal/msg_queues.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/osal/resmgmt.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/osal/usb_osal_wrapper.c 

OBJS += \
./sdk/osal/msg_queues.o \
./sdk/osal/resmgmt.o \
./sdk/osal/usb_osal_wrapper.o 

C_DEPS += \
./sdk/osal/msg_queues.d \
./sdk/osal/resmgmt.d \
./sdk/osal/usb_osal_wrapper.d 


# Each subdirectory must supply rules for building sources it contributes
sdk/osal/msg_queues.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/osal/msg_queues.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -DRELEASE_BUILD -Ddg_configDEVICE=DEVICE_DA1469x -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\config\custom_config.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/osal/resmgmt.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/osal/resmgmt.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -DRELEASE_BUILD -Ddg_configDEVICE=DEVICE_DA1469x -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\config\custom_config.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/osal/usb_osal_wrapper.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/osal/usb_osal_wrapper.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -DRELEASE_BUILD -Ddg_configDEVICE=DEVICE_DA1469x -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\config\custom_config.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


