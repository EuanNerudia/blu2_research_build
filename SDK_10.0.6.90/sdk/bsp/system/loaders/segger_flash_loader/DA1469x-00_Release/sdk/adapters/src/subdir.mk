################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_crypto.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_flash.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_iso7816.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_keyboard_scanner.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_lcdc.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_nvms.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_nvms_direct.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_nvms_ves.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_pmu.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_sdadc.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_snc.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_template.c \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/sys_platform_devices_internal.c 

OBJS += \
./sdk/adapters/src/ad_crypto.o \
./sdk/adapters/src/ad_flash.o \
./sdk/adapters/src/ad_iso7816.o \
./sdk/adapters/src/ad_keyboard_scanner.o \
./sdk/adapters/src/ad_lcdc.o \
./sdk/adapters/src/ad_nvms.o \
./sdk/adapters/src/ad_nvms_direct.o \
./sdk/adapters/src/ad_nvms_ves.o \
./sdk/adapters/src/ad_pmu.o \
./sdk/adapters/src/ad_sdadc.o \
./sdk/adapters/src/ad_snc.o \
./sdk/adapters/src/ad_template.o \
./sdk/adapters/src/sys_platform_devices_internal.o 

C_DEPS += \
./sdk/adapters/src/ad_crypto.d \
./sdk/adapters/src/ad_flash.d \
./sdk/adapters/src/ad_iso7816.d \
./sdk/adapters/src/ad_keyboard_scanner.d \
./sdk/adapters/src/ad_lcdc.d \
./sdk/adapters/src/ad_nvms.d \
./sdk/adapters/src/ad_nvms_direct.d \
./sdk/adapters/src/ad_nvms_ves.d \
./sdk/adapters/src/ad_pmu.d \
./sdk/adapters/src/ad_sdadc.d \
./sdk/adapters/src/ad_snc.d \
./sdk/adapters/src/ad_template.d \
./sdk/adapters/src/sys_platform_devices_internal.d 


# Each subdirectory must supply rules for building sources it contributes
sdk/adapters/src/ad_crypto.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_crypto.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -DRELEASE_BUILD -Ddg_configDEVICE=DEVICE_DA1469x -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\config\custom_config.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_flash.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_flash.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -DRELEASE_BUILD -Ddg_configDEVICE=DEVICE_DA1469x -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\config\custom_config.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_iso7816.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_iso7816.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -DRELEASE_BUILD -Ddg_configDEVICE=DEVICE_DA1469x -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\config\custom_config.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_keyboard_scanner.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_keyboard_scanner.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -DRELEASE_BUILD -Ddg_configDEVICE=DEVICE_DA1469x -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\config\custom_config.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_lcdc.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_lcdc.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -DRELEASE_BUILD -Ddg_configDEVICE=DEVICE_DA1469x -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\config\custom_config.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_nvms.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_nvms.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -DRELEASE_BUILD -Ddg_configDEVICE=DEVICE_DA1469x -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\config\custom_config.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_nvms_direct.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_nvms_direct.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -DRELEASE_BUILD -Ddg_configDEVICE=DEVICE_DA1469x -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\config\custom_config.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_nvms_ves.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_nvms_ves.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -DRELEASE_BUILD -Ddg_configDEVICE=DEVICE_DA1469x -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\config\custom_config.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_pmu.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_pmu.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -DRELEASE_BUILD -Ddg_configDEVICE=DEVICE_DA1469x -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\config\custom_config.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_sdadc.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_sdadc.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -DRELEASE_BUILD -Ddg_configDEVICE=DEVICE_DA1469x -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\config\custom_config.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_snc.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_snc.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -DRELEASE_BUILD -Ddg_configDEVICE=DEVICE_DA1469x -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\config\custom_config.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/ad_template.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/ad_template.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -DRELEASE_BUILD -Ddg_configDEVICE=DEVICE_DA1469x -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\config\custom_config.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

sdk/adapters/src/sys_platform_devices_internal.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/middleware/adapters/src/sys_platform_devices_internal.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall  -g -DRELEASE_BUILD -Ddg_configDEVICE=DEVICE_DA1469x -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\segger_flash_loader\config\custom_config.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


