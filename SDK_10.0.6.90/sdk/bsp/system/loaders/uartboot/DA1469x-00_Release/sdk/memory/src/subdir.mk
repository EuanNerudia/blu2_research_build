################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/memory/src/qspi_automode.c 

OBJS += \
./sdk/memory/src/qspi_automode.o 

C_DEPS += \
./sdk/memory/src/qspi_automode.d 


# Each subdirectory must supply rules for building sources it contributes
sdk/memory/src/qspi_automode.o: C:/Projects/SmartSnippets/SDK_10.0.6.90/sdk/bsp/memory/src/qspi_automode.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m33 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -Werror -Wall -Wno-stringop-overflow  -g -DRELEASE_BUILD -Ddg_configDEVICE=DEVICE_DA1469x -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\adapters\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\memory\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\config" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\peripherals\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\uartboot\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\middleware\osal" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\sys_man\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\include" -I"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\util\crc" -include"C:\Projects\SmartSnippets\SDK_10.0.6.90\sdk\bsp\system\loaders\uartboot\config\custom_config.h" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


