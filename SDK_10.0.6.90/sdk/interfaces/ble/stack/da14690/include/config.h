/**
 ****************************************************************************************
 *
 * @file config.h
 *
 * @brief Global configuration header file.
 *
 * Copyright (C) 2016-2019 Dialog Semiconductor.
 * This computer program includes Confidential, Proprietary Information
 * of Dialog Semiconductor. All Rights Reserved.
 *
 ****************************************************************************************
 */


#ifndef _CONFIG_H_
#define _CONFIG_H_

/* Override defaults configuration options here */

#define dg_cfgSIMULATION_MODE           (0)

#define dg_cfgIMAGE_MODE                DEVELOPMENT_MODE
#define dg_cfgCMAC_CLOCK                CMAC_CLK_32
#define dg_cfgENABLE_GPIO_DEBUG         (1)
#define dg_cfgUSE_FPGA                  (0)
#define dg_cfg690_REVISION              DA690_RevAB //DA690_RevAA

#if (dg_cfgSIMULATION_MODE == 1)
#define dg_cfgUSE_DBG_ASSERT_SET        DBG_ASSERT_SIMULATION
#else
#define dg_cfgUSE_DBG_ASSERT_SET        DBG_ASSERT_RUN
#endif

#if (dg_cfgIMAGE_MODE == DEVELOPMENT_MODE)
#define dg_cfgRUN_PROFILING             (0) // doesn't work when sleep is on
#define dg_cfgPROCESS_BLE_ET            (1)
#define dg_cfgBLE_INT_HANDLING_DEBUG    (1)
#endif

// The macros below are ok for 32000/32768 but not ok for i.e. RCX. Most probably they must be part
// of the dynamic configuration structure (Jump Table?)
#define dg_cfgPOWER_UP_DELAY            (42) // in LP cycles
#define dg_cfgXTAL32M_DELAY             (60) // in LP cycles
#define dg_cfgWAKEUP_DELAY              (1) // in LP cycles
#define dg_cfgMIN_SLEEP_DURATION        (2) // in slots

#define dg_cfgUSE_SLEEP_DEEP            (1)

/*
 * preferred value for XTALRDY_CTRL_REG[XTALRDY_CNT]
 * (actual value will be determined by characterization)
 */
#define dg_cfgPREFERRED_XTALRDY_CNT     (0x30)

#include "config_defaults.h"

#endif // _CONFIG_H_
