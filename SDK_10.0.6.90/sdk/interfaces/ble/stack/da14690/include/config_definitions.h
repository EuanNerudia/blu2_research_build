/**
 ****************************************************************************************
 *
 * @file config_definitions.h
 *
 * @brief Configuration definitions header file.
 *
 * Copyright (C) 2016-2019 Dialog Semiconductor.
 * This computer program includes Confidential, Proprietary Information
 * of Dialog Semiconductor. All Rights Reserved.
 *
 ****************************************************************************************
 */


#ifndef _CONFIG_DEFINITIONS_H_
#define _CONFIG_DEFINITIONS_H_

#define DEVELOPMENT_MODE                0       // Code is built for debugging
#define PRODUCTION_MODE                 1       // Code is built for production

#define CMAC_CLK_16                     0       // 16MHz
#define CMAC_CLK_32                     1       // 32MHz

#define DBG_ASSERT_NONE                 0       // Assertions are empty
#define DBG_ASSERT_RUN                  1       // Assertions are set for running platform
#define DBG_ASSERT_SIMULATION           2       // Assertions are set for simulation mode 

#define DA690_RevAA                     1
#define DA690_RevAB                     2

#endif // _CONFIG_DEFINITIONS_H_
