~~~{.c}
****************************************************************************************
*
* Copyright (C) 2015-2019. Dialog Semiconductor.
* This computer program includes Confidential, Proprietary Information
* of Dialog Semiconductor. All Rights Reserved.
*
****************************************************************************************
~~~

Follow [this link](modules.html) to browse the SmartSnippets DA1469x SDK documentation.

[Here](@ref ble_pers_strg) you can read about BLE persistent storage for
bonded devices.

[Here](@ref flash_support) you can read about flash support in the SDK.

Instructions for example projects
=================================

DK Applications
---------------

- [Apple Media Service demo application] (@ref ams)
- [Apple Notification Center Service demo aplication] (@ref ancs)
- [Bond Management Service demo application] (@ref BMS)
- [Cycling Speed and Cadence collector demo application] (@ref cscp_collector)
- [HOGP Device demo application] (@ref hogp_device)
- [HOGP Host demo application] (@ref hogp_host)
- [Heart Rate demo application] (@ref hrp)
- [Heart Rate collector demo application] (@ref hrp_collector)
- [Weightscale profile demo application] (@ref wsp_weightscale)
- [Proximity Reporter demo application] (@ref pxp)
- [SUOTA in Proximity Reporter] (@ref pxp_suota)
- [BLE Central demo application] (@ref ble_central)
- [BLE peripheral demo application] (@ref ble_peripheral)
- [BLE CLI demo application] (@ref ble_cli)
- [Multi Link demo application] (@ref multi_link)
- [FreeRTOS Template Project] (@ref freertos_retarget)
- [SUOTA 1.2 client application] (@ref suota_client)
- [Temperature drift monitoring demo application] (@ref snc_gpadc_demo)
- [Sensor Node (SNC) and SNC-Adapter application] (@ref snc_test)

Utilities
=========

- [CLI programmer application] (@ref cli_programmer)
- [Collect debug information ] (@ref collect_debug_info)
- [Collect task debug information] (@ref monitor_task)
- [Secure image programming] (@ref secure_image)
