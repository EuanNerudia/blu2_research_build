var searchData=
[
  ['real_5ftime_5fclock_20_28rtc_29_20driver',['Real_Time_Clock (RTC) Driver',['../group___h_w___r_t_c.html',1,'']]],
  ['realtime_20os',['RealTime OS',['../group___m_i_d___r_t_o_s.html',1,'']]],
  ['resource_20management_20for_20abstract_20os',['Resource Management for Abstract OS',['../group___o_s_a_l___r_e_s_o_u_r_c_e___m_g_m_t.html',1,'']]],
  ['register_20description',['Register Description',['../group___p_l_a___b_s_p___r_e_g_i_s_t_e_r_s.html',1,'']]],
  ['ram_5fproject_5fmemory_5flayout_5fsettings',['RAM_PROJECT_MEMORY_LAYOUT_SETTINGS',['../group___r_a_m___p_r_o_j_e_c_t___m_e_m_o_r_y___l_a_y_o_u_t___s_e_t_t_i_n_g_s.html',1,'']]],
  ['refip',['REFIP',['../group___r_e_f_i_p.html',1,'']]],
  ['rf_5fdriver_5fsettings',['RF_DRIVER_SETTINGS',['../group___r_f___d_r_i_v_e_r___s_e_t_t_i_n_g_s.html',1,'']]],
  ['rf_5fsettings',['RF_SETTINGS',['../group___r_f___s_e_t_t_i_n_g_s.html',1,'']]],
  ['root',['ROOT',['../group___r_o_o_t.html',1,'']]],
  ['rx_5fmode_5fsettings',['RX_MODE_SETTINGS',['../group___r_x___m_o_d_e___s_e_t_t_i_n_g_s.html',1,'']]]
];
