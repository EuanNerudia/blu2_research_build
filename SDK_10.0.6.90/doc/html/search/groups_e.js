var searchData=
[
  ['peripheral_20devices',['Peripheral Devices',['../group___device___peripheral__peripherals.html',1,'']]],
  ['packets',['Packets',['../group___d_g_t_l___p_k_t.html',1,'']]],
  ['pdc_20driver',['PDC Driver',['../group___h_w___p_d_c.html',1,'']]],
  ['power_20manager_20driver',['Power Manager Driver',['../group___h_w___p_m_u.html',1,'']]],
  ['power_20domain_20driver',['Power Domain Driver',['../group___p_d.html',1,'']]],
  ['peripheral_5fselection',['PERIPHERAL_SELECTION',['../group___p_e_r_i_p_h_e_r_a_l___s_e_l_e_c_t_i_o_n.html',1,'']]],
  ['peripheral_20lld_27s',['Peripheral LLD&apos;s',['../group___p_l_a___d_r_i___p_e_r_p_h_e_r_a_l_s.html',1,'']]],
  ['platform_2fdevice',['Platform/Device',['../group___p_l_a_t_f_o_r_m___d_e_v_i_c_e.html',1,'']]],
  ['pmu_20adapter',['PMU Adapter',['../group___p_m_u___a_d_a_p_t_e_r.html',1,'']]],
  ['peripheral_20device_20register_20masking',['Peripheral Device Register Masking',['../group___pos_mask__peripherals.html',1,'']]],
  ['power_20manager_20service',['Power Manager Service',['../group___p_o_w_e_r___m_a_n_a_g_e_r.html',1,'']]],
  ['power_5fsettings',['POWER_SETTINGS',['../group___p_o_w_e_r___s_e_t_t_i_n_g_s.html',1,'']]],
  ['prf_5fconfig',['PRF_CONFIG',['../group___p_r_f___c_o_n_f_i_g.html',1,'']]],
  ['profiling_5fsettings',['PROFILING_SETTINGS',['../group___p_r_o_f_i_l_i_n_g___s_e_t_t_i_n_g_s.html',1,'']]]
];
