var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz",
  1: "_abcdefghiklmnopqrstuwx",
  2: "abcdfghiklmopqrstuw",
  3: "_abcdfghiklmnpqrstuw",
  4: "_abcdefghijklmnopqrstuvwxyz",
  5: "abcdeghiklmnopqrsuw",
  6: "_abcdeghiklmnorstuw",
  7: "abcdefghiklmnoprstuvwx",
  8: "_b",
  9: "abcdefghiklmnopqrstuvwy",
  10: "abcdefghlmpstw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

