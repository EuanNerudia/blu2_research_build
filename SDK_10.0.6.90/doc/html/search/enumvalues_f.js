var searchData=
[
  ['reset_5firqn',['Reset_IRQn',['../group___configuration__of___c_m_s_i_s.html#gga7e1129cd8a196f4284d41db3e82ad5c8a50ad21f2fd0d54d04b390d5a9145889a',1,'DA1469xAB.h']]],
  ['rfdiag_5firqn',['RFDIAG_IRQn',['../group___configuration__of___c_m_s_i_s.html#gga7e1129cd8a196f4284d41db3e82ad5c8a8a9eae0530105319a1b3c532cba45cd0',1,'DA1469xAB.h']]],
  ['role_5fend',['ROLE_END',['../group___c_o___b_t.html#ggade9ca5088d171ad20b4c237f1c2d6260a54cbe6d25cfd1585ebdd58c916e08c32',1,'co_bt.h']]],
  ['role_5fmaster',['ROLE_MASTER',['../group___c_o___b_t.html#ggade9ca5088d171ad20b4c237f1c2d6260a47f287004d68c97c3589027d98225676',1,'co_bt.h']]],
  ['role_5fslave',['ROLE_SLAVE',['../group___c_o___b_t.html#ggade9ca5088d171ad20b4c237f1c2d6260a6cff0abbfa0fc9286a1d13b0ffd76cba',1,'co_bt.h']]],
  ['rtc_5fevent_5firqn',['RTC_EVENT_IRQn',['../group___configuration__of___c_m_s_i_s.html#gga7e1129cd8a196f4284d41db3e82ad5c8a5b942e25ec4362da271f205f1717e753',1,'DA1469xAB.h']]],
  ['rtc_5firqn',['RTC_IRQn',['../group___configuration__of___c_m_s_i_s.html#gga7e1129cd8a196f4284d41db3e82ad5c8adcc0f2770f7f57f75fac3d8bcac0e858',1,'DA1469xAB.h']]]
];
